# 0.2.0

* Add support for bevy 0.13
* Add transform tool to rotate and scale groups of objects

# 0.1.2

* Expose more modules and types

# 0.1.1  

* Improve mouse zoom scrolling to allow for infinite zooming  
* Fix bug where sometimes grid render system caused crash  