use bevy::{ecs::system::SystemId, prelude::*, render::primitives::Aabb};

use crate::{
    controls::AbstractMouseState,
    core::{EditorObject, EditorWorldObject, LevelEditProperties},
    geometry::*,
    map_entity::{MapEntity, MapEntityListings},
    selection::SelectedEntities,
    user_data::{UserDataComponent, UserDataListings},
};

// -------------------------------------------------------------------------------------------------

pub struct UtilityPlugin;

#[derive(Resource)]
pub struct UtilSystemIds {
    /// remove all the currently selected entities from the world
    pub remove_selected_objs: SystemId,

    /// reset the camera zoom to 1 and center at origin
    pub normalize_view: SystemId,

    /// nudge all selected objects to the right by grid size
    pub nudge_selection_right: SystemId,

    /// nudge all selected objects to the left by grid size
    pub nudge_selection_left: SystemId,

    /// nudge all selected objects upward by grid size
    pub nudge_selection_up: SystemId,

    /// nudge all selected objects downward by grid size
    pub nudge_selection_down: SystemId,

    /// force all the map entities in the world to adhere to the data structure defined in their
    /// corresponding entity listing
    pub enforce_entity_data_types: SystemId,

    /// iterate through each map entity and remove all entities who do not belong to a listing, and
    /// then bump each entity listing id down if there are any gaps in the entity listing id
    pub validate_entity_listing_ids: SystemId,

    /// iterate through each selected polygon and recenter it's translation so it's in the middle
    /// of all of it's vertices
    pub recenter_selected_poly_translations: SystemId,

    /// select each editor object in the scene
    pub select_all: SystemId,
}

// -------------------------------------------------------------------------------------------------

impl Plugin for UtilityPlugin {
    fn build(&self, app: &mut App) {
        // register utility systems and track the system ids
        let sys_ids = UtilSystemIds {
            remove_selected_objs: app.world.register_system(remove_selected_objs),
            normalize_view: app.world.register_system(normalize_view),
            nudge_selection_right: app.world.register_system(nudge_selection_right),
            nudge_selection_left: app.world.register_system(nudge_selection_left),
            nudge_selection_up: app.world.register_system(nudge_selection_up),
            nudge_selection_down: app.world.register_system(nudge_selection_down),
            enforce_entity_data_types: app.world.register_system(enforce_entity_data_types),
            validate_entity_listing_ids: app.world.register_system(validate_entity_listing_ids),
            recenter_selected_poly_translations: app
                .world
                .register_system(recenter_sel_poly_translations),
            select_all: app.world.register_system(select_all),
        };

        // add the system ids to the app
        app.insert_resource(sys_ids);
    }
}

// Utility Misc: -----------------------------------------------------------------------------------

/// return true if the two rects overlap (edge inclusive)
pub fn rects_overlap(rect_a: &Rect, rect_b: &Rect) -> bool {
    !(rect_a.max.x < rect_b.min.x
        || rect_a.max.y < rect_b.min.y
        || rect_a.min.x > rect_b.max.x
        || rect_a.min.y > rect_b.max.y)
}

/// create a sprite bundle that represents a dot of the specified size, in pixels, at the specified
/// location, with the color specified
pub fn create_dot_sprite(translation: Vec3, size: f32, color: Color) -> SpriteBundle {
    SpriteBundle {
        transform: Transform::from_scale(Vec3::new(size, size, 1.0)).with_translation(translation),
        sprite: Sprite {
            color,
            ..Default::default()
        },
        ..Default::default()
    }
}

// Utility Queries: --------------------------------------------------------------------------------

/// returns a vec of entities representing all the geometries that overlap the specified world rect
pub fn get_geoms_in_aabb(
    world_rect: Rect,
    query: &Query<(Entity, &GlobalTransform, &GeometryComponent)>,
) -> Vec<Entity> {
    let mut vec = Vec::<Entity>::new();
    for (ent, glob_trans, geom) in query {
        if geom.poly().overlaps_aabb(glob_trans, &world_rect) {
            vec.push(ent);
        }
    }
    vec
}

/// returns all the non-geometry entities overlapping the specified world rect
pub fn get_selectable_non_geoms_in_aabb(
    world_rect: Rect,
    query: &Query<
        (Entity, &GlobalTransform, &Aabb),
        (With<EditorWorldObject>, Without<GeometryComponent>),
    >,
) -> Vec<Entity> {
    let mut vec = Vec::<Entity>::new();
    for (ent, glob_trans, aabb) in query {
        let scale = glob_trans.extract_transform_scale2();
        if rects_overlap(
            &world_rect,
            &Rect::from_center_half_size(
                glob_trans.transform_point(aabb.center.into()).truncate(),
                Vec2::new(scale.x * aabb.half_extents.x, scale.y * aabb.half_extents.y),
            ),
        ) {
            vec.push(ent);
        }
    }
    vec
}

pub fn get_mouse_ent(
    mouse: &Res<AbstractMouseState>,
    geoms: &Query<(Entity, &GlobalTransform, &GeometryComponent)>,
    non_geoms: &Query<
        (Entity, &GlobalTransform, &Aabb),
        (With<EditorWorldObject>, Without<GeometryComponent>),
    >,
    selection: &ResMut<SelectedEntities>,
) -> Option<Entity> {
    // TODO divide geometries by spatial partitions so we don't check unnecessary geometries
    if mouse.inside_ui() {
        return None;
    }
    // check for geoms overlapping mouse
    let mut mouse_ent: Option<Entity> = None;
    let click_pos = mouse.position_world();
    for (ent, glob_trans, geom) in geoms {
        let local_point = glob_trans.inverse_transform_point2(click_pos);
        if geom.poly().contains_local_point(local_point) {
            mouse_ent = Some(ent);
            // prioritize selected entities
            if selection.contains(&ent) {
                return Some(ent);
            }
        }
    }

    // check for non-geoms overlapping mouse
    for (ent, glob_trans, aabb) in non_geoms {
        let scale = glob_trans.extract_transform_scale2();
        let aabb_rect = &Rect::from_center_half_size(
            glob_trans.transform_point(aabb.center.into()).truncate(),
            Vec2::new(scale.x * aabb.half_extents.x, scale.y * aabb.half_extents.y),
        );
        if aabb_rect.contains(click_pos) {
            mouse_ent = Some(ent);
            if selection.contains(&ent) {
                return Some(ent);
            }
        }
    }
    mouse_ent
}

// Utility Math: -----------------------------------------------------------------------------------

pub trait RotateBy {
    fn rotate_by(&self, radians: f32) -> Self;
}

impl RotateBy for Vec2 {
    fn rotate_by(&self, radians: f32) -> Self {
        Self {
            x: self.x * radians.cos() - self.y * radians.sin(),
            y: self.x * radians.sin() + self.y * radians.cos(),
        }
    }
}

// Utility Systems: --------------------------------------------------------------------------------

/// iterate through each map entity and ensure it's custom data matches with the data types
/// specified in the entity listing that it corresponds to
fn enforce_entity_data_types(
    mut commands: Commands,
    data_listings: Res<UserDataListings>,
    listings: Res<MapEntityListings>,
    mut query: Query<(Entity, &MapEntity, Option<&mut UserDataComponent>)>,
) {
    let mut mismatches = 0;
    for (ent, map_ent, usr_dat) in &mut query {
        if map_ent.listing_id >= listings.entity_types.len() {
            commands.entity(ent).despawn();
            continue;
        }
        let listing = &listings.entity_types[map_ent.listing_id];
        // there is supposed to be data on the entity
        if let Some(data) = data_listings.get(&listing.user_data) {
            // so if there's no data component, add one
            if usr_dat.is_none() {
                commands.entity(ent).insert(UserDataComponent {
                    listing: listing.user_data.clone(),
                    data: data.data.clone(),
                });
                mismatches += 1;
            // or if there is a data component, ensure it matches the data listing
            } else {
                if usr_dat.unwrap().enforce_data_type(&data.data) {
                    mismatches += 1;
                }
            }
        // there shouldn't be data on the entity
        } else {
            // so if htere is a data component, remove it
            if usr_dat.is_some() {
                commands.entity(ent).remove::<UserDataComponent>();
            }
        }
    }
    if mismatches > 0 {
        println!(
            "forced {} mismatched entity data structures to realign",
            mismatches
        );
    }
}

/// iterate through each map entity and remove all entities who do not belong to a listing, and
/// then bump each entity listing id down if there are any gaps in the entity listing id
fn validate_entity_listing_ids(
    mut commands: Commands,
    mut listings: ResMut<MapEntityListings>,
    mut map_ent_query: Query<(Entity, &mut MapEntity)>,
) {
    let mut removed = 0;
    for (i, listing) in listings.entity_types.iter_mut().enumerate() {
        let gap_size = listing.id - i;
        if gap_size > 0 {
            for (ent, mut map_ent) in &mut map_ent_query {
                if map_ent.listing_id == listing.id {
                    map_ent.listing_id -= gap_size;
                } else if map_ent.listing_id >= i && map_ent.listing_id < listing.id {
                    commands.entity(ent).despawn();
                    removed += 1;
                }
            }
            listing.id -= gap_size;
        }
    }

    let max_id = (listings.entity_types.len() as i32) - 1;
    for (ent, map_ent) in &map_ent_query {
        if map_ent.listing_id as i32 > max_id {
            commands.entity(ent).despawn();
            removed += 1;
        }
    }

    if removed > 0 {
        println!("removed {} map entities with deleted listing id", removed);
    }
}

/// remove all the currentle selected entities from the world
fn remove_selected_objs(mut commands: Commands, mut selection: ResMut<SelectedEntities>) {
    // remove all entities
    for ent in selection.iter() {
        commands.entity(*ent).despawn();
    }

    // deselect all entities
    selection.clear();
}

/// reset the camera zoom to 0.5 and center at origin
fn normalize_view(
    mut cam_query: Query<(&mut Transform, &mut OrthographicProjection), With<Camera2d>>,
) {
    let (mut cam_trans, mut cam_proj) = cam_query.single_mut();
    cam_proj.scale = 0.5;
    cam_trans.translation.x = 0.0;
    cam_trans.translation.y = 0.0;
}

fn nudge_selection_right(
    selection: Res<SelectedEntities>,
    level_props: Res<LevelEditProperties>,
    mut query: Query<&mut Transform>,
) {
    let nudge_amount: f32 = level_props.grid_size;
    for ent in selection.iter() {
        if let Ok(mut trans) = query.get_mut(*ent) {
            trans.translation.x += nudge_amount;
        }
    }
}

fn nudge_selection_left(
    selection: Res<SelectedEntities>,
    level_props: Res<LevelEditProperties>,
    mut query: Query<&mut Transform>,
) {
    let nudge_amount: f32 = level_props.grid_size;
    for ent in selection.iter() {
        if let Ok(mut trans) = query.get_mut(*ent) {
            trans.translation.x -= nudge_amount;
        }
    }
}

fn nudge_selection_up(
    selection: Res<SelectedEntities>,
    level_props: Res<LevelEditProperties>,
    mut query: Query<&mut Transform>,
) {
    let nudge_amount: f32 = level_props.grid_size;
    for ent in selection.iter() {
        if let Ok(mut trans) = query.get_mut(*ent) {
            trans.translation.y += nudge_amount;
        }
    }
}

fn nudge_selection_down(
    selection: Res<SelectedEntities>,
    level_props: Res<LevelEditProperties>,
    mut query: Query<&mut Transform>,
) {
    let nudge_amount: f32 = level_props.grid_size;
    for ent in selection.iter() {
        if let Ok(mut trans) = query.get_mut(*ent) {
            trans.translation.y -= nudge_amount;
        }
    }
}

fn recenter_sel_poly_translations(
    selection: Res<SelectedEntities>,
    mut poly_query: Query<(&mut Transform, &mut GeometryComponent), With<EditorObject>>,
    props: Res<LevelEditProperties>,
) {
    for ent in &selection.entities {
        if let Ok((mut trans, mut geom)) = poly_query.get_mut(*ent) {
            if geom.poly().verts().len() <= 0 {
                continue;
            }
            // calculate the min and max bounds of the polygon
            let mut targ_loc_min = geom.poly().verts()[0];
            let mut targ_loc_max = geom.poly().verts()[0];
            for vert in geom.poly().verts() {
                targ_loc_min.x = targ_loc_min.x.min(vert.x);
                targ_loc_min.y = targ_loc_min.y.min(vert.y);
                targ_loc_max.x = targ_loc_max.x.max(vert.x);
                targ_loc_max.y = targ_loc_max.y.max(vert.y);
            }
            // get center of the bounds and use that as the centroid
            let mut targ_trans = trans.transform_point2((targ_loc_min + targ_loc_max) * 0.5);
            if props.snap_to_grid {
                targ_trans = (targ_trans / props.grid_size).round() * props.grid_size;
            }
            let dif = targ_trans - trans.translation.truncate();
            if dif.length_squared() <= 0.0 {
                continue;
            }
            // apply target translation
            trans.translation.x = targ_trans.x;
            trans.translation.y = targ_trans.y;
            // adjust vertices by offset
            for vert in geom.poly_mut().verts_mut() {
                *vert -= dif;
            }
        }
    }
}

fn select_all(
    mut selection: ResMut<SelectedEntities>,
    query: Query<
        Entity,
        (
            With<EditorObject>,
            Or<(With<GeometryComponent>, With<MapEntity>)>,
        ),
    >,
) {
    selection.entities.clear();
    for ent in &query {
        selection.entities.insert(ent);
    }
}
