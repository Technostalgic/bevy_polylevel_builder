use bevy::asset::AssetLoader;
use bevy::{ecs::system::SystemId, prelude::*};
use bevy_prototype_lyon::shapes;
use futures_lite::AsyncReadExt;
use serde::*;
use serde_json::{self, from_slice};
use std::collections::hash_map::DefaultHasher;
use std::fs::OpenOptions;
use std::hash::{Hash, Hasher};
use std::io::Read;
use std::io::Write;
use std::path::PathBuf;

use crate::core::{EditorObject, EditorWorldObject};
use crate::core::{LevelEditProperties, LEVEL_ENT_Z_ORDER};
use crate::geometry::{GeometryBundle, GeometryComponent, Polygon};
use crate::map_entity::{MapEntity, MapEntityBundle, MapEntityListing, MapEntityListings};
use crate::user_data::{
    HashIdLink, MapEntityUserDataConverter, TypedUserData, TypedUserDataFiller, UserData,
    UserDataComponent, UserDataListing, UserDataListingHandle, UserDataListings,
};

// Type Definitions: -------------------------------------------------------------------------------

pub struct SavePlugin;

pub struct LevelAssetLoader;

#[derive(Resource)]
pub struct SaveSystemIds {
    /// the save file syste id, run this system to save the current level editor state to the
    /// current file path
    pub save_file_id: SystemId,

    /// the load file id, run this system to load the current file path and build the level editor
    /// state from it. the current level editor state will remain, clear_level system should
    /// probably be run before this system, unless you are trying to load multiple levels additively
    pub load_file_id: SystemId,

    /// clear the current level editor state, remove all level related entities
    pub clear_level_id: SystemId,
}

#[derive(Asset, TypePath, Serialize, Deserialize, Clone)]
pub struct LevelData {
    pub user_data: Vec<UserDataListingData>,
    pub poly_data: PolygonCollectionData,
    pub entity_data: EntityCollectionData,
}

#[derive(TypePath, Serialize, Deserialize, Clone)]
pub struct PolygonCollectionData {
    pub polys: Vec<PolygonData>,
}

#[derive(TypePath, Serialize, Deserialize, Clone)]
pub struct PolygonData {
    pub translation: Vec2,
    pub verts: Vec<Vec2>,
    pub user_data: Option<CustomData>,
    pub id_hash: u64,
}

#[derive(TypePath, Serialize, Deserialize, Default, Clone)]
pub struct EntityCollectionData {
    pub listings: Vec<MapEntityTypeListingData>,
    pub entities: Vec<EntityData>,
}

#[derive(TypePath, Serialize, Deserialize, Clone)]
pub struct EntityData {
    pub translation: Vec2,
    pub entity_type_id: usize,
    pub user_data: Option<CustomData>,
    pub id_hash: u64,
}

#[derive(TypePath, Serialize, Deserialize, Clone)]
pub struct CustomData {
    pub listing_id: Option<usize>,
    pub data: UserData,
}

#[derive(Reflect, Serialize, Deserialize, Clone)]
pub struct MapEntityTypeListingData {
    pub label: String,
    pub icon_color: Color,
    pub icon_image: Option<PathBuf>,
    pub scale: Vec2,
    pub id: usize,
    pub usr_data: Option<usize>,
}

#[derive(TypePath, Serialize, Deserialize, Clone)]
pub struct UserDataListingData {
    pub label: String,
    pub data: TypedUserData,
}

// Implementations: --------------------------------------------------------------------------------

impl Plugin for SavePlugin {
    fn build(&self, app: &mut App) {
        let sys_ids = SaveSystemIds {
            save_file_id: app.world.register_system(save_file),
            load_file_id: app.world.register_system(load_file),
            clear_level_id: app.world.register_system(clear_level),
        };
        app.register_asset_loader(LevelAssetLoader)
            .init_asset::<LevelData>()
            .insert_resource(sys_ids);
    }
}

impl AssetLoader for LevelAssetLoader {
    type Asset = LevelData;
    type Settings = ();
    type Error = std::io::Error;
    fn load<'a>(
        &'a self,
        reader: &'a mut bevy::asset::io::Reader,
        _settings: &'a Self::Settings,
        _load_context: &'a mut bevy::asset::LoadContext,
    ) -> bevy::utils::BoxedFuture<'a, Result<Self::Asset, Self::Error>> {
        Box::pin(async move {
            let mut bytes = Vec::new();
            reader.read_to_end(&mut bytes).await?;
            let asset = from_slice::<LevelData>(&bytes).expect("unable to parse level");
            Ok(asset)
        })
    }
    fn extensions(&self) -> &[&str] {
        &["lvl.json"]
    }
}

impl PolygonData {
    fn from_polygon(
        translation: Vec2,
        poly: &Polygon,
        usr_dat: Option<&UserDataComponent>,
        ent: Entity,
    ) -> Self {
        let verts = poly.verts().clone();
        let mut hasher = DefaultHasher::new();
        ent.hash(&mut hasher);
        let usr_dat = if let Some(dat) = usr_dat {
            Some(CustomData {
                listing_id: dat.listing.get_index(),
                data: dat.data.to_map_ent_usr_dat(),
            })
        } else {
            None
        };
        Self {
            translation,
            verts,
            user_data: usr_dat.clone(),
            id_hash: hasher.finish(),
        }
    }
}

impl MapEntityTypeListingData {
    pub fn from_map_ent_listing(listing: &MapEntityListing) -> Self {
        Self {
            label: listing.label.clone(),
            id: listing.id.clone(),
            scale: listing.scale.clone(),
            icon_color: listing.icon.color.clone(),
            icon_image: if listing.icon_image == Handle::<Image>::default() {
                None
            } else {
                todo!("implement image asset path")
            },
            usr_data: listing.user_data.get_index(),
        }
    }

    pub fn to_map_ent_listing(&self) -> MapEntityListing {
        MapEntityListing {
            label: self.label.clone(),
            id: self.id.clone(),
            scale: self.scale.clone(),
            icon: Sprite {
                color: self.icon_color.clone(),
                ..Default::default()
            },
            icon_image: default(),
            user_data: if let Some(dat_ind) = self.usr_data {
                UserDataListingHandle::from_index(dat_ind)
            } else {
                UserDataListingHandle::INVALID
            },
        }
    }
}

impl UserDataListingData {
    pub fn from_listing(listing: &UserDataListing) -> Self {
        Self {
            label: listing.label.clone(),
            data: listing.data.clone(),
        }
    }

    pub fn to_listing(&self) -> UserDataListing {
        UserDataListing {
            label: self.label.clone(),
            data: self.data.clone(),
        }
    }
}

// System Definitions: -----------------------------------------------------------------------------

/// run this system to save the level data to the current specified file path
fn save_file(
    editor_props: Res<LevelEditProperties>,
    ent_listings: Res<MapEntityListings>,
    usr_dat_listings: Res<UserDataListings>,
    geom_query: Query<(
        Entity,
        &GlobalTransform,
        &GeometryComponent,
        Option<&UserDataComponent>,
    )>,
    entity_query: Query<(
        Entity,
        &GlobalTransform,
        &MapEntity,
        Option<&UserDataComponent>,
    )>,
) {
    if editor_props.cur_file_path.is_none() {
        warn!("File path not specified");
        return;
    }

    // attempt to create or open the file at the specified path
    let file_path = editor_props.cur_file_path.as_ref().unwrap();
    let file = OpenOptions::new()
        .write(true)
        .truncate(true)
        .create(true)
        .open(file_path);

    if let Ok(mut file) = file {
        // attempt to build and serialize the level
        let level_data =
            marshal_level_data(&ent_listings, &usr_dat_listings, &geom_query, &entity_query);
        let serialized_data = serde_json::to_string_pretty(&level_data);

        if let Ok(data) = serialized_data {
            // file was successfully created/opened and data was successfully serialized
            let write = file.write_all(data.as_bytes());

            // file was successfully written
            if write.is_ok() {
                println!("File Saved!");

            // write error
            } else if let Err(err) = write {
                warn!("{}", err);
            }

        // serialization error
        } else if let Err(err) = serialized_data {
            warn!("{}", err);
        }
    // file error
    } else if let Err(err) = file {
        warn!("{}", err);
    }
}

/// run this system to load the current file path and build the level editor
/// state from it. the current level editor state will remain, clear_level system should
/// probably be run before this system, unless you are trying to load multiple levels additively
fn load_file(mut commands: Commands, editor_props: Res<LevelEditProperties>) {
    if editor_props.cur_file_path.is_none() {
        warn!("File path not specified");
        return;
    }

    let file_path = editor_props.cur_file_path.as_ref().unwrap();
    let file = OpenOptions::new().read(true).open(file_path);

    if let Ok(mut file) = file {
        // attempt to read the data
        let mut file_data = Vec::<u8>::new();
        let success = file.read_to_end(&mut file_data);

        if success.is_ok() {
            // attemt to deserialize level data from the json file data and build the level from it
            let level_data = serde_json::from_slice::<LevelData>(file_data.as_slice());
            if let Ok(level_data) = level_data {
                build_level_from_data(&mut commands, &level_data);

            // deserialization error
            } else if let Err(err) = level_data {
                warn!("{}", err);
            }

        // read error
        } else {
            warn!("{}", success.unwrap_err());
        }

    // file error
    } else if let Err(err) = file {
        warn!("{}", err);
    }
}

/// load the level file at the specified data path and attempt parse level data from it. if the
/// file is successfully loaded and parsed, the level data is returned wrapped in an Option.
pub fn load_file_at(file_path: PathBuf) -> Option<LevelData> {
    // attempt to open the file
    let file = OpenOptions::new().read(true).open(file_path);
    if let Err(err) = file {
        warn!("{}", err);
        return None;
    }
    let mut file = file.unwrap();

    // attempt read the data
    let mut file_data = Vec::<u8>::new();
    if let Err(err) = file.read_to_end(&mut file_data) {
        warn!("{}", err);
        return None;
    }

    // attemt to deserialize data from the json file data and build level data from it
    let data = serde_json::from_slice::<LevelData>(file_data.as_slice());
    if let Err(err) = data {
        warn!("{}", err);
        return None;
    }

    // return the level data if success
    Some(data.unwrap())
}

/// clears all level related entities from the level editor, so that you have an empty level
fn clear_level(
    mut commands: Commands,
    query: Query<Entity, Or<(With<GeometryComponent>, With<MapEntity>)>>,
) {
    // destroy all level objects
    for ent in &query {
        commands.entity(ent).despawn();
    }
}

// Utility: ----------------------------------------------------------------------------------------

/// creates the level data structure from the supplied data
pub fn marshal_level_data(
    ent_listings: &Res<MapEntityListings>,
    usr_dat_listings: &Res<UserDataListings>,
    geom_query: &Query<(
        Entity,
        &GlobalTransform,
        &GeometryComponent,
        Option<&UserDataComponent>,
    )>,
    entity_query: &Query<(
        Entity,
        &GlobalTransform,
        &MapEntity,
        Option<&UserDataComponent>,
    )>,
) -> LevelData {
    LevelData {
        user_data: marshal_user_data_listings(&usr_dat_listings),
        poly_data: marshal_poly_collection_data(&geom_query),
        entity_data: marshal_entity_collection_data(&ent_listings, &entity_query),
    }
}

/// creates the polygon data structure form the supplied data
pub fn marshal_poly_collection_data(
    geom_query: &Query<(
        Entity,
        &GlobalTransform,
        &GeometryComponent,
        Option<&UserDataComponent>,
    )>,
) -> PolygonCollectionData {
    let mut polys = Vec::<PolygonData>::new();
    for (ent, glob_trans, poly, usr_dat) in geom_query {
        polys.push(marshal_poly_data(glob_trans, poly, usr_dat, ent));
    }
    PolygonCollectionData { polys }
}

/// create a polygon data structure from a geometry component
pub fn marshal_poly_data(
    glob_trans: &GlobalTransform,
    geom: &GeometryComponent,
    usr_dat: Option<&UserDataComponent>,
    ent: Entity,
) -> PolygonData {
    PolygonData::from_polygon(
        glob_trans.translation().truncate(),
        geom.poly(),
        usr_dat,
        ent,
    )
}

/// create entity collection structure from world data
pub fn marshal_entity_collection_data(
    ent_listings: &Res<MapEntityListings>,
    ent_query: &Query<(
        Entity,
        &GlobalTransform,
        &MapEntity,
        Option<&UserDataComponent>,
    )>,
) -> EntityCollectionData {
    let mut ents = Vec::<EntityData>::new();
    for (ent, glob_trans, ent_dat, usr_dat) in ent_query {
        ents.push(marshal_entity_data(glob_trans, ent_dat, usr_dat, ent));
    }
    EntityCollectionData {
        listings: marshal_entity_listing_data(ent_listings),
        entities: ents,
    }
}

/// create entity listing data from world resources
pub fn marshal_entity_listing_data(
    ent_listings: &Res<MapEntityListings>,
) -> Vec<MapEntityTypeListingData> {
    let mut listing_data = Vec::<MapEntityTypeListingData>::new();
    for listing in &ent_listings.entity_types {
        listing_data.push(MapEntityTypeListingData::from_map_ent_listing(listing));
    }
    listing_data
}

/// create data about user data listings from world resources
pub fn marshal_user_data_listings(
    usr_dat_listings: &Res<UserDataListings>,
) -> Vec<UserDataListingData> {
    let mut listing_data = Vec::<UserDataListingData>::new();
    for listing in &usr_dat_listings.data_listings {
        listing_data.push(UserDataListingData::from_listing(listing));
    }
    listing_data
}

/// create entity data structure from world entity components
pub fn marshal_entity_data(
    glob_trans: &GlobalTransform,
    entity: &MapEntity,
    user_data: Option<&UserDataComponent>,
    ent: Entity,
) -> EntityData {
    let id = entity.listing_id;
    let mut hasher = DefaultHasher::new();
    ent.hash(&mut hasher);
    EntityData {
        entity_type_id: id,
        translation: glob_trans.translation().truncate(),
        user_data: if let Some(typed_data) = user_data {
            Some(CustomData {
                listing_id: typed_data.listing.get_index(),
                data: typed_data.data.to_map_ent_usr_dat(),
            })
        } else {
            None
        },
        id_hash: hasher.finish(),
    }
}

/// builds a complete level editor state from a supplied level data structure
pub fn build_level_from_data(commands: &mut Commands, data: &LevelData) {
    build_usr_dat_from_data(commands, &data.user_data);
    build_geometries_from_data(commands, &data.poly_data);
    build_entities_from_data(commands, &data.entity_data);
}

/// builds the level geometry from the supplied polygon collection data
pub fn build_geometries_from_data(commands: &mut Commands, data: &PolygonCollectionData) {
    for poly in &data.polys {
        build_geometry_from_data(commands, poly);
    }
}

/// build an entity with a geometry bundle based on the supplied polygon data
pub fn build_geometry_from_data(commands: &mut Commands, data: &PolygonData) -> Entity {
    let shape = shapes::Polygon {
        closed: true,
        points: data.verts.clone(),
    };
    let mut ent_cmd = commands.spawn((
        EditorObject,
        EditorWorldObject,
        GeometryBundle::from_shape(data.translation.extend(0.0), shape),
        HashIdLink {
            hash_id: data.id_hash,
        },
    ));
    if let Some(usr_dat) = &data.user_data {
        if let Some(listing_index) = usr_dat.listing_id {
            ent_cmd.insert((
                UserDataComponent {
                    listing: UserDataListingHandle::from_index(listing_index),
                    data: default(),
                },
                TypedUserDataFiller {
                    data_untyped: usr_dat.data.clone(),
                },
            ));
        }
    }
    ent_cmd.id()
}

/// build user listings resource from data
pub fn build_usr_dat_from_data(commands: &mut Commands, data: &Vec<UserDataListingData>) {
    let mut listings: Vec<UserDataListing> = Vec::new();
    for list_dat in data {
        listings.push(list_dat.to_listing());
    }
    commands.insert_resource(UserDataListings {
        data_listings: listings,
    });
}

/// build level entities and components into world from data
pub fn build_entities_from_data(commands: &mut Commands, data: &EntityCollectionData) {
    // build listings
    let mut listings: Vec<MapEntityListing> = Vec::new();
    for listing_dat in &data.listings {
        listings.push(listing_dat.to_map_ent_listing());
    }
    // build entities
    for ent_dat in &data.entities {
        build_entity_from_data(commands, &ent_dat, &listings[ent_dat.entity_type_id]);
    }
    // insert listings
    commands.insert_resource(MapEntityListings {
        entity_types: listings,
    });
}

/// build level entity and bundle into world from supplied entity data and listing
pub fn build_entity_from_data(
    commands: &mut Commands,
    data: &EntityData,
    listing: &MapEntityListing,
) -> Entity {
    let mut ent_cmd = commands.spawn((
        MapEntityBundle {
            name: Name::new(listing.label.clone()),
            level_ent_dat: MapEntity {
                listing_id: data.entity_type_id,
            },
            sprite_bundle: SpriteBundle {
                sprite: listing.icon.clone(),
                texture: listing.icon_image.clone(),
                transform: Transform {
                    translation: data.translation.extend(LEVEL_ENT_Z_ORDER),
                    scale: listing.scale.extend(1.0),
                    ..Default::default()
                },
                ..Default::default()
            },
            ..Default::default()
        },
        HashIdLink {
            hash_id: data.id_hash,
        },
    ));
    if listing.user_data.is_valid() {
        if let Some(usr_dat) = &data.user_data {
            ent_cmd.insert((
                UserDataComponent {
                    listing: listing.user_data.clone(),
                    data: default(),
                },
                TypedUserDataFiller {
                    data_untyped: usr_dat.data.clone(),
                },
            ));
        }
    }
    ent_cmd.id()
}
