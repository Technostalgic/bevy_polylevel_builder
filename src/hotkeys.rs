use bevy::{prelude::*, utils::HashSet};
use bevy_egui::EguiContexts;

use crate::prelude::LevelEditorState;

// -------------------------------------------------------------------------------------------------

pub struct HotkeyPlugin;

#[derive(Default, Clone, Copy, PartialEq, Eq)]
pub enum KeyPressCondition {
    Any,
    Pressed,
    #[default]
    Unpressed,
}

#[derive(Default, Clone, Copy, PartialEq, Eq)]
pub struct HotkeyBinding {
    pub require_ctrl: KeyPressCondition,
    pub require_shift: KeyPressCondition,
    pub require_alt: KeyPressCondition,
    pub action_key: Option<KeyCode>,
}

#[derive(Resource, Default)]
pub struct HotkeyPollState {
    ui_focus: bool,
    held_ctrl: bool,
    held_shift: bool,
    held_alt: bool,
    held_ctrl_last: bool,
    held_shift_last: bool,
    held_alt_last: bool,
    action_keys_triggered: HashSet<KeyCode>,
    action_keys_untriggered: HashSet<KeyCode>,
    action_keys_held: HashSet<KeyCode>,
}

// -------------------------------------------------------------------------------------------------

impl Plugin for HotkeyPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(HotkeyPollState::default()).add_systems(
            PreUpdate,
            poll_hotkey_state.run_if(in_state(LevelEditorState::Editing)),
        );
    }
}

impl KeyPressCondition {
    /// returns true always if key press condition is Any, but if the condition is Pressed, the
    /// button must be pressed, if the condition is Unpressed, the button must not be pressed
    pub fn match_btn_state(&self, button_is_pressed: bool) -> bool {
        match self {
            KeyPressCondition::Any => true,
            KeyPressCondition::Pressed => button_is_pressed,
            KeyPressCondition::Unpressed => !button_is_pressed,
        }
    }
}

impl HotkeyBinding {
    /// create a new hotkey binding from an optional action key, and whether or not ctrl, shift,
    /// and/or alt key modifiers need to be pressed. If no action key is specified only the modifier
    /// keys are required to trigger this keybinding
    pub fn _new(
        action_key: Option<KeyCode>,
        ctrl: KeyPressCondition,
        shift: KeyPressCondition,
        alt: KeyPressCondition,
    ) -> Self {
        Self {
            require_ctrl: ctrl,
            require_shift: shift,
            require_alt: alt,
            action_key,
        }
    }
}

impl HotkeyPollState {
    pub fn held_ctrl(&self) -> bool {
        self.held_ctrl
    }
    pub fn held_shift(&self) -> bool {
        self.held_shift
    }
    pub fn held_alt(&self) -> bool {
        self.held_alt
    }

    /// returns true if the egui has hotkey keyboard focus
    pub fn ui_focus(&self) -> bool {
        self.ui_focus
    }

    /// returns true if one or more potential bindings was triggered this frame
    pub fn any_bindings_triggered(&self) -> bool {
        self.action_keys_triggered.len() > 0
    }

    /// returns true if one or more potential bindings are being held down this frame
    pub fn _any_bindings_held(&self) -> bool {
        self.action_keys_held.len() > 0
    }

    /// whether or not the specified key binding was triggered this frame
    pub fn is_binding_triggered(&self, binding: &HotkeyBinding) -> bool {
        // check to see if the modifiers match
        let modifiers_match = binding.require_ctrl.match_btn_state(self.held_ctrl())
            && binding.require_shift.match_btn_state(self.held_shift())
            && binding.require_alt.match_btn_state(self.held_alt());

        // check to see if action key is triggered, if available
        if !modifiers_match {
            false
        } else if let Some(action_key) = binding.action_key {
            self.action_keys_triggered.contains(&action_key)

        // if no action key is specified, we only need to check modifier keys
        } else {
            modifiers_match
        }
    }

    /// whether or not the specified key binding was released this frame
    pub fn is_binding_untriggered(&self, binding: &HotkeyBinding) -> bool {
        // check to see if the modifiers match
        let modifiers_match = binding.require_ctrl.match_btn_state(self.held_ctrl_last)
            && binding.require_shift.match_btn_state(self.held_shift_last)
            && binding.require_alt.match_btn_state(self.held_alt_last);

        // check to see if action key is triggered, if available
        if !modifiers_match {
            false
        } else if let Some(action_key) = binding.action_key {
            self.action_keys_untriggered.contains(&action_key)

        // if no action key is specified, we only need to check modifier keys
        } else {
            modifiers_match
        }
    }

    /// whether or not the specified key binding was being held down this frame
    pub fn is_binding_held(&self, binding: &HotkeyBinding) -> bool {
        // check to see if the modifiers match
        let modifiers_match = binding.require_ctrl.match_btn_state(self.held_ctrl())
            && binding.require_shift.match_btn_state(self.held_shift())
            && binding.require_alt.match_btn_state(self.held_alt());

        // check to see if action key is triggered, if available
        if !modifiers_match {
            false
        } else if let Some(action_key) = binding.action_key {
            self.action_keys_held.contains(&action_key)

        // if no action key is specified, we only need to check modifier keys
        } else {
            modifiers_match
        }
    }
}

// -------------------------------------------------------------------------------------------------

fn poll_hotkey_state(
    mut egui_ctx: EguiContexts,
    mut poll_state: ResMut<HotkeyPollState>,
    keys: Res<ButtonInput<KeyCode>>,
) {
    poll_state.ui_focus = egui_ctx.ctx_mut().wants_keyboard_input();

    // store previous frame states for hotkey modifiers
    poll_state.held_ctrl_last = poll_state.held_ctrl;
    poll_state.held_shift_last = poll_state.held_shift;
    poll_state.held_alt_last = poll_state.held_alt;

    // poll key states for hotkey modifiers
    poll_state.held_ctrl =
        keys.pressed(KeyCode::ControlLeft) || keys.pressed(KeyCode::ControlRight);
    poll_state.held_shift = keys.pressed(KeyCode::ShiftLeft) || keys.pressed(KeyCode::ShiftRight);
    poll_state.held_alt = keys.pressed(KeyCode::AltLeft) || keys.pressed(KeyCode::AltRight);

    // poll for held keys
    poll_state.action_keys_held.clear();
    for held in keys.get_pressed() {
        poll_state.action_keys_held.insert(*held);
    }

    // poll for triggered keys
    poll_state.action_keys_triggered.clear();
    for key in keys.get_just_pressed() {
        poll_state.action_keys_triggered.insert(*key);
    }

    // poll for untriggered keys
    poll_state.action_keys_untriggered.clear();
    for key in keys.get_just_released() {
        poll_state.action_keys_untriggered.insert(*key);
    }
}
