use bevy::{ecs::query::QueryIter, prelude::*, render::primitives::Aabb, sprite::Anchor};

use crate::{
    core::{EditorObject, EditorUtilityObject, PixelWorldSize},
    geometry::{GeometryComponent, TransformPoint2D},
    utility::create_dot_sprite,
};

// -------------------------------------------------------------------------------------------------

pub(crate) struct GizmoExtensionPlugin;

#[derive(Debug, Clone)]
struct DotRenderData {
    pub position: Vec2,
    pub color: Color,
    pub size: f32,
    pub z_order: f32,
}

#[derive(Debug, Clone)]
struct LineRenderData {
    pub start_position: Vec2,
    pub end_position: Vec2,
    pub color: Color,
    pub thickness: f32,
    pub z_order: f32,
}

#[derive(Debug, Clone)]
struct OutlineRenderData {
    pub entity: Entity,
    pub color: Color,
    pub thickness: f32,
    pub delta_z: f32,
}

#[derive(Debug, Clone)]
struct TextRenderData {
    pub text: String,
    pub position: Vec2,
    pub color: Color,
    pub text_anchor: Anchor,
    pub z_order: f32,
}

#[derive(Resource, Default)]
pub struct GizmosExt {
    dots_to_draw: Vec<DotRenderData>,
    lines_to_draw: Vec<LineRenderData>,
    outlines_to_draw: Vec<OutlineRenderData>,
    texts_to_draw: Vec<TextRenderData>,
}

#[derive(Component, Clone, Copy)]
struct GizmoRenderMarker;

#[derive(Component, Clone, Copy)]
struct GizmoTextRenderMarker;

type RenderIter<'w, 's, 'a, 'b, 'c> = QueryIter<
    'w,
    's,
    (&'a mut Transform, &'b mut Sprite, &'c mut Visibility),
    With<GizmoRenderMarker>,
>;

type TxtRenderIter<'w, 's, 'a, 'b, 'c, 'd> = QueryIter<
    'w,
    's,
    (
        &'a mut Transform,
        &'b mut Anchor,
        &'c mut Text,
        &'d mut Visibility,
    ),
    (With<GizmoTextRenderMarker>, Without<GizmoRenderMarker>),
>;

// -------------------------------------------------------------------------------------------------

impl Plugin for GizmoExtensionPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(GizmosExt::default())
            .add_systems(PostUpdate, handle_gizmo_ext);
    }
}

impl GizmosExt {
    /// Render a dot for this frame at the specified coordinates. The dot will exist for exactly
    /// one frame and this function will need to be called again every frame if it is to persist.
    /// the z_order parameter is just the z position in 3d world space that the dot will exist at
    pub fn draw_dot(&mut self, position: Vec2, color: Color, size: f32, z_order: f32) {
        self.dots_to_draw.push(DotRenderData {
            position,
            color,
            size,
            z_order,
        });
    }
    /// Render a line for this frame at the specified coordinates. The line will exist for exactly
    /// one frame and this function will need to be called again every frame if it is to persist.
    /// the z_order parameter is just the z position in 3d world space that the line will exist at
    pub fn draw_line(
        &mut self,
        start_position: Vec2,
        end_position: Vec2,
        color: Color,
        thickness: f32,
        z_order: f32,
    ) {
        self.lines_to_draw.push(LineRenderData {
            start_position,
            end_position,
            color,
            thickness,
            z_order,
        });
    }
    /// Render the outline of an entity for this frame. The outline will exist for exactly
    /// one frame and this function will need to be called again every frame if it is to persist.
    /// The entity will only be outlined if it has a valid component to outline, such as a geometry
    /// or map entity. delta_z parameter represents how far in front of the entity the outline
    /// should be rendered
    pub fn draw_outline(&mut self, entity: Entity, color: Color, thickness: f32, delta_z: f32) {
        self.outlines_to_draw.push(OutlineRenderData {
            entity,
            color,
            thickness,
            delta_z,
        });
    }
    /// Render an axis aligned box with the specified properties
    pub fn draw_rect(
        &mut self,
        corner_a: Vec2,
        corner_b: Vec2,
        color: Color,
        thickness: f32,
        z_order: f32,
    ) {
        let min = corner_a.min(corner_b);
        let max = corner_a.max(corner_b);
        self.draw_line(min, Vec2::new(max.x, min.y), color, thickness, z_order);
        self.draw_line(Vec2::new(max.x, min.y), max, color, thickness, z_order);
        self.draw_line(max, Vec2::new(min.x, max.y), color, thickness, z_order);
        self.draw_line(Vec2::new(min.x, max.y), min, color, thickness, z_order);
    }
    /// Render the outlone of a polygon with specified vertices
    pub fn draw_poly(
        &mut self,
        verts: impl IntoIterator<Item = Vec2>,
        closed: bool,
        color: Color,
        thickness: f32,
        z_order: f32,
    ) {
        let mut prev_vert: Option<Vec2> = None;
        let mut first_vert: Option<Vec2> = None;
        for vert in verts {
            if prev_vert.is_some() {
                self.draw_line(
                    *prev_vert.as_ref().unwrap(),
                    vert,
                    color,
                    thickness,
                    z_order,
                );
            }
            if first_vert.is_none() {
                first_vert = Some(vert);
            }
            prev_vert = Some(vert);
        }
        if closed {
            if prev_vert.is_some() {
                self.draw_line(
                    prev_vert.unwrap(),
                    first_vert.unwrap(),
                    color,
                    thickness,
                    z_order,
                );
            }
        }
    }
    /// Render a text thingy
    pub fn draw_text(
        &mut self,
        text: String,
        position: Vec2,
        color: Color,
        text_anchor: Anchor,
        z_order: f32,
    ) {
        self.texts_to_draw.push(TextRenderData {
            text,
            position,
            color,
            text_anchor,
            z_order,
        });
    }
}

impl Default for DotRenderData {
    fn default() -> Self {
        Self {
            color: Color::GREEN,
            size: 4.0,
            ..default()
        }
    }
}

// Systems: ----------------------------------------------------------------------------------------

fn handle_gizmo_ext(
    mut commands: Commands,
    pixel_size: Res<PixelWorldSize>,
    mut gizmo_ext: ResMut<GizmosExt>,
    mut rdr_query: Query<(&mut Transform, &mut Sprite, &mut Visibility), With<GizmoRenderMarker>>,
    mut txt_rdr_query: Query<
        (&mut Transform, &mut Anchor, &mut Text, &mut Visibility),
        (With<GizmoTextRenderMarker>, Without<GizmoRenderMarker>),
    >,
    outline_comp_query: Query<
        (&GlobalTransform, Option<&Aabb>, Option<&GeometryComponent>),
        With<EditorObject>,
    >,
) {
    let mut render_iter = rdr_query.iter_mut();
    let mut txt_render_iter = txt_rdr_query.iter_mut();
    // draw dot gizmos
    for dot in &gizmo_ext.dots_to_draw {
        draw_dot(&mut commands, &mut render_iter, dot, pixel_size.0);
    }
    // draw line gizmos
    for line in &gizmo_ext.lines_to_draw {
        draw_line(&mut commands, &mut render_iter, line, pixel_size.0);
    }
    // draw outline gizmos
    for outline in &gizmo_ext.outlines_to_draw {
        draw_outline(
            &mut commands,
            &mut render_iter,
            outline,
            pixel_size.0,
            &outline_comp_query,
        );
    }
    // draw texts
    for text in &gizmo_ext.texts_to_draw {
        draw_text(&mut commands, &mut txt_render_iter, text, pixel_size.0);
    }
    // hide any remaining unused renderers
    for (_, _, mut vis) in render_iter {
        *vis = Visibility::Hidden;
    }
    for (_, _, _, mut vis) in txt_render_iter {
        *vis = Visibility::Hidden;
    }
    // clear the gizmo render jobs
    gizmo_ext.dots_to_draw.clear();
    gizmo_ext.lines_to_draw.clear();
    gizmo_ext.outlines_to_draw.clear();
    gizmo_ext.texts_to_draw.clear();
}

// Utility: ----------------------------------------------------------------------------------------

fn draw_dot(commands: &mut Commands, iter: &mut RenderIter, dot: &DotRenderData, px_size: f32) {
    if let Some((mut trans, mut spr, mut vis)) = iter.next() {
        *vis = Visibility::Visible;
        trans.rotation = Quat::IDENTITY;
        spr.color = dot.color;
        spr.anchor = Anchor::Center;
        trans.scale.x = px_size * dot.size;
        trans.scale.y = px_size * dot.size;
        trans.translation.x = dot.position.x;
        trans.translation.y = dot.position.y;
        trans.translation.z = dot.z_order;
    } else {
        commands.spawn((
            EditorObject,
            EditorUtilityObject,
            GizmoRenderMarker,
            create_dot_sprite(dot.position.extend(dot.z_order), dot.size, dot.color),
        ));
    }
}

fn draw_line(commands: &mut Commands, iter: &mut RenderIter, line: &LineRenderData, px_size: f32) {
    if let Some((mut trans, mut spr, mut vis)) = iter.next() {
        *vis = Visibility::Visible;
        spr.color = line.color;
        spr.anchor = Anchor::CenterLeft;
        trans.scale.y = px_size * line.thickness;
        trans.translation.x = line.start_position.x;
        trans.translation.y = line.start_position.y;
        trans.translation.z = line.z_order;
        let dif_pos = line.end_position - line.start_position;
        trans.rotation = Quat::from_rotation_z(dif_pos.y.atan2(dif_pos.x));
        trans.scale.x = dif_pos.length();
    } else {
        let dif_pos = line.end_position - line.start_position;
        let mut spr_bund = create_dot_sprite(
            line.start_position.extend(line.z_order),
            px_size * line.thickness,
            line.color,
        );
        spr_bund.transform.rotation = Quat::from_rotation_z(dif_pos.y.atan2(dif_pos.x));
        spr_bund.transform.scale.x = dif_pos.length();
        commands.spawn((
            EditorObject,
            EditorUtilityObject,
            GizmoRenderMarker,
            spr_bund,
        ));
    }
}

fn draw_outline(
    commands: &mut Commands,
    iter: &mut RenderIter,
    outline: &OutlineRenderData,
    px_size: f32,
    query: &Query<
        (&GlobalTransform, Option<&Aabb>, Option<&GeometryComponent>),
        With<EditorObject>,
    >,
) {
    let z_pos: f32;

    // get the vertices needed for the outline
    let mut verts: Vec<Vec2> = default();
    if let Ok((glob_trans, aabb, geom)) = query.get(outline.entity) {
        z_pos = glob_trans.translation().z + outline.delta_z;
        if let Some(geom) = geom {
            for vert in geom.poly().verts() {
                verts.push(glob_trans.transform_point2(*vert));
            }
        } else if let Some(aabb) = aabb {
            verts.push(glob_trans.transform_point2(aabb.max().xy()));
            verts.push(glob_trans.transform_point2(Vec2::new(aabb.max().x, aabb.min().y)));
            verts.push(glob_trans.transform_point2(aabb.min().xy()));
            verts.push(glob_trans.transform_point2(Vec2::new(aabb.min().x, aabb.max().y)));
        } else {
            return;
        }
    } else {
        return;
    }

    // position, scale, and rotate sprites at the vertices to shape an outline
    for (i, vert) in verts.iter().enumerate() {
        let next_vert = if i + 1 < verts.len() {
            verts[i + 1]
        } else {
            verts[0]
        };
        // use existing renderers
        if let Some((mut trans, mut spr, mut vis)) = iter.next() {
            *vis = Visibility::Visible;
            spr.color = outline.color;
            spr.anchor = Anchor::CenterLeft;
            trans.scale.y = px_size * outline.thickness;
            trans.translation.x = vert.x;
            trans.translation.y = vert.y;
            trans.translation.z = z_pos;
            let vert_dif = next_vert - *vert;
            trans.rotation = Quat::from_rotation_z(vert_dif.y.atan2(vert_dif.x));
            trans.scale.x = vert_dif.length();
        // if out of renderers, create one
        } else {
            let vert_dif = next_vert - *vert;
            let mut spr_bund = create_dot_sprite(
                vert.extend(z_pos),
                px_size * outline.thickness,
                outline.color,
            );
            spr_bund.transform.rotation = Quat::from_rotation_z(vert_dif.y.atan2(vert_dif.x));
            spr_bund.transform.scale.x = vert_dif.length();
            commands.spawn((
                EditorObject,
                EditorUtilityObject,
                GizmoRenderMarker,
                spr_bund,
            ));
        }
    }
}

fn draw_text(
    commands: &mut Commands,
    iter: &mut TxtRenderIter,
    text: &TextRenderData,
    px_size: f32,
) {
    let scl = px_size;
    if let Some((mut trans, mut anchor, mut txt_comp, mut vis)) = iter.next() {
        *vis = Visibility::Visible;
        *anchor = text.text_anchor;
        trans.translation = text.position.extend(text.z_order);
        txt_comp.sections[0].value = text.text.clone();
        trans.scale.x = scl;
        trans.scale.y = scl;
    } else {
        commands.spawn((
            EditorObject,
            EditorUtilityObject,
            GizmoTextRenderMarker,
            Text2dBundle {
                text_anchor: Anchor::BottomCenter,
                transform: Transform::from_translation(text.position.extend(text.z_order))
                    .with_scale(Vec3::new(scl, scl, 1.0)),
                text: Text {
                    sections: [TextSection {
                        value: text.text.clone(),
                        style: TextStyle {
                            color: text.color,
                            font_size: 14.0,
                            ..Default::default()
                        },
                    }]
                    .to_vec(),
                    ..Default::default()
                },
                ..Default::default()
            },
        ));
    }
}
