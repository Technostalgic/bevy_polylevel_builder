use bevy::{ecs::system::SystemId, prelude::*};

use crate::{
    geometry::GeometryComponent,
    map_entity::{MapEntity, MapEntityListings},
    save::{
        build_entity_from_data, build_geometry_from_data, marshal_entity_data, marshal_poly_data,
        EntityData, PolygonData,
    },
    selection::SelectedEntities,
    user_data::{UserDataComponent, UserDataSysIds},
};

// -------------------------------------------------------------------------------------------------

pub struct ClipboardPlugin;

#[derive(Resource, Default)]
pub struct Clipboard {
    max_extra_entries: usize,
    entries: Vec<ClipboardEntry>,
}

#[derive(Clone, Default)]
pub struct ClipboardEntry {
    geometries: Vec<PolygonData>,
    entities: Vec<EntityData>,
}

#[derive(Resource)]
pub struct ClipboardSystemIds {
    pub copy_selection_id: SystemId,
    pub paste_clipboard_top_entry_id: SystemId,
}

// -------------------------------------------------------------------------------------------------

impl Plugin for ClipboardPlugin {
    fn build(&self, app: &mut App) {
        let sys_ids = ClipboardSystemIds {
            copy_selection_id: app.world.register_system(copy_selection),
            paste_clipboard_top_entry_id: app.world.register_system(paste_clipboard_top_entry),
        };

        app.insert_resource(Clipboard::default())
            .insert_resource(sys_ids);
    }
}

impl ClipboardEntry {
    /// pastes the cliboard entry into the world, returns true if any objects with user data
    /// components are spawned into the world
    pub fn paste_into_world(
        &self,
        selected_ents: &mut ResMut<SelectedEntities>,
        commands: &mut Commands,
        ent_listings: &Res<MapEntityListings>,
    ) -> bool {
        // clear selection
        selected_ents.clear();
        let mut any_usr_data = false;

        // build all clipboard geometry data and append to selection
        for poly in &self.geometries {
            selected_ents.insert(build_geometry_from_data(commands, &poly));
            // TODO check for user data on geometries pasted
        }

        for map_ent in &self.entities {
            if map_ent.entity_type_id >= ent_listings.entity_types.len() {
                warn!(
                    "Cannot paste entity: listing {} does not exist",
                    map_ent.entity_type_id
                );
                continue;
            }
            if map_ent.user_data.is_some() {
                any_usr_data = true;
            }
            let listing = &ent_listings.entity_types[map_ent.entity_type_id];
            selected_ents.insert(build_entity_from_data(commands, map_ent, &listing));
        }

        any_usr_data
    }
}

// -------------------------------------------------------------------------------------------------

fn copy_selection(
    mut clipboard: ResMut<Clipboard>,
    selection: Res<SelectedEntities>,
    geom_query: Query<(
        Entity,
        &GlobalTransform,
        &GeometryComponent,
        Option<&UserDataComponent>,
    )>,
    map_entity_query: Query<(
        Entity,
        &GlobalTransform,
        &MapEntity,
        Option<&UserDataComponent>,
    )>,
) {
    // ensure something is selected
    if selection.len() <= 0 {
        warn!("Nothing that can be copied is selected");
        return;
    }

    // create a new entry and add all polygon data to it
    let mut entry = ClipboardEntry::default();
    for ent in selection.iter() {
        if let Ok((ent, glob_trans, geom, usr_dat)) = geom_query.get(*ent) {
            let poly_data = marshal_poly_data(glob_trans, geom, usr_dat, ent);
            entry.geometries.push(poly_data);
        }
        if let Ok((ent, glob_trans, map_ent, maybe_usr_dat)) = map_entity_query.get(*ent) {
            let map_ent_data = marshal_entity_data(glob_trans, map_ent, maybe_usr_dat, ent);
            entry.entities.push(map_ent_data);
        }
    }

    // if clipboard is too large, remove entries from the bottom
    while clipboard.entries.len() > clipboard.max_extra_entries {
        clipboard.entries.remove(0);
    }

    // add entry to clipboard
    clipboard.entries.push(entry);
}

fn paste_clipboard_top_entry(
    mut commands: Commands,
    mut selected_ents: ResMut<SelectedEntities>,
    clipboard: Res<Clipboard>,
    ent_listings: Res<MapEntityListings>,
    usr_dat_sys_ids: Res<UserDataSysIds>,
) {
    let entry_count = clipboard.entries.len();
    if entry_count <= 0 {
        warn!("Clipboard contains nothing to paste");
        return;
    }
    let any_usr_dat = clipboard.entries[entry_count - 1].paste_into_world(
        &mut selected_ents,
        &mut commands,
        &ent_listings,
    );
    if any_usr_dat {
        commands.run_system(usr_dat_sys_ids.link_hash_ids);
    }
}
