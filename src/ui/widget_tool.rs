use super::LevelEditUiState;
use crate::{
    map_entity::MapEntityListings,
    tools::{
        entity::EntityToolProperties,
        polygon::PolygonToolProperties,
        select::SelectToolProperties,
        transform::{TransformMode, TransformToolProperties},
        vertex::VertexToolProperties,
        EditorTool, OnToolSwitched, SelectionModifier, SelectionModifiers,
    },
    utility::UtilSystemIds,
};
use bevy::prelude::*;
use bevy_egui::{
    egui::{self, Pos2, Ui},
    EguiContexts,
};

// -------------------------------------------------------------------------------------------------

pub fn handle_ui(
    mut commands: Commands,
    util_sys: Res<UtilSystemIds>,
    mut contexts: EguiContexts,
    mut tool: ResMut<EditorTool>,
    mut select_modifiers: ResMut<SelectionModifiers>,
    ent_listings: Res<MapEntityListings>,
    mut ui_state: ResMut<LevelEditUiState>,
    mut tool_evts: EventWriter<OnToolSwitched>,
    mut select_props: ResMut<SelectToolProperties>,
    mut poly_props: ResMut<PolygonToolProperties>,
    mut vert_props: ResMut<VertexToolProperties>,
    mut ent_props: ResMut<EntityToolProperties>,
    mut trans_props: ResMut<TransformToolProperties>,
) {
    let window = egui::Window::new("Tool");
    window
        .default_open(true)
        .default_pos(Pos2::new(150.0, 0.0))
        .default_size(bevy_egui::egui::Vec2::new(150.0, 0.0))
        .show(contexts.ctx_mut(), |ui| {
            let mut next_tool = tool.clone();
            // ui.radio_value(&mut next_tool, EditorTool::Empty, "Empty");
            ui.radio_value(&mut next_tool, EditorTool::Select, "Select");
            ui.radio_value(&mut next_tool, EditorTool::Polygon, "Polygon");
            ui.radio_value(&mut next_tool, EditorTool::Vertex, "Vertex");
            ui.radio_value(&mut next_tool, EditorTool::Entity, "Entity");
            ui.radio_value(&mut next_tool, EditorTool::Transform, "Transform");
            tool.set(next_tool, &mut tool_evts);

            ui.separator();
            ui.label(format!("{:?} Tool", *tool).to_string());

            match *tool {
                EditorTool::Select => sel_ui(
                    ui,
                    &mut commands,
                    &mut select_modifiers,
                    &mut select_props,
                    &util_sys,
                ),
                EditorTool::Polygon => poly_ui(ui, &mut poly_props),
                EditorTool::Vertex => vert_ui(ui, &mut commands, &mut vert_props),
                EditorTool::Entity => ent_ui(ui, &ent_listings, &mut ui_state, &mut ent_props),
                EditorTool::Transform => trans_ui(ui, &mut trans_props),
                _ => {}
            }
        });
}

/// handles the section of the ui for the selection tool settings
fn sel_ui(
    ui: &mut Ui,
    commands: &mut Commands,
    select_modifiers: &mut ResMut<SelectionModifiers>,
    tool_properties: &mut ResMut<SelectToolProperties>,
    util_sys: &Res<UtilSystemIds>,
) {
    ui.collapsing("Description", |ui2| {
        ui2.label(
            "Left Click to select/deselect, drag for selection rect, \
            Right Click and drag for ruler",
        );
    });
    ui.add_space(5.0);

    ui.checkbox(&mut tool_properties.render_selection, "Render Selection");
    if ui.button("Recenter").clicked() {
        commands.run_system(util_sys.recenter_selected_poly_translations);
    }

    ui.add_space(5.0);
    ui.label("Selection Modifiers");

    let mut mod_union = select_modifiers.contains(&SelectionModifier::Union);
    let mut mod_difference = select_modifiers.contains(&SelectionModifier::Difference);
    ui.checkbox(&mut mod_union, "union (ctrl)");
    ui.checkbox(&mut mod_difference, "difference (alt)");

    if mod_union {
        select_modifiers.insert(SelectionModifier::Union);
    } else {
        select_modifiers.remove(&SelectionModifier::Union);
    }

    if mod_difference {
        select_modifiers.insert(SelectionModifier::Difference);
    } else {
        select_modifiers.remove(&SelectionModifier::Difference);
    }
}

fn poly_ui(ui: &mut Ui, _tool_properties: &mut ResMut<PolygonToolProperties>) {
    ui.collapsing("Description", |ui2| {
        ui2.label(
            "Left Click to add vertices to selected polygons, \
            Left Click to place new polygon if none are selected",
        );
    });
    ui.add_space(5.0);
}

fn vert_ui(
    ui: &mut Ui,
    commands: &mut Commands,
    tool_properties: &mut ResMut<VertexToolProperties>,
) {
    ui.collapsing("Description", |ui2| {
        ui2.label(
            "Left Click and drage to select vertices, Right Click and drag to translate them",
        );
    });
    ui.add_space(5.0);
    if ui
        .button("Snap To Grid")
        .on_hover_text("Snap all currently selected vertices to world grid")
        .clicked()
    {
        commands.run_system(tool_properties.snap_selected_verts.unwrap());
    }
}

fn ent_ui(
    ui: &mut Ui,
    ent_listings: &Res<MapEntityListings>,
    ui_state: &mut ResMut<LevelEditUiState>,
    tool_props: &mut ResMut<EntityToolProperties>,
) {
    ui.collapsing("Description", |ui2| {
        ui2.label("Left Click to place selected entity");
    });
    ui.collapsing("Entity", |ui2| {
        let mut cur_sel = tool_props.cur_selected_type;
        for listing in &ent_listings.entity_types {
            ui2.radio_value(&mut cur_sel, listing.id, listing.label.clone());
        }
        tool_props.cur_selected_type = cur_sel;
        if ui2.button("Edit Entities...").clicked() {
            ui_state.show_edit_entity_listings_window = true;
        }
    });
    ui.add_space(5.0);
}

fn trans_ui(ui: &mut Ui, tool_properties: &mut ResMut<TransformToolProperties>) {
    ui.collapsing("Description", |ui| {
        ui.label("Left Click and drag to rotate/scale, Right Click to place/remove anchor point");
    });
    ui.add_space(5.0);
    ui.label("Mode");
    ui.radio_value(
        &mut tool_properties.transform_mode,
        TransformMode::Rotate,
        "Rotate",
    );
    ui.radio_value(
        &mut tool_properties.transform_mode,
        TransformMode::Scale,
        "Scale",
    );
}
