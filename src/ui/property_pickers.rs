use bevy::{prelude::*, render::primitives::Aabb};

use crate::{
    controls::{AbstractClick, AbstractMouseState},
    core::{EditorWorldObject, LevelEditorState, UI_TOOL_GIZMO_Z_ORDER},
    geometry::GeometryComponent,
    gizmo_extension::GizmosExt,
    selection::SelectedEntities,
    tools::EditorTool,
    user_data::{EntityReference, TypedUserData, UserDataComponent, Value},
};

// -------------------------------------------------------------------------------------------------

pub(super) struct PropertyPickersPlugin;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum PropertyPathNode {
    Key(String),
    Index(usize),
}

#[derive(Default, Debug, Clone, Copy, PartialEq, Eq)]
pub enum PickingType {
    #[default]
    Entity,
    LocalPosition,
    WorldPosition,
}

#[derive(Resource, Default)]
pub struct PickingEntityRef {
    pub pick_type: PickingType,
    pub prev_tool: EditorTool,
    pub entity_target: Vec<Entity>,
    pub property_path: Vec<PropertyPathNode>,
}

// -------------------------------------------------------------------------------------------------

impl Plugin for PropertyPickersPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            Update,
            (handle_selecting_entity, handle_selecting_position)
                .run_if(in_state(LevelEditorState::Editing))
                .run_if(resource_exists::<PickingEntityRef>),
        );
    }
}

// Utility: ----------------------------------------------------------------------------------------

fn get_property_mut<'a>(
    data: &'a mut TypedUserData,
    path: &Vec<PropertyPathNode>,
) -> Option<&'a mut Value> {
    if path.len() <= 0 {
        return None;
    }
    let mut iter = path.iter();
    let mut node_value = if let PropertyPathNode::Key(key) = iter.next().unwrap() {
        if let Some(node_val) = data.get_mut(key) {
            node_val
        } else {
            return None;
        }
    } else {
        return None;
    };
    for node in iter {
        node_value = match &node {
            &PropertyPathNode::Key(key) => {
                if let Value::Object(obj) = node_value {
                    let val = obj.get_mut(key);
                    if let Some(val) = val {
                        val
                    } else {
                        return None;
                    }
                } else {
                    return None;
                }
            }
            &PropertyPathNode::Index(index) => match node_value {
                Value::Array(arr) | Value::TypedArray(_, arr) => &mut arr[*index],
                _ => {
                    return None;
                }
            },
        };
    }
    Some(node_value)
}

// Systems: ----------------------------------------------------------------------------------------

fn handle_selecting_entity(
    mut commands: Commands,
    mut cur_tool: ResMut<EditorTool>,
    mouse: Res<AbstractMouseState>,
    selection: ResMut<SelectedEntities>,
    mut picker: ResMut<PickingEntityRef>,
    geoms: Query<(Entity, &GlobalTransform, &GeometryComponent)>,
    non_geoms: Query<
        (Entity, &GlobalTransform, &Aabb),
        (With<EditorWorldObject>, Without<GeometryComponent>),
    >,
    mut gizmo_ext: ResMut<GizmosExt>,
    mut usr_data_query: Query<(&mut UserDataComponent, Option<&Sprite>)>,
) {
    if picker.pick_type != PickingType::Entity {
        return;
    }
    // ensure picker is picking a valid property
    if picker.property_path.len() <= 0 {
        panic!("property picker resource is invalid!");
    }
    // set the tool to empty
    if *cur_tool != EditorTool::Empty {
        picker.prev_tool = cur_tool.clone();
        *cur_tool = EditorTool::Empty;
    }
    // only do anything if mouse is not in ui
    if mouse.inside_ui() {
        return;
    }

    // target the entity under the cursor
    let target = crate::utility::get_mouse_ent(&mouse, &geoms, &non_geoms, &selection);

    // render the outline of the targeted entity
    if let Some(target) = target {
        let mut outline_col = Color::GREEN;
        if !picker.entity_target.is_empty() {
            if let Ok((_, Some(spr))) = usr_data_query.get(picker.entity_target[0]) {
                outline_col = spr.color;
            }
        }
        gizmo_ext.draw_outline(target, outline_col, 3.0, 1.0);
    }

    // select target entity on click
    if mouse.just_released(&AbstractClick::Primary) {
        for ent in &picker.entity_target {
            if let Ok((mut usr_data, _)) = usr_data_query.get_mut(*ent) {
                let prop_val = get_property_mut(&mut usr_data.data, &picker.property_path);
                if let Some(Value::EntityReference(maybe_ent)) = prop_val {
                    if let Some(targ) = target {
                        *maybe_ent = EntityReference::Ready(targ);
                    } else {
                        *maybe_ent = EntityReference::None;
                    }
                }
            }
        }
        commands.remove_resource::<PickingEntityRef>();
        *cur_tool = picker.prev_tool;
    }
}

fn handle_selecting_position(
    mut commands: Commands,
    mut cur_tool: ResMut<EditorTool>,
    mouse: Res<AbstractMouseState>,
    mut gizmo_ext: ResMut<GizmosExt>,
    mut picker: ResMut<PickingEntityRef>,
    mut usr_data_query: Query<(&GlobalTransform, &mut UserDataComponent, Option<&Sprite>)>,
) {
    // return if not picking position
    if !matches!(
        picker.pick_type,
        PickingType::LocalPosition | PickingType::WorldPosition
    ) {
        return;
    }
    // ensure picker is picking a valid property
    if picker.property_path.len() <= 0 {
        panic!("property picker resource is invalid!");
    }
    // set the tool to empty
    if *cur_tool != EditorTool::Empty {
        picker.prev_tool = cur_tool.clone();
        *cur_tool = EditorTool::Empty;
    }
    // only do anything if mouse is not in ui
    if mouse.inside_ui() {
        return;
    }

    // get the target position at mouse
    let mut target_position = mouse.position_world_snapped();

    // get the color of the target entity (if applicable)
    let mut dot_color = Color::GREEN;
    if picker.entity_target.len() > 0 {
        if let Ok((glob_trans, _, Some(spr))) = usr_data_query.get(picker.entity_target[0]) {
            dot_color = spr.color.with_a(spr.color.a() * 0.5);
            // draw connection to entity
            gizmo_ext.draw_line(
                glob_trans.translation().truncate(),
                target_position,
                dot_color.with_a(dot_color.a() * 0.5),
                2.0,
                UI_TOOL_GIZMO_Z_ORDER,
            );
        }
    }

    // draw a dot at the target position
    gizmo_ext.draw_dot(target_position, dot_color, 5.0, UI_TOOL_GIZMO_Z_ORDER);

    // on click
    if mouse.just_released(&AbstractClick::Primary) {
        // if local position, shift target position based on selected entity position
        if picker.pick_type == PickingType::LocalPosition && picker.entity_target.len() > 0 {
            let sel_ent = picker.entity_target[0];
            if let Ok((glob_trans, _, _)) = usr_data_query.get(sel_ent) {
                target_position -= glob_trans.translation().truncate();
            }
        }
        // apply value to property at each target entity
        for ent in &picker.entity_target {
            if let Ok((_, mut usr_data, _)) = usr_data_query.get_mut(*ent) {
                let prop_val = get_property_mut(&mut usr_data.data, &picker.property_path);
                if let Some(Value::Vector(vect)) = prop_val {
                    *vect = target_position.clone();
                }
            }
        }
        commands.remove_resource::<PickingEntityRef>();
        *cur_tool = picker.prev_tool;
    }
}
