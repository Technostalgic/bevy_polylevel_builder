use bevy::prelude::*;
use bevy_egui::*;

use super::{ui_entity_obj_data, LevelEditUiState, SystemLabels};
use crate::{
    core::{EditorWorldObject, LevelEditorState},
    map_entity::MapEntityListings,
    user_data::{UserDataComponent, UserDataListing, UserDataListingHandle, UserDataListings},
};

// -------------------------------------------------------------------------------------------------

pub struct EditDataListingsPlugin;

// -------------------------------------------------------------------------------------------------

impl Plugin for EditDataListingsPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            PreUpdate,
            handle_ui
                .in_set(SystemLabels::EguiHandling)
                .after(EguiSet::BeginFrame)
                .run_if(in_state(LevelEditorState::Editing)),
        );
    }
}

// -------------------------------------------------------------------------------------------------

fn handle_ui(
    mut contexts: EguiContexts,
    mut data_listings: ResMut<UserDataListings>,
    mut ui_state: ResMut<LevelEditUiState>,
    mut ent_listings: ResMut<MapEntityListings>,
    mut usr_dat_query: Query<&mut UserDataComponent, With<EditorWorldObject>>,
) {
    if !ui_state.show_edit_data_listings_window {
        return;
    }

    let mut to_remove_listing: Option<usize> = None;
    let mut id_src = 1000;
    let window = egui::Window::new("User Data Listings");
    window
        .default_open(true)
        .default_size(bevy_egui::egui::Vec2::new(135.0, 500.0))
        .scroll2([false, true])
        .show(contexts.ctx_mut(), |ui| {
            for (i, listing) in data_listings.data_listings.iter_mut().enumerate() {
                egui::CollapsingHeader::new(listing.label.clone())
                    .id_source(id_src)
                    .show(ui, |ui| {
                        ui.horizontal(|ui| {
                            // set to_remove to signal to remove this listing if the remove button
                            // is clicked on by the user
                            if ui
                                .button("x")
                                .on_hover_text("remove this data listing")
                                .clicked()
                            {
                                to_remove_listing = Some(i);
                            }
                            ui.text_edit_singleline(&mut listing.label);
                        });
                        ui_entity_obj_data(
                            ui,
                            &mut listing.data,
                            &mut id_src,
                            super::EntityTypedDataEditableness::Editable,
                            &mut default(),
                        )
                    });
                id_src += 1;
            }

            if ui.button("Add New").clicked() {
                let len = data_listings.data_listings.len();
                data_listings.data_listings.push(UserDataListing {
                    label: format!("usr_data_{}", len),
                    data: default(),
                });
            }

            ui.add_space(8.0);
            if ui.button("Save & Close").clicked() {
                ui_state.show_edit_data_listings_window = false;
            }
        });
    // remove the specified listing if it has been set
    if let Some(removed_index) = to_remove_listing {
        for ent_listing in &mut ent_listings.entity_types {
            // invalidate all data listing handles on map entity listings that point to the removed
            // data listing
            if let Some(usr_dat_index) = ent_listing.user_data.get_index() {
                if usr_dat_index == removed_index {
                    ent_listing.user_data = UserDataListingHandle::INVALID;
                // any user data above the removed index needs to be shifted down
                } else if usr_dat_index > removed_index {
                    ent_listing.user_data = UserDataListingHandle::from_index(usr_dat_index - 1);
                }
            }
        }
        // invalidate all data listing handles that exist in the world which correspond to the
        // removed index
        for mut usr_dat in &mut usr_dat_query {
            if let Some(usr_dat_index) = usr_dat.listing.get_index() {
                if usr_dat_index == removed_index {
                    usr_dat.listing = UserDataListingHandle::INVALID;
                    usr_dat.data.clear();
                // any user data above the removed index needs to be shifted down
                } else if usr_dat_index > removed_index {
                    usr_dat.listing = UserDataListingHandle::from_index(usr_dat_index - 1);
                }
            }
        }
        // finally, remove the actual data listing
        data_listings.data_listings.remove(removed_index);
    }
}
