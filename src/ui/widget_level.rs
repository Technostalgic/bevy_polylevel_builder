use super::{ui_float_input, LevelEditPropertiesState, LevelEditUiState};
use crate::core::{EditorObject, EditorUtilityObject, EditorWorldObject};
use crate::geometry::GeometryComponent;
use crate::gizmo_extension::GizmosExt;
use crate::map_entity::MapEntity;
use crate::selection::SelectedEntities;
use crate::tools::select::SEL_HOVERED_COLOR;
use crate::tools::SelectionModifier;
use crate::ui::AsyncControlState;
use crate::{
    core::{LevelEditProperties, PixelWorldSize},
    save, tools,
};
use bevy::ecs::system::SystemId;
use bevy::prelude::*;
use bevy::tasks::{block_on, AsyncComputeTaskPool, Task};
use bevy_egui::{
    egui::{self, Pos2},
    EguiContexts,
};
use bevy_prototype_lyon::prelude::*;
use futures_lite::future;
use rfd::{AsyncFileDialog, FileHandle};

// -------------------------------------------------------------------------------------------------

#[derive(Component, Default)]
pub struct GridRenderer {
    pub octave: u8,
    last_grid_size: f32,
    last_cam_scl: f32,
    dirty_counter: u8,
}

#[derive(Component)]
pub(super) struct RfdTaskComponent {
    task: Task<Option<FileHandle>>,
}

#[derive(Component, PartialEq, Eq)]
pub(super) enum RfdTaskAction {
    _None,
    Save,
    Open,
}

#[derive(Resource)]
pub struct WidgetLevelSystemIds {
    pub save_as_id: SystemId,
    pub open_file_id: SystemId,
}

// -------------------------------------------------------------------------------------------------

impl GridRenderer {
    pub fn new(octave: u8) -> Self {
        Self {
            octave: octave,
            ..Default::default()
        }
    }
}

// -------------------------------------------------------------------------------------------------

pub(super) fn handle_ui(
    mut commands: Commands,
    mut contexts: EguiContexts,
    mut selected_entities: ResMut<SelectedEntities>,
    mut editor_props: ResMut<LevelEditProperties>,
    mut edit_ui_state: ResMut<LevelEditUiState>,
    mut gizmo_ext: ResMut<GizmosExt>,
    selection_modifiers: Res<tools::SelectionModifiers>,
    lvl_obj_query: Query<
        (Entity, Option<&GeometryComponent>, Option<&MapEntity>),
        With<EditorWorldObject>,
    >,
    sys_ids: Res<WidgetLevelSystemIds>,
) {
    let window = egui::Window::new("Level");
    window
        .default_open(true)
        .default_pos(Pos2::new(0.0, 0.0))
        .default_size(bevy_egui::egui::Vec2::new(135.0, 1080.0))
        .scroll2([false, true])
        .show(contexts.ctx_mut(), |ui| {
            // edit background color
            ui.horizontal(|ui| {
                ui.label("Background");
                let mut rgb = [
                    editor_props.background_color.r(),
                    editor_props.background_color.g(),
                    editor_props.background_color.b(),
                ];
                let resp = ui.color_edit_button_rgb(&mut rgb);
                if resp.changed() {
                    editor_props.background_color = Color::rgb(rgb[0], rgb[1], rgb[2]);
                }
            });
            // edit grid size
            ui.horizontal(|ui| {
                ui.label("Grid size");
                ui_float_input(ui, &mut editor_props.grid_size, 50.0);
            });

            // grid properties
            ui.checkbox(&mut editor_props.render_grid, "Show grid");
            ui.checkbox(&mut editor_props.snap_to_grid, "Snap to grid");
            ui.add_space(8.0);

            // save/open file dialogue
            ui.horizontal(|ui| {
                if ui.button("Save As..").clicked() {
                    commands.run_system(sys_ids.save_as_id);
                }
                if ui.button("Open..").clicked() {
                    commands.run_system(sys_ids.open_file_id);
                }
            });

            // additional config buttons
            ui.add_space(8.0);
            if ui.button("Data Listings..").clicked() {
                edit_ui_state.show_edit_data_listings_window = true;
            }
            if ui.button("Entity Listings..").clicked() {
                edit_ui_state.show_edit_entity_listings_window = true;
            }

            // geometry hierarchy:
            ui.add_space(8.0);
            ui.separator();
            ui.collapsing("Level Objects", |ui| {
                for (i, (ent, geom, map_ent)) in lvl_obj_query.iter().enumerate() {
                    let was_selected = selected_entities.contains(&ent);

                    let type_str = if geom.is_some() {
                        "Geometry"
                    } else if map_ent.is_some() {
                        "Entity"
                    } else {
                        "Object"
                    };
                    let geom_ui = ui.selectable_label(was_selected, format!("{}_{}", type_str, i));

                    // select/deselect the entity if clicked
                    if geom_ui.clicked() {
                        if !selection_modifiers.any_selection_modifiers() {
                            selected_entities.clear();
                            if !was_selected {
                                selected_entities.insert(ent);
                            }
                        } else if selection_modifiers.contains(&SelectionModifier::Union) {
                            selected_entities.insert(ent);
                        } else if selection_modifiers.contains(&SelectionModifier::Difference) {
                            selected_entities.remove(&ent);
                        }
                    }
                    // highlight object on hover
                    else if geom_ui.hovered() {
                        gizmo_ext.draw_outline(ent, SEL_HOVERED_COLOR, 4.0, 0.25);
                    }
                    // open entity context menu on right click
                    else if geom_ui.secondary_clicked() {
                        todo!()
                    }
                }
            });
        });
}

/// system that handles rendering the grid
pub fn handle_grid_rendering(
    world_px: Res<PixelWorldSize>,
    editor_props: Res<LevelEditProperties>,
    mut grid_query: Query<(
        &mut Transform,
        &mut Visibility,
        &mut GridRenderer,
        &mut Path,
        &mut Stroke,
    )>,
    cam_query: Query<(&GlobalTransform, &Camera, &OrthographicProjection), With<EditorObject>>,
) {
    let (cam_glob_trans, cam, cam_proj) = cam_query.single();

    for (mut trans, mut vis, mut grid_rdr, mut path, mut stroke) in &mut grid_query {
        if !editor_props.render_grid {
            *vis = Visibility::Hidden;
            continue;
        } else {
            *vis = Visibility::Visible;
        }

        let min = cam.ndc_to_world(cam_glob_trans, Vec3::new(-1.0, -1.0, 0.0));
        let max = cam.ndc_to_world(cam_glob_trans, Vec3::new(1.0, 1.0, 0.0));
        if min.is_none() || max.is_none() {
            return;
        }
        let min = min.unwrap();
        let max = max.unwrap();
        let width = max.x - min.x;
        let height = max.y - min.y;

        let grid_sz = editor_props.grid_size * grid_rdr.octave as f32;

        // if the grid size has changed or camera zoom level has changed, reset dirty counter
        if grid_rdr.last_grid_size != grid_sz || grid_rdr.last_cam_scl != cam_proj.scale {
            grid_rdr.dirty_counter = 0;
            grid_rdr.last_cam_scl = cam_proj.scale;
            grid_rdr.last_grid_size = grid_sz;
        }

        // values used for fading the grid transparency based on zoom level
        let min_line_px_dist = 10.0;
        let max_line_px_dist = 50.0;
        let dif_line_px_dist = max_line_px_dist - min_line_px_dist;

        // exactly one frame after dirty, recalculate values
        if grid_rdr.dirty_counter == 1 {
            // if the screen pixels between each grid cell is greater than the threshold
            if grid_sz / world_px.0 > min_line_px_dist {
                // rebuild path and replace old path with new one
                let targ_path = build_grid_path(width, height, grid_sz);
                *path = targ_path;
                *vis = Visibility::Visible;
                stroke.options.line_width = world_px.0 * 1.0;
            } else {
                *vis = Visibility::Hidden;
            }
        }

        if *vis == Visibility::Visible {
            // handle transparency fading
            let max_opacity = 0.5;
            let targ_opacity = (((grid_sz / world_px.0) - min_line_px_dist) / dif_line_px_dist)
                .min(1.0)
                * max_opacity;
            let targ_color = stroke.color.with_a(targ_opacity);
            stroke.color = targ_color;

            // position the grid to camera
            let off = Vec3::new(min.x % grid_sz, min.y % grid_sz, 1.0);
            trans.translation = cam_glob_trans.translation() - off;
        }

        // counter to track how many frames since grid renderer was marked dirty
        if grid_rdr.dirty_counter < 100 {
            grid_rdr.dirty_counter += 1;
        }
    }
}

/// resolve the rfd task if the file dialogue is open
pub(super) fn handle_rfd_task_resolution(
    mut commands: Commands,
    save_sys_ids: Res<save::SaveSystemIds>,
    mut editor_props: ResMut<LevelEditProperties>,
    mut editor_prop_state: ResMut<LevelEditPropertiesState>,
    mut query: Query<(Entity, &mut RfdTaskComponent, Option<&RfdTaskAction>)>,
) {
    for (ent, mut task, maybe_action) in &mut query {
        if let Some(val) = block_on(future::poll_once(&mut task.task)) {
            if let Some(file) = val {
                editor_props.cur_file_path = Some(file.path().to_path_buf());
            } else {
                editor_props.cur_file_path = None;
            }
            editor_prop_state.cur_file_path_state = AsyncControlState::Confirmed;

            // if there's an action specified for what to do when the path is selected then do it
            if let Some(path) = editor_props.cur_file_path.as_ref() {
                if let Some(action) = maybe_action {
                    match action {
                        RfdTaskAction::Open => {
                            println!("Opening file {:?}", path);
                            commands.run_system(save_sys_ids.clear_level_id);
                            commands.run_system(save_sys_ids.load_file_id);
                        }
                        RfdTaskAction::Save => {
                            println!("Saving file {:?}", path);
                            commands.run_system(save_sys_ids.save_file_id);
                        }
                        _ => {}
                    }
                }
            }

            commands.entity(ent).despawn();
        }
    }
}

/// opens file dialogue and saves to picked file
pub(super) fn save_as(
    mut commands: Commands,
    editor_props: ResMut<LevelEditProperties>,
    mut editor_prop_state: ResMut<LevelEditPropertiesState>,
) {
    if let Some(_path) = editor_prop_state
        .cur_file_path_state
        .validate_control_value(&editor_props.cur_file_path)
    {
        editor_prop_state.cur_file_path_state = AsyncControlState::Waiting;
        let task = AsyncComputeTaskPool::get().spawn(save_file_dialogue());
        commands.spawn((
            EditorObject,
            EditorUtilityObject,
            RfdTaskComponent { task },
            RfdTaskAction::Save,
        ));
    }
}

/// opens file dialogue and loads the picked file into the level
pub(super) fn open_file(
    mut commands: Commands,
    editor_props: ResMut<LevelEditProperties>,
    mut editor_prop_state: ResMut<LevelEditPropertiesState>,
) {
    if let Some(_path) = editor_prop_state
        .cur_file_path_state
        .validate_control_value(&editor_props.cur_file_path)
    {
        editor_prop_state.cur_file_path_state = AsyncControlState::Waiting;
        let task = AsyncComputeTaskPool::get().spawn(pick_file_dialogue());
        commands.spawn((
            EditorObject,
            EditorUtilityObject,
            RfdTaskComponent { task },
            RfdTaskAction::Open,
        ));
    }
}

// -------------------------------------------------------------------------------------------------

// utility function to build grid path
pub fn build_grid_path(width: f32, height: f32, grid_size: f32) -> Path {
    let height = height + grid_size * 2.0;

    let start_x = width * -0.5;
    let start_y = height * -0.5;

    let mut i = 0;
    let mut points = Vec::<Vec2>::new();
    let mut cur_delta = 0.0;

    // add initial point
    points.push(Vec2::new(start_x, start_y));

    // append all points for vertical lines starting from left
    while cur_delta <= width {
        let cur_x = start_x + cur_delta;
        let sign = if i % 2 <= 0 { 1.0 } else { -1.0 };
        points.push(Vec2::new(cur_x, height * sign));
        points.push(Vec2::new(cur_x + grid_size, height * sign));
        cur_delta += grid_size;
        i += 1;
    }

    // ensure path to beginning of next set of points does not intersect viewport
    let point_count = points.len();
    if point_count > 0 {
        let last_pt = points[point_count - 1];

        // if path will cross diagonally across screen, add another point in the bottom right
        // corner to avoid that
        if last_pt.y >= 0.0 {
            points.push(Vec2::new(last_pt.x, start_y));
        }
    }

    // add starting point for horizontal lines
    points.push(Vec2::new(start_x, start_y));

    // reset delta and append points for horizontal lines starting from bottom
    i = 0;
    cur_delta = 0.0;
    while cur_delta <= height {
        let cur_y = start_y + cur_delta;
        let sign = if i % 2 <= 0 { 1.0 } else { -1.0 };
        points.push(Vec2::new(width * sign, cur_y));
        points.push(Vec2::new(width * sign, cur_y + grid_size));
        cur_delta += grid_size;
        i += 1;
    }

    // build the grid polygon path from the points
    GeometryBuilder::build_as(&shapes::Polygon {
        closed: false,
        points: points,
        ..Default::default()
    })
}

fn save_file_dialogue() -> impl futures_lite::Future<Output = Option<FileHandle>> {
    AsyncFileDialog::new()
        .add_filter("level", &["lvl.json"])
        .save_file()
}

fn pick_file_dialogue() -> impl futures_lite::Future<Output = Option<FileHandle>> {
    AsyncFileDialog::new().pick_file()
}
