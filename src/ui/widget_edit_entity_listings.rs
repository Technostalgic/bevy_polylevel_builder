use bevy::{prelude::*, sprite::Anchor};
use bevy_egui::{egui::Ui, *};

use super::{ui_data_listing_selector, ui_vec2_input, LevelEditUiState, SystemLabels};
use crate::{
    core::LevelEditorState,
    map_entity::{MapEntityListing, MapEntityListings},
    user_data::UserDataListings,
    utility::UtilSystemIds,
};

// -------------------------------------------------------------------------------------------------

const SELECTABLE_ANCHOR_VARIANTS: [Anchor; 9] = [
    Anchor::TopLeft,
    Anchor::TopCenter,
    Anchor::TopRight,
    Anchor::CenterLeft,
    Anchor::Center,
    Anchor::CenterRight,
    Anchor::BottomLeft,
    Anchor::BottomCenter,
    Anchor::BottomRight,
];

pub struct EditEntListingsPlugin;

// -------------------------------------------------------------------------------------------------

impl Plugin for EditEntListingsPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            PreUpdate,
            handle_ui
                .in_set(SystemLabels::EguiHandling)
                .after(EguiSet::BeginFrame)
                .run_if(in_state(LevelEditorState::Editing)),
        );
    }
}

// Systems: ----------------------------------------------------------------------------------------

fn handle_ui(
    mut commands: Commands,
    util_sys_ids: Res<UtilSystemIds>,
    mut contexts: EguiContexts,
    mut ui_state: ResMut<LevelEditUiState>,
    mut ent_listings: ResMut<MapEntityListings>,
    data_listings: Res<UserDataListings>,
) {
    if !ui_state.show_edit_entity_listings_window {
        return;
    }
    let window = egui::Window::new("Entity Listings");
    let mut id_source = 1000;
    window
        .default_open(true)
        .default_size(bevy_egui::egui::Vec2::new(135.0, 500.0))
        .scroll2([false, true])
        .show(contexts.ctx_mut(), |ui| {
            let mut to_remove: Option<usize> = None;
            for mut listing in &mut ent_listings.entity_types {
                let remove = ui_entity_listing(ui, &data_listings, &mut listing, &mut id_source);
                if remove {
                    to_remove = Some(listing.id);
                }
            }
            if let Some(to_remove) = to_remove {
                ent_listings.entity_types.remove(to_remove);
                commands.run_system(util_sys_ids.validate_entity_listing_ids);
            }
            if ui.button("Add New").clicked() {
                let mut new_listing = MapEntityListing::default();
                new_listing.scale = Vec2::new(10.0, 10.0);
                new_listing.label = "new_entity".to_string();
                new_listing.id = ent_listings.entity_types.len();
                ent_listings.entity_types.push(new_listing);
            }
            ui.add_space(8.0);
            if ui.button("Save & Close").clicked() {
                ui_state.show_edit_entity_listings_window = false;
                commands.run_system(util_sys_ids.enforce_entity_data_types);
            }
        });
}

// Utility: ----------------------------------------------------------------------------------------

/// create ui for a entity listing, returns if the listing should be removed or not
fn ui_entity_listing(
    ui: &mut Ui,
    data_listings: &Res<UserDataListings>,
    listing: &mut MapEntityListing,
    id_source: &mut u32,
) -> bool {
    let mut to_remove = false;
    egui::containers::CollapsingHeader::new(format!(
        "{}, id: {}",
        listing.label.clone(),
        listing.id
    ))
    .id_source(id_source.clone())
    .show(ui, |ui| {
        ui.horizontal(|ui| {
            if ui
                .button("x")
                .on_hover_text("Remove entity listing")
                .clicked()
            {
                to_remove = true;
            }
            ui.text_edit_singleline(&mut listing.label);
        });
        ui.horizontal(|ui| {
            ui.label("Size");
            ui_vec2_input(ui, &mut listing.scale, 100.0);
        });
        ui.horizontal(|ui| {
            ui.label("Color");
            let mut rgba = [
                listing.icon.color.r(),
                listing.icon.color.g(),
                listing.icon.color.b(),
                listing.icon.color.a(),
            ];
            ui.color_edit_button_rgba_unmultiplied(&mut rgba);
            listing.icon.color = Color::rgba(rgba[0], rgba[1], rgba[2], rgba[3]);
        });
        ui.horizontal(|ui| {
            ui.label("Anchor");
            let mut selection = selectable_anchor_index(&listing.icon.anchor);
            egui::containers::ComboBox::from_id_source(435).show_index(
                ui,
                &mut selection,
                SELECTABLE_ANCHOR_VARIANTS.len(),
                |i| format!("{:?}", SELECTABLE_ANCHOR_VARIANTS[i]),
            );
            listing.icon.anchor = SELECTABLE_ANCHOR_VARIANTS[selection];
        });
        ui.horizontal(|ui| {
            ui.label("Data");
            ui_data_listing_selector(
                ui,
                &mut listing.user_data,
                id_source,
                &data_listings.data_listings,
            );
        });
    });
    *id_source += 1;
    to_remove
}

/// get the index of the [`Anchor`] type variant in the [`SELECTABLE_ANCHOR_VARIANTS`] array
fn selectable_anchor_index(anchor: &Anchor) -> usize {
    match anchor {
        Anchor::TopLeft => 0,
        Anchor::TopCenter => 1,
        Anchor::TopRight => 2,
        Anchor::CenterLeft => 3,
        Anchor::Center => 4,
        Anchor::CenterRight => 5,
        Anchor::BottomLeft => 6,
        Anchor::BottomCenter => 7,
        Anchor::BottomRight => 8,
        _ => 4,
    }
}
