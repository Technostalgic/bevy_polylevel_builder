use bevy::prelude::*;
use bevy_egui::{
    egui::{Color32, Pos2},
    *,
};

use crate::{
    core::LevelEditorState,
    geometry::GeometryComponent,
    map_entity::{MapEntity, MapEntityListings},
    selection::SelectedEntities,
    user_data::{UserDataComponent, UserDataListings},
};

use super::{ui_data_listing_selector, ui_entity_obj_data, ui_vec2_input, SystemLabels};

// -------------------------------------------------------------------------------------------------

pub(super) struct EditSelectionWidgetPlugin;

// -------------------------------------------------------------------------------------------------

impl Plugin for EditSelectionWidgetPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            PreUpdate,
            handle_window
                .in_set(SystemLabels::EguiHandling)
                .after(EguiSet::BeginFrame)
                .run_if(in_state(LevelEditorState::Editing)),
        );
    }
}

// -------------------------------------------------------------------------------------------------

fn handle_window(
    mut commands: Commands,
    mut egui_ctxs: EguiContexts,
    selection: Res<SelectedEntities>,
    data_listings: Res<UserDataListings>,
    ent_listings: Res<MapEntityListings>,
    mut query: Query<(
        Entity,
        &mut Transform,
        Option<&mut GeometryComponent>,
        Option<&MapEntity>,
        Option<&mut UserDataComponent>,
    )>,
) {
    if selection.len() != 1 {
        return;
    }

    // try to get an object with a user data component
    let mut id_source = 0;
    let selected_ent = *selection.iter().next().unwrap();
    let query_itm = query.get_mut(selected_ent);
    if query_itm.is_err() {
        return;
    }
    let (ent, mut trans, geom, map_ent, usr_dat) = query_itm.unwrap();

    let window = egui::Window::new("Selected Object")
        .scroll2([false, true])
        .default_pos(Pos2::new(1920.0, 0.0))
        .default_open(true);
    window.show(egui_ctxs.ctx_mut(), |ui| {
        ui.label("Selected Object:");
        ui.indent(id_source.clone(), |ui| {
            id_source += 1;
            ui.horizontal(|ui| {
                ui.label("position");
                let mut pos2 = Vec2::new(trans.translation.x, trans.translation.y);
                ui_vec2_input(ui, &mut pos2, 150.0);
                trans.translation.x = pos2.x;
                trans.translation.y = pos2.y;
            });
            // if it's a map entity, show map entity properties
            if let Some(map_ent) = map_ent {
                if map_ent.listing_id < ent_listings.entity_types.len() {
                    let listing = &ent_listings.entity_types[map_ent.listing_id];
                    let color = Color32::from_rgba_unmultiplied(
                        (listing.icon.color.r() * 256.0) as u8,
                        (listing.icon.color.g() * 256.0) as u8,
                        (listing.icon.color.b() * 256.0) as u8,
                        255 as u8,
                    );
                    ui.label("Entity:");
                    ui.indent(id_source.clone(), |ui| {
                        id_source += 1;
                        ui.colored_label(color, format!("{} (id {})", listing.label, listing.id));
                    });
                }
            // if it's a polygon, show polygon properties
            } else if let Some(mut geom) = geom {
                // show ui for vertices
                ui.collapsing("Vertices", |ui| {
                    for vert in geom.poly_mut().verts_mut() {
                        ui_vec2_input(ui, vert, 100.0);
                    }
                });
                // if no data, show option to add user data
                if usr_dat.is_none() {
                    if ui.button("Add User Data").clicked() {
                        commands.entity(ent).insert(UserDataComponent::default());
                    }
                }
            }
            // if there is a data component, show ui for it
            if let Some(mut usr_dat) = usr_dat {
                ui.horizontal(|ui| {
                    // if it's a map entity, only show data and data type listing
                    ui.label("Data:");
                    if map_ent.is_some() {
                        if let Some(dat_listing) = data_listings.get(&usr_dat.listing) {
                            ui.label(format!("{}", dat_listing.label));
                        }
                    // if it's not an entity, allow user to select whatever user data they need
                    } else {
                        let resp = ui_data_listing_selector(
                            ui,
                            &mut usr_dat.listing,
                            &mut id_source,
                            &data_listings.data_listings,
                        );
                        // adjust user data to match selected listing
                        if resp.changed() {
                            if let Some(dat_listing) = data_listings.get(&usr_dat.listing) {
                                usr_dat.enforce_data_type(&dat_listing.data);
                            } else {
                                usr_dat.data = default();
                            }
                        }
                    }
                });
                ui.indent(id_source.clone(), |ui| {
                    // if not a map entity, allow user to remove data
                    if map_ent.is_none() {
                        if ui
                            .button("Remove")
                            .on_hover_text("Remove the user data from the object")
                            .clicked()
                        {
                            commands.entity(ent).remove::<UserDataComponent>();
                        }
                    }
                    let data = &mut usr_dat.data;
                    id_source += 1;
                    let picking = ui_entity_obj_data(
                        ui,
                        data,
                        &mut id_source,
                        super::EntityTypedDataEditableness::Tweakable,
                        &mut default(),
                    );
                    if let Some(mut picking) = picking {
                        picking.entity_target = selection.entities.clone().into_iter().collect();
                        commands.insert_resource(picking);
                    }
                });
            }
        });
    });
}
