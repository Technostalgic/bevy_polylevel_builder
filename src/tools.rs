pub mod entity;
pub mod polygon;
pub mod select;
pub mod transform;
pub mod vertex;

use bevy::{prelude::*, sprite::Anchor, utils::HashSet};
use entity::*;
use polygon::*;
use select::*;
use vertex::*;

use crate::{
    controls::AbstractDragState,
    core::{
        EditorObject, EditorUtilityObject, LevelEditorState, PixelWorldSize,
        UI_SEL_RECT_GIZMO_Z_ORDER,
    },
};

use self::transform::TransformToolPlugin;

// -------------------------------------------------------------------------------------------------

const POLY_ADD_COLOR: Color = Color::rgb(0.3, 0.5, 1.0);

pub struct ToolPlugin;

#[derive(Event)]
pub struct OnToolSwitched {
    pub previous_tool: EditorTool,
}

#[derive(Resource, Clone, Copy, PartialEq, Eq, Default, Debug)]
pub enum EditorTool {
    #[default]
    Empty,
    Select,
    Polygon,
    Vertex,
    Entity,
    Transform,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub enum SelectionModifier {
    Union,
    Difference,
}

#[derive(Clone, Deref, DerefMut, Default, Resource)]
pub struct SelectionModifiers {
    pub modifiers: HashSet<SelectionModifier>,
}

#[derive(Default, Resource)]
pub struct ToolState {
    pub cached_tool: Option<EditorTool>,
}

#[derive(Component)]
struct SelectRectRenderMarker;

#[derive(Default, Resource)]
pub struct SelectionRectGraphic {
    pub visible: bool,
    pub start_pos_world: Vec2,
    pub end_pos_world: Vec2,
}

// Implementations: --------------------------------------------------------------------------------

impl Plugin for ToolPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins((
            SelectToolPlugin,
            PolygonToolPlugin,
            VertexToolPlugin,
            EntityToolPlugin,
            TransformToolPlugin,
        ))
        .insert_resource(ToolState::default())
        .insert_resource(SelectionModifiers::default())
        .insert_resource(SelectionRectGraphic::default())
        .add_event::<OnToolSwitched>()
        .add_systems(
            OnEnter::<LevelEditorState>(LevelEditorState::Editing),
            setup,
        )
        .add_systems(
            Update,
            (handle_events, render_select_rect).run_if(in_state(LevelEditorState::Editing)),
        );
    }
}

impl EditorTool {
    /// returns an event that must be sent through an event reader if it's the main app tool being
    /// set, so that we get the tool switched event whenever the tool is switched
    pub fn set(&mut self, tool: EditorTool, evt_sender: &mut EventWriter<OnToolSwitched>) -> bool {
        if *self == tool {
            return false;
        }
        evt_sender.send(OnToolSwitched {
            previous_tool: *self,
        });
        *self = tool;
        true
    }
}

impl SelectionModifiers {
    pub fn any_selection_modifiers(&self) -> bool {
        self.modifiers.contains(&SelectionModifier::Union)
            || self.modifiers.contains(&SelectionModifier::Difference)
    }
}

// Tool Systems: -----------------------------------------------------------------------------------

fn setup(mut commands: Commands) {
    // select rect renderers x4
    for _ in 0..4 {
        commands.spawn((
            EditorObject,
            EditorUtilityObject,
            SelectRectRenderMarker,
            SpriteBundle {
                sprite: Sprite {
                    color: Color::GREEN,
                    ..Default::default()
                },
                visibility: Visibility::Hidden,
                ..Default::default()
            },
        ));
    }
}

fn handle_events(
    mut commands: Commands,
    mut tool_evts: EventReader<OnToolSwitched>,
    sel_sys_ids: Res<SelectToolSystemIds>,
) {
    for evt in tool_evts.read() {
        // run render cleanup system when switched away from each corresponding tool
        match evt.previous_tool {
            EditorTool::Select => commands.run_system(sel_sys_ids.clean_render),
            EditorTool::Polygon => {}
            EditorTool::Vertex => {}

            // if switched from any other tool, do nothing
            _ => {}
        }
    }
}

/// renders the selection tool stuff
fn render_select_rect(
    select_rect: Res<SelectionRectGraphic>,
    mouse_drag: Res<AbstractDragState>,
    world_px: Res<PixelWorldSize>,
    mut query: Query<(&mut Transform, &mut Sprite, &mut Visibility), With<SelectRectRenderMarker>>,
) {
    // hide and return graphic if not visible
    if !select_rect.visible {
        for (_, _, mut vis) in &mut query {
            *vis = Visibility::Hidden;
        }
        return;
    }

    // render selection rect
    let z_order = UI_SEL_RECT_GIZMO_Z_ORDER;
    let sel_rect = Rect::from_corners(mouse_drag.start_pos_world(), mouse_drag.end_pos_world());
    for (i, (mut trans, mut spr, mut vis)) in query.iter_mut().enumerate() {
        *vis = Visibility::Visible;
        spr.anchor = match i {
            0 => Anchor::CenterLeft,   // top edge
            1 => Anchor::BottomCenter, // right edge
            2 => Anchor::CenterRight,  // bottom edge
            3 => Anchor::TopCenter,    // left edge
            _ => Anchor::Center,
        };
        trans.translation = match i / 2 {
            0 => sel_rect.min.extend(z_order),
            1 => sel_rect.max.extend(z_order),
            _ => Vec3::ZERO,
        };
        trans.scale = match i % 2 {
            0 => Vec2::new(sel_rect.width(), world_px.0 * 2.0),
            1 => Vec2::new(world_px.0 * 2.0, sel_rect.height()),
            _ => Vec2::ZERO,
        }
        .extend(1.0);
    }
}
