use bevy::{prelude::*, window::WindowResolution};
use bevy_poly_level::prelude::{BevyLevelEditPlugin, LevelEditorState};

// -------------------------------------------------------------------------------------------------

fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins.set(WindowPlugin {
                primary_window: Some(Window {
                    resolution: WindowResolution::new(1280.0, 720.0),
                    decorations: true,
                    resizable: true,
                    title: "Level Editor".to_string(),
                    ..Default::default()
                }),
                ..Default::default()
            }),
            BevyLevelEditPlugin::new(true, true, Some(KeyCode::F5)),
        ))
        .insert_resource(Msaa::Sample4)
        .add_systems(Startup, setup)
        .run();
}

fn setup(mut state: ResMut<NextState<LevelEditorState>>) {
    state.0 = Some(LevelEditorState::Editing);
}
