use bevy::{prelude::*, utils::HashSet};

use crate::{
    core::{EditorObject, UI_PROPERTY_GIZMO_Z_ORDER},
    gizmo_extension::GizmosExt,
    map_entity::{MapEntity, MapEntityListings},
    prelude::LevelEditorState,
    tools::{SelectionModifier, SelectionModifiers},
    user_data::{EntityReference, UserDataComponent, Value},
};

// -------------------------------------------------------------------------------------------------

pub const SEL_SELECTED_COLOR: Color = Color::Rgba {
    red: 0.0,
    green: 0.75,
    blue: 0.0,
    alpha: 1.0,
};

pub struct SelectionPlugin;

#[derive(Clone, Default, Resource, Deref, DerefMut)]
pub struct SelectedEntities {
    pub entities: HashSet<Entity>,
}

// -------------------------------------------------------------------------------------------------

impl Plugin for SelectionPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(SelectedEntities::default())
            .add_systems(
                Update,
                (
                    render_selected_entity_outlines,
                    render_selected_user_data_props,
                )
                    .run_if(in_state(LevelEditorState::Editing)),
            );
    }
}

impl SelectedEntities {
    /// modify the selection, adding or removing the specified objects from the set based on the
    /// given selection modifiers
    pub fn modify_selection<T: IntoIterator<Item = Entity>>(
        &mut self,
        target_ents: T,
        modifiers: &Res<SelectionModifiers>,
    ) {
        if modifiers.contains(&SelectionModifier::Union) {
            for ent in target_ents {
                self.insert(ent);
            }
        } else if modifiers.contains(&SelectionModifier::Difference) {
            for ent in target_ents {
                self.remove(&ent);
            }
        } else {
            self.clear();
            for ent in target_ents {
                self.insert(ent);
            }
        }
    }
}

// -------------------------------------------------------------------------------------------------

fn render_selected_entity_outlines(
    mut gizmo_ext: ResMut<GizmosExt>,
    selection: Res<SelectedEntities>,
    transform_query: Query<&GlobalTransform, With<EditorObject>>,
) {
    for ent in &selection.entities {
        gizmo_ext.draw_outline(*ent, SEL_SELECTED_COLOR, 2.0, 1.0);
        if let Ok(trans) = transform_query.get(*ent) {
            gizmo_ext.draw_dot(trans.translation().truncate(), SEL_SELECTED_COLOR, 5.0, 1.0);
        }
    }
}

fn render_selected_user_data_props(
    mut gizmo_ext: ResMut<GizmosExt>,
    ent_listings: Res<MapEntityListings>,
    selection: Res<SelectedEntities>,
    usr_dat_query: Query<
        (&GlobalTransform, &UserDataComponent, Option<&MapEntity>),
        With<EditorObject>,
    >,
    target_query: Query<&GlobalTransform, (With<Transform>, With<EditorObject>)>,
) {
    for ent in &selection.entities {
        if let Ok((glob_trans, usr_dat, maybe_map_ent)) = usr_dat_query.get(*ent) {
            let mut prop_color = Color::WHITE;
            if let Some(map_ent) = maybe_map_ent {
                if map_ent.listing_id < ent_listings.entity_types.len() {
                    prop_color = ent_listings.entity_types[map_ent.listing_id].icon.color;
                }
            }
            for (_, prop_val) in &usr_dat.data {
                match prop_val {
                    Value::EntityReference(ent_ref) => {
                        if let EntityReference::Ready(ent_val) = ent_ref {
                            if let Ok(targ_glob_trans) = target_query.get(*ent_val) {
                                draw_ent_ref_prop(
                                    &mut gizmo_ext,
                                    glob_trans,
                                    targ_glob_trans,
                                    *ent_val,
                                    prop_color,
                                );
                            }
                        }
                    }
                    Value::Vector(vec_val) => {
                        gizmo_ext.draw_line(
                            glob_trans.translation().truncate(),
                            *vec_val,
                            prop_color.with_a(0.25),
                            1.0,
                            UI_PROPERTY_GIZMO_Z_ORDER,
                        );
                        gizmo_ext.draw_dot(
                            *vec_val,
                            prop_color.with_a(0.5),
                            6.0,
                            UI_PROPERTY_GIZMO_Z_ORDER,
                        );
                    }
                    Value::TypedArray(t, arr) => match **t {
                        Value::EntityReference(_) => {
                            for ent_ref in arr {
                                if let Value::EntityReference(EntityReference::Ready(ent_val)) =
                                    ent_ref
                                {
                                    if let Ok(targ_glob_trans) = target_query.get(*ent_val) {
                                        draw_ent_ref_prop(
                                            &mut gizmo_ext,
                                            glob_trans,
                                            targ_glob_trans,
                                            *ent_val,
                                            prop_color,
                                        );
                                    }
                                }
                            }
                        }
                        Value::Vector(_) => {
                            for vec_val in arr.windows(2) {
                                if let Value::Vector(vec_val_cur) = vec_val[0] {
                                    if let Value::Vector(vec_val_next) = vec_val[1] {
                                        gizmo_ext.draw_line(
                                            vec_val_cur,
                                            vec_val_next,
                                            prop_color.with_a(0.25),
                                            1.0,
                                            UI_PROPERTY_GIZMO_Z_ORDER,
                                        );
                                    }
                                    gizmo_ext.draw_dot(
                                        vec_val_cur,
                                        prop_color.with_a(0.5),
                                        6.0,
                                        UI_PROPERTY_GIZMO_Z_ORDER,
                                    );
                                }
                            }
                            if let Some(Value::Vector(vec_val)) = arr.last() {
                                gizmo_ext.draw_dot(
                                    *vec_val,
                                    prop_color.with_a(0.5),
                                    6.0,
                                    UI_PROPERTY_GIZMO_Z_ORDER,
                                );
                            }
                        }
                        _ => {}
                    },
                    _ => {
                        continue;
                    }
                }
            }
        }
    }
}

// -------------------------------------------------------------------------------------------------

fn draw_ent_ref_prop(
    gizmo_ref: &mut ResMut<GizmosExt>,
    from: &GlobalTransform,
    to: &GlobalTransform,
    targ_ent: Entity,
    color: Color,
) {
    gizmo_ref.draw_line(
        from.translation().truncate(),
        to.translation().truncate(),
        color.with_a(0.25),
        1.0,
        UI_PROPERTY_GIZMO_Z_ORDER,
    );
    gizmo_ref.draw_outline(targ_ent, color, 1.0, 0.25);
}
