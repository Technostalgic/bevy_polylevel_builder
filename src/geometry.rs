use std::usize;

use bevy::prelude::*;
use bevy_prototype_lyon::prelude::*;

use crate::core::{LevelEditorState, PixelWorldSize, LEVEL_GEOM_Z_ORDER};

// -------------------------------------------------------------------------------------------------

// pub type Angle = Unit<Complex<f32>>;

pub struct GeometryPlugin;

#[derive(Component)]
pub struct GeometryComponent {
    poly: Polygon,
    render_path_dirty: bool,
}

#[derive(Bundle)]
pub struct GeometryBundle {
    pub geometry: GeometryComponent,
    pub shape_bundle: ShapeBundle,
    pub fill: Fill,
    pub stroke: Stroke,
}

#[derive(Default)]
pub struct Polygon {
    verts: Vec<Vec2>,
}

// -------------------------------------------------------------------------------------------------

pub trait ToFromVec2 {
    fn to_vec2(&self) -> Vec2;
    fn from_vec2(value: &Vec2) -> Self;
}

pub trait ToFromPolygon {
    fn to_polygon(&self) -> Polygon;
    fn from_polygon(value: &Polygon) -> Self;
}

pub trait TransformPoint2D {
    /// transform a 2d point to global space from local space
    fn transform_point2(&self, point_2d: Vec2) -> Vec2;

    /// transform a 2d point to local space from global space
    fn inverse_transform_point2(&self, point_2d: Vec2) -> Vec2;

    /// create a world space rect from two points in local space
    fn transform_rect_from_points(&self, local_pt_a: Vec2, local_pt_b: Vec2) -> Rect;

    /// create a local space rect from two points in global space
    fn inverse_transform_rect_from_points(&self, glob_pt_a: Vec2, glob_pt_b: Vec2) -> Rect;

    /// transforms point 0,0 and 1,1 and figures what the scale used to transform them was
    fn extract_transform_scale2(&self) -> Vec2;
}

// -------------------------------------------------------------------------------------------------

impl Plugin for GeometryPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            PostUpdate,
            handle_geom_render_path.run_if(in_state(LevelEditorState::Editing)),
        );
    }
}

impl Polygon {
    pub fn new(verts: Vec<Vec2>) -> Self {
        Polygon { verts }
    }

    pub fn verts(&self) -> &Vec<Vec2> {
        &self.verts
    }

    pub fn verts_mut(&mut self) -> &mut Vec<Vec2> {
        &mut self.verts
    }

    /// calculate the point on the edge that is closest to the specified local point, the edge
    /// index that it lies on, and the distance of that point from the specified point.
    /// Result is formatted as: (closest_point, edge_index, closest_distance_squared)
    /// Note that this point should be in local space, not world space. You must inverse-transform
    /// the point before use if it's a world-space point
    pub fn closest_local_edge_point(&self, local_point: Vec2) -> (Vec2, usize, f32) {
        let mut result: Option<(Vec2, usize, f32)> = None;

        let verts = &self.verts;
        let vert_count = verts.len();
        for i in 0..vert_count {
            let cur_vert = verts[i];
            let nex_vert = verts[(i + 1) % vert_count];

            let edge_vect = nex_vert - cur_vert;
            let dif_point = local_point - cur_vert;
            let mut edge_point = dif_point.project_onto(edge_vect);

            // clamp the edge point to the edge beginning and end
            if edge_point.x.signum() != edge_vect.x.signum()
                || edge_point.y.signum() != edge_vect.y.signum()
            {
                edge_point = Vec2::ZERO;
            } else if edge_point.length_squared() > edge_vect.length_squared() {
                edge_point = edge_vect;
            }

            // store the result as current dist if it's closer
            let edge_dist_sq = dif_point.distance_squared(edge_point);
            if let Some(res) = result.as_ref() {
                if res.2 > edge_dist_sq {
                    result = Some((edge_point + cur_vert, i, edge_dist_sq));
                }
            } else {
                result = Some((edge_point + cur_vert, i, edge_dist_sq));
            }
        }

        // return the result or the point itself if there is no result
        if let Some(result) = result {
            result
        } else {
            (local_point, usize::MAX, 0.0)
        }
    }

    /// returns the vertex which is closest to the specified point in local space.
    /// result is formated as (vertex_index, distance_squared)
    pub fn closest_vertex(&self, local_point: Vec2) -> (usize, f32) {
        if self.verts.len() <= 0 {
            panic!("polygon has no vertices");
        }

        let mut index: usize = 0;
        let mut dist_sq: f32 = local_point.distance_squared(self.verts[0]);

        for (i, vert) in self.verts.iter().enumerate() {
            let dsq = local_point.distance_squared(*vert);
            if dsq < dist_sq {
                index = i;
                dist_sq = dsq;
            }
        }

        (index, dist_sq)
    }

    /// Check whether or not the polygon contains the specified point. Note that this point should
    /// be in local space, not world space. You must inverse-transform the point before use if
    /// it's a world-space point
    pub fn contains_local_point(&self, local_point: Vec2) -> bool {
        // keep track of how many times the polygon edges wrap around the point
        let mut wrap_count = 0;

        // loop through each pair of vertices (edges)
        let verts = &self.verts;
        let vert_count = verts.len();
        for i in 0..vert_count {
            let cur_vert = verts[i];
            let nex_vert = verts[(i + 1) % vert_count];

            // determine whether or not the edge crosses the point along the y-axis
            let crosses_y =
                (local_point.y - cur_vert.y).signum() != (local_point.y - nex_vert.y).signum();
            if crosses_y {
                // a wrap occurs if the intersection between a horizontal line at the point's y
                // coordinate and the edge is to the right of (or equal to) the point
                let min_x = cur_vert.x.min(nex_vert.x);
                if min_x >= local_point.x {
                    wrap_count += 1;
                    continue;
                }
                let max_x = cur_vert.x.max(nex_vert.x);
                if max_x < local_point.x {
                    continue;
                }

                // simple cases are accounted for, now if the intersecion point lies inside the
                // edge's aabb, things are a bit more complex

                // slop-intercept form of edge, treating point as origin
                let cur_dif_vert = cur_vert - local_point;
                let nex_dif_vert = nex_vert - local_point;
                let edge_dif = nex_dif_vert - cur_dif_vert;
                let edge_slope = edge_dif.y / edge_dif.x;
                let edge_offset = cur_dif_vert.y - edge_slope * cur_dif_vert.x;

                // if y-intercept is greater that zero, the intersection is to the right of point,
                // and a wrap occurs
                let edge_y_intercept = -edge_offset / edge_slope;
                if edge_y_intercept >= 0.0 {
                    wrap_count += 1;
                    continue;
                }
            }
        }

        // if the polygon winds around the point an odd number of times, the point is inside the
        // polygon
        wrap_count % 2 > 0
    }

    /// checks to see if the polygon overlaps the specified aabb (in global space) at any point
    pub fn overlaps_aabb(&self, self_transform: &GlobalTransform, global_rect: &Rect) -> bool {
        // check to see if any points in the polygon are inside the aabb
        for vert in &self.verts {
            let glob_vert = self_transform.transform_point2(*vert);
            if global_rect.contains(glob_vert) {
                return true;
            }
        }

        // check to see if the polygon contains any corners of the global rect
        for i in 0..4 {
            let pt = self_transform.inverse_transform_point2(match i {
                0 => global_rect.min,
                1 => global_rect.max,
                2 => Vec2::new(global_rect.min.x, global_rect.max.y),
                3 => Vec2::new(global_rect.max.x, global_rect.min.y),
                _ => Vec2::ZERO,
            });
            if self.contains_local_point(pt) {
                return true;
            }
        }

        // TODO cover case where polygon and rect intersect but neither contain any points
        // from each other

        false
    }
}

impl ToFromPolygon for shapes::Polygon {
    fn to_polygon(&self) -> Polygon {
        let mut poly_verts = Vec::<Vec2>::new();
        for point in &self.points {
            poly_verts.push(*point);
        }
        Polygon::new(poly_verts)
    }
    fn from_polygon(value: &Polygon) -> Self {
        let mut points = Vec::<Vec2>::new();
        for vert in value.verts() {
            points.push(*vert);
        }
        shapes::Polygon {
            closed: true,
            points,
            ..Default::default()
        }
    }
}

impl TransformPoint2D for GlobalTransform {
    fn transform_point2(&self, point_2d: Vec2) -> Vec2 {
        self.transform_point(point_2d.extend(0.0)).truncate()
    }
    fn inverse_transform_point2(&self, point_2d: Vec2) -> Vec2 {
        self.compute_matrix()
            .inverse()
            .transform_point(point_2d.extend(0.0))
            .truncate()
    }
    fn transform_rect_from_points(&self, local_pt_a: Vec2, local_pt_b: Vec2) -> Rect {
        let glob_pt_a = self.transform_point2(local_pt_a);
        let glob_pt_b = self.transform_point2(local_pt_b);
        Rect::from_corners(glob_pt_a, glob_pt_b)
    }
    fn inverse_transform_rect_from_points(&self, glob_pt_a: Vec2, glob_pt_b: Vec2) -> Rect {
        let inv_mat = self.compute_matrix().inverse();
        let loc_pt_a = inv_mat.transform_point(glob_pt_a.extend(0.0)).truncate();
        let loc_pt_b = inv_mat.transform_point(glob_pt_b.extend(0.0)).truncate();
        Rect::from_corners(loc_pt_a, loc_pt_b)
    }
    fn extract_transform_scale2(&self) -> Vec2 {
        let origin = self.transform_point2(Vec2::ZERO);
        let axis_x = self.transform_point2(Vec2::new(1.0, 0.0)) - origin;
        let axis_y = self.transform_point2(Vec2::new(0.0, 1.0)) - origin;
        Vec2::new(axis_x.length(), axis_y.length())
    }
}

impl TransformPoint2D for Transform {
    fn transform_point2(&self, point_2d: Vec2) -> Vec2 {
        self.transform_point(point_2d.extend(0.0)).truncate()
    }
    fn inverse_transform_point2(&self, point_2d: Vec2) -> Vec2 {
        self.compute_matrix()
            .inverse()
            .transform_point(point_2d.extend(0.0))
            .truncate()
    }
    fn transform_rect_from_points(&self, local_pt_a: Vec2, local_pt_b: Vec2) -> Rect {
        let glob_pt_a = self.transform_point2(local_pt_a);
        let glob_pt_b = self.transform_point2(local_pt_b);
        Rect::from_corners(glob_pt_a, glob_pt_b)
    }
    fn inverse_transform_rect_from_points(&self, glob_pt_a: Vec2, glob_pt_b: Vec2) -> Rect {
        let inv_mat = self.compute_matrix().inverse();
        let loc_pt_a = inv_mat.transform_point(glob_pt_a.extend(0.0)).truncate();
        let loc_pt_b = inv_mat.transform_point(glob_pt_b.extend(0.0)).truncate();
        Rect::from_corners(loc_pt_a, loc_pt_b)
    }
    fn extract_transform_scale2(&self) -> Vec2 {
        self.scale.truncate()
    }
}

impl GeometryComponent {
    pub fn from_shape(polygon: &shapes::Polygon) -> Self {
        GeometryComponent {
            poly: polygon.to_polygon(),
            render_path_dirty: true,
        }
    }

    pub fn poly(&self) -> &Polygon {
        &self.poly
    }

    /// mutable ref to poly. marks render path as dirty
    pub fn poly_mut(&mut self) -> &mut Polygon {
        self.render_path_dirty = true;
        &mut self.poly
    }

    /// insert a vertex into the geometry's polygon shape. Note the vetex should be in local
    /// coordinates with respect to the gemoetry's transform. marks render path as dirty
    pub fn insert_local_vert(&mut self, index: usize, local_vert: Vec2) {
        self.poly.verts.insert(index, local_vert);
        self.render_path_dirty = true;
    }

    /// remove the vertex at the specified index in the geometry's polygon data. marks render path
    /// as dirty
    pub fn remove_vert_at(&mut self, index: usize) {
        self.poly.verts.remove(index);
        self.render_path_dirty = true;
    }

    fn update_render_path(&mut self, path: &mut Path) {
        self.render_path_dirty = false;
        *path = GeometryBuilder::build_as(&shapes::Polygon::from_polygon(&self.poly));
    }
}

impl GeometryBundle {
    /// create a geometry bundle from a bevy_prototype_lyon shape
    pub fn from_shape(translation: Vec3, shape: shapes::Polygon) -> Self {
        Self {
            geometry: GeometryComponent::from_shape(&shape),
            shape_bundle: ShapeBundle {
                spatial: SpatialBundle {
                    transform: Transform::from_translation(translation),
                    ..Default::default()
                },
                path: GeometryBuilder::build_as(&shape),
                ..Default::default()
            },
            fill: Self::default_fill(),
            stroke: Self::default_stroke(),
        }
    }

    /// create a geometry bundle with a polygon in the shape of a square
    pub fn square(position: Vec2, size: f32) -> Self {
        let shape = shapes::Polygon {
            closed: true,
            points: vec![
                Vec2::new(-size, -size),
                Vec2::new(size, -size),
                Vec2::new(size, size),
                Vec2::new(-size, size),
            ],
            ..Default::default()
        };
        Self::from_shape(position.extend(LEVEL_GEOM_Z_ORDER), shape)
    }

    pub fn default_fill() -> Fill {
        Fill::color(Color::WHITE)
    }

    pub fn default_stroke() -> Stroke {
        Stroke {
            color: Color::rgba(0.0, 0.0, 0.0, 1.0),
            options: StrokeOptions::default()
                .with_line_join(LineJoin::Round)
                .with_line_width(1.0),
        }
    }
}

// Systems: ----------------------------------------------------------------------------------------

pub fn handle_geom_render_path(
    px_size: Res<PixelWorldSize>,
    mut query: Query<(&mut GeometryComponent, &mut Path, &mut Stroke)>,
) {
    for (mut geom, mut path, mut stroke) in &mut query {
        stroke.options.line_width = px_size.0 * 2.0;
        if geom.render_path_dirty {
            geom.update_render_path(&mut path);
        }
    }
}
