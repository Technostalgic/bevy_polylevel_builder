pub mod property_pickers;
pub mod widget_edit_data_listings;
pub mod widget_edit_entity_listings;
pub mod widget_edit_selection;
pub mod widget_level;
pub mod widget_tool;

use std::mem::discriminant;

use bevy::prelude::*;
use bevy_egui::{
    egui::{self, InnerResponse, Pos2, Response, Ui},
    EguiContexts, EguiSet,
};
use bevy_prototype_lyon::prelude::{LineCap, LineJoin, ShapeBundle, Stroke, StrokeOptions};

use crate::{
    controls::AbstractMouseState,
    core::LevelEditorState,
    core::{EditorObject, EditorUtilityObject},
    user_data::{TypedUserData, UserDataListing, UserDataListingHandle, Value},
};

use self::{
    property_pickers::{PickingEntityRef, PickingType, PropertyPathNode, PropertyPickersPlugin},
    widget_edit_data_listings::EditDataListingsPlugin,
    widget_edit_entity_listings::EditEntListingsPlugin,
    widget_edit_selection::EditSelectionWidgetPlugin,
    widget_level::{
        handle_grid_rendering, handle_rfd_task_resolution, open_file, save_as, GridRenderer,
        WidgetLevelSystemIds,
    },
};

// -------------------------------------------------------------------------------------------------

#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy, SystemSet)]
enum SystemLabels {
    EguiHandling,
}

#[derive(Default, Clone, Copy, PartialEq, Eq)]
pub enum EntityTypedDataEditableness {
    #[default]
    Readonly,
    Tweakable,
    Editable,
}

/// used to specify the async state of a control, whether it's wating for a poll from an async task,
/// or has been confirmed, or hasn't been initialized
#[derive(Default, PartialEq, Eq, Clone, Copy)]
pub enum AsyncControlState {
    /// if the value for the control has been confirmed
    #[default]
    Confirmed,

    /// if an async task has been dispatched to get a value from the control, but the task has not
    /// yet been completed
    Waiting,
}

pub struct UiPlugin;

pub trait ToFromPos2 {
    fn to_pos2(&self) -> Pos2;
    fn from_pos2(value: Pos2) -> Self;
}

/// for internal use within ui to keep track of async control states, like file dialogues popping up
/// from a button press
#[derive(Resource, Default)]
pub(super) struct LevelEditPropertiesState {
    pub cur_file_path_state: AsyncControlState,
}

#[derive(Resource, Default)]
pub struct LevelEditUiState {
    pub show_edit_entity_listings_window: bool,
    pub show_edit_data_listings_window: bool,
}

// -------------------------------------------------------------------------------------------------

impl AsyncControlState {
    pub fn validate_control_value<T>(&self, control_val: T) -> Option<T> {
        match &self {
            AsyncControlState::Confirmed => Some(control_val),
            AsyncControlState::Waiting => None,
        }
    }
}

impl Plugin for UiPlugin {
    fn build(&self, app: &mut App) {
        // register the save as and open file systems
        let widget_lvl_sys_ids = WidgetLevelSystemIds {
            save_as_id: app.world.register_system(save_as),
            open_file_id: app.world.register_system(open_file),
        };

        app.add_plugins((
            EditEntListingsPlugin,
            EditDataListingsPlugin,
            EditSelectionWidgetPlugin,
            PropertyPickersPlugin,
        ))
        .insert_resource(LevelEditUiState::default())
        .insert_resource(LevelEditPropertiesState::default())
        .insert_resource(widget_lvl_sys_ids)
        .add_systems(
            OnEnter::<LevelEditorState>(LevelEditorState::Editing),
            setup,
        )
        .add_systems(
            PreUpdate,
            (
                handle_mouse_in_ui_flag.after(SystemLabels::EguiHandling),
                (widget_tool::handle_ui, widget_level::handle_ui)
                    .in_set(SystemLabels::EguiHandling)
                    .after(EguiSet::BeginFrame),
            )
                .run_if(in_state(LevelEditorState::Editing)),
        )
        .add_systems(
            Update,
            handle_rfd_task_resolution.run_if(in_state(LevelEditorState::Editing)),
        )
        .add_systems(
            PostUpdate,
            handle_grid_rendering.run_if(in_state(LevelEditorState::Editing)),
        );
    }
}

impl ToFromPos2 for Vec2 {
    fn to_pos2(&self) -> Pos2 {
        Pos2 {
            x: self.x,
            y: self.y,
        }
    }
    fn from_pos2(value: Pos2) -> Self {
        Vec2 {
            x: value.x,
            y: value.y,
        }
    }
}

// -------------------------------------------------------------------------------------------------

pub fn setup(mut commands: Commands, mut contexts: EguiContexts) {
    contexts.ctx_mut().set_visuals(egui::Visuals {
        ..Default::default()
    });

    // spawn grid renderers
    commands.spawn((
        EditorObject,
        EditorUtilityObject,
        ShapeBundle {
            ..Default::default()
        },
        Stroke {
            color: Color::WHITE,
            options: StrokeOptions::default()
                .with_start_cap(LineCap::Butt)
                .with_end_cap(LineCap::Butt)
                .with_line_join(LineJoin::Miter),
        },
        GridRenderer::new(1),
    ));
    commands.spawn((
        EditorObject,
        EditorUtilityObject,
        ShapeBundle {
            ..Default::default()
        },
        Stroke {
            color: Color::WHITE,
            options: StrokeOptions::default()
                .with_start_cap(LineCap::Butt)
                .with_end_cap(LineCap::Butt)
                .with_line_join(LineJoin::Miter),
        },
        GridRenderer::new(5),
    ));
}

fn handle_mouse_in_ui_flag(mut mouse: ResMut<AbstractMouseState>, mut egui_ctxs: EguiContexts) {
    if !mouse.inside_ui() {
        mouse.unset_was_inside_ui();
    }
    mouse.set_inside_ui(egui_ctxs.ctx_mut().wants_pointer_input());
}

// UI Utility Functions: ---------------------------------------------------------------------------

/// create a general purpose number ui input that modifies the specified floating point value
fn ui_float_input(ui: &mut egui::Ui, number: &mut f32, max_width: f32) -> Response {
    let mut av_size = ui.available_size();
    av_size.y = 18.0;
    if av_size.x > max_width {
        av_size.x = max_width;
    }
    let mut num_str = format!("{}", number);
    let response = ui.add_sized(av_size, bevy_egui::egui::TextEdit::singleline(&mut num_str));
    if let Ok(parsed_num) = num_str.parse::<f32>() {
        *number = parsed_num;
    }
    response
}

/// create a general purpose number ui input that modifies the specified integer value
fn ui_int_input(ui: &mut egui::Ui, number: &mut i32, max_width: f32) -> Response {
    let mut av_size = ui.available_size();
    av_size.y = 18.0;
    if av_size.x > max_width {
        av_size.x = max_width;
    }
    let mut num_str = format!("{}", number);
    let response = ui.add_sized(av_size, bevy_egui::egui::TextEdit::singleline(&mut num_str));
    if let Ok(parsed_num) = num_str.parse::<i32>() {
        *number = parsed_num;
    }
    response
}

/// create a general purpose vec2 ui input that modifies the passed in vector reference
fn ui_vec2_input(
    ui: &mut egui::Ui,
    vector: &mut Vec2,
    max_width: f32,
) -> InnerResponse<(Response, Response)> {
    ui.horizontal(|ui| -> (Response, Response) {
        let mut av_size = ui.available_size();
        if av_size.x > max_width {
            av_size.x = max_width * 0.5 - 5.0;
        } else {
            av_size.x = av_size.x * 0.5 - 5.0;
        }
        let res1 = ui_float_input(ui, &mut vector.x, av_size.x);
        let res2 = ui_float_input(ui, &mut vector.y, av_size.x);
        (res1, res2)
    })
}

/// get an arbitrary but consistent index which represents a correlponding [`Value`] enum variant
fn selectable_json_value_index(val: &Value) -> usize {
    match val {
        Value::Null => 0,
        Value::Bool(_) => 1,
        Value::Int(_) => 2,
        Value::Float(_) => 3,
        Value::Vector(_) => 4,
        Value::String(_) => 5,
        Value::EntityReference(_) => 6,
        Value::Array(_) => 7,
        Value::TypedArray(_, _) => 8,
        Value::Object(_) => 9,
        // _ => 1,
    }
}

/// parse an index into a [`Value`] enum variant that the index represent
fn selectable_json_value_from_index(index: usize) -> Value {
    match index {
        0 => Value::Null,
        1 => Value::Bool(default()),
        2 => Value::Int(default()),
        3 => Value::Float(default()),
        4 => Value::Vector(default()),
        5 => Value::String(default()),
        6 => Value::EntityReference(default()),
        7 => Value::Array(default()),
        8 => Value::TypedArray(Box::new(default()), default()),
        9 => Value::Object(default()),
        _ => Value::Null,
    }
}

/// parse an index into a string that represents the name of the corresponding [`Value`]
/// enum variant
fn selectable_json_val_str_from_index(index: usize) -> String {
    match index {
        0 => "Null".to_string(),
        1 => "Bool".to_string(),
        2 => "Integer".to_string(),
        3 => "Float".to_string(),
        4 => "Vector".to_string(),
        5 => "String".to_string(),
        6 => "Entity".to_string(),
        7 => "Array".to_string(),
        8 => "TypedArr".to_string(),
        9 => "Object".to_string(),
        _ => "Undefined".to_string(),
    }
}

/// create ui controls that allow the user to select a user data listing from a dropdown box
fn ui_data_listing_selector(
    ui: &mut Ui,
    handle_value: &mut UserDataListingHandle,
    id_source: &mut u32,
    data_listings: &Vec<UserDataListing>,
) -> Response {
    let index = handle_value.get_index();
    let mut index: usize = if let Some(ind) = index { ind + 1 } else { 0 };
    if data_listings.len() <= 0 {
        *handle_value = UserDataListingHandle::INVALID;
        index = 0;
    }
    let resp = egui::containers::ComboBox::from_id_source(id_source.clone()).show_index(
        ui,
        &mut index,
        data_listings.len() + 1,
        |i| {
            if i > 0 {
                &data_listings[i - 1].label
            } else {
                "none"
            }
        },
    );
    *id_source += 1;
    if resp.changed {
        if index > 0 {
            *handle_value = UserDataListingHandle::from_index(index - 1);
        } else {
            *handle_value = UserDataListingHandle::INVALID;
        }
    }
    resp
}

/// create ui controls that allow the modification of a json object data structure
#[must_use]
fn ui_entity_obj_data(
    ui: &mut Ui,
    type_data: &mut TypedUserData,
    id_source: &mut u32,
    editable: EntityTypedDataEditableness,
    cur_property_path: &mut Vec<PropertyPathNode>,
) -> Option<PickingEntityRef> {
    let mut picking: Option<PickingEntityRef> = None;
    let mut len = 0;
    let mut delta_key: Option<(String, String)> = None;
    for (label, mut val) in type_data.iter_mut() {
        // modify current path node to reflect current property path properly
        cur_property_path.push(PropertyPathNode::Key(label.clone()));
        len += 1;
        ui.horizontal(|ui| {
            ui.label(label);
            ui_entity_dat_type_picker(ui, val, id_source, editable);
            *id_source += 1;
        });
        ui.indent(62, |ui| {
            if editable == EntityTypedDataEditableness::Editable {
                ui.horizontal(|ui| {
                    if ui
                        .button("x")
                        .on_hover_text("Remove this field from the data")
                        .clicked()
                    {
                        delta_key = Some((label.clone(), "".to_string()));
                    }
                    let mut field_key = label.clone();
                    let resp = ui.text_edit_singleline(&mut field_key);
                    if resp.changed() {
                        if field_key.len() <= 0 {
                            field_key = "_".to_string();
                        }
                        delta_key = Some((label.clone(), field_key.replace(" ", "_").to_string()));
                    }
                });
            }
            ui.horizontal(|ui| {
                if let Some(pick) =
                    ui_entity_dat_value(ui, &mut val, id_source, editable, cur_property_path)
                {
                    picking = Some(pick);
                }
            });
            // remove the last node from the path since all keys have been iterated
            cur_property_path.pop();
        });
    }
    if editable == EntityTypedDataEditableness::Editable {
        if ui.button("Add Field").clicked() {
            type_data.insert(format!("field_{}", len), Value::Null);
        }
    }
    // apply hash swap if there is a key changed
    if let Some((old_key, new_key)) = delta_key {
        if let Some(val) = type_data.remove(&old_key) {
            // if no new key specified, the field was just meant to be removed, not replaced
            if new_key.len() > 0 {
                type_data.insert(new_key, val);
            }
        }
    }
    picking
}

/// create a ui dropbox that allows the user to change the type of json data stored in a field
fn ui_entity_dat_type_picker(
    ui: &mut Ui,
    json_value: &mut Value,
    id_source: &mut u32,
    editable: EntityTypedDataEditableness,
) {
    let o_index = selectable_json_value_index(json_value);
    if editable != EntityTypedDataEditableness::Editable {
        let mut label = selectable_json_val_str_from_index(o_index).to_string();
        if let Value::TypedArray(t, _) = json_value {
            label = format!(
                "{}<{}>",
                label,
                selectable_json_val_str_from_index(selectable_json_value_index(t))
            );
        }
        ui.label(format!("<{}>", label));
        return;
    }
    let mut index = o_index.clone();
    egui::containers::ComboBox::from_id_source(id_source.clone()).show_index(
        ui,
        &mut index,
        10,
        |i| selectable_json_val_str_from_index(i),
    );
    *id_source += 1;
    if o_index != index {
        *json_value = selectable_json_value_from_index(index);
    }
}

/// create ui controls that allow the user to change the specific value in a json field
#[must_use]
fn ui_entity_dat_value(
    ui: &mut Ui,
    json_value: &mut Value,
    id_source: &mut u32,
    editable: EntityTypedDataEditableness,
    cur_property_path: &mut Vec<PropertyPathNode>,
) -> Option<PickingEntityRef> {
    let mut picking: Option<PickingEntityRef> = None;
    match json_value {
        Value::Bool(ref mut value) => {
            if editable == EntityTypedDataEditableness::Readonly {
                ui.label(if *value { " true" } else { " false" });
            } else {
                ui.checkbox(value, "");
            }
        }
        Value::Int(ref mut value) => {
            if editable == EntityTypedDataEditableness::Readonly {
                ui.label(format!("{}", value));
            } else {
                ui_int_input(ui, value, 100.0);
            }
        }
        Value::Float(ref mut value) => {
            if editable == EntityTypedDataEditableness::Readonly {
                ui.label(format!("{}", value));
            } else {
                ui_float_input(ui, value, 100.0);
            }
        }
        Value::Vector(ref mut value) => {
            if editable == EntityTypedDataEditableness::Readonly {
                ui.label(format!("{}", value));
            } else {
                ui.horizontal(|ui| {
                    ui_vec2_input(ui, value, 100.0);
                    if editable == EntityTypedDataEditableness::Tweakable {
                        if ui
                            .button("world")
                            .on_hover_text("select a position in the level with your mouse")
                            .clicked()
                        {
                            picking = Some(PickingEntityRef {
                                pick_type: PickingType::WorldPosition,
                                property_path: cur_property_path.clone(),
                                ..Default::default()
                            });
                        } else if ui
                            .button("local")
                            .on_hover_text("select a relative position with your mouse")
                            .clicked()
                        {
                            picking = Some(PickingEntityRef {
                                pick_type: PickingType::LocalPosition,
                                property_path: cur_property_path.clone(),
                                ..Default::default()
                            });
                        }
                    }
                });
            }
        }
        Value::String(ref mut value) => {
            if editable == EntityTypedDataEditableness::Readonly {
                ui.label(value.clone());
            } else {
                ui.text_edit_singleline(value);
            }
        }
        Value::EntityReference(ref mut value) => match editable {
            EntityTypedDataEditableness::Readonly => {
                ui.label(format!("{:?}", value));
            }
            EntityTypedDataEditableness::Tweakable => {
                ui.horizontal(|ui| {
                    ui.label(format!("{:?}", value));
                    if ui
                        .button("select")
                        .on_hover_text("select an entity in the level with your mouse")
                        .clicked()
                    {
                        picking = Some(PickingEntityRef {
                            pick_type: PickingType::Entity,
                            property_path: cur_property_path.clone(),
                            ..Default::default()
                        });
                    }
                });
            }
            EntityTypedDataEditableness::Editable => {
                ui.label("(Can't Set Default)");
            }
        },
        Value::Array(ref mut arr) => {
            ui.vertical(|ui| {
                ui.indent(id_source.clone(), |ui| {
                    *id_source += 1;
                    for (i, val) in arr.iter_mut().enumerate() {
                        cur_property_path.push(PropertyPathNode::Index(i));
                        ui_entity_dat_type_picker(
                            ui,
                            val,
                            id_source,
                            match editable {
                                EntityTypedDataEditableness::Readonly => {
                                    EntityTypedDataEditableness::Readonly
                                }
                                _ => EntityTypedDataEditableness::Editable,
                            },
                        );
                        if let Some(pick) =
                            ui_entity_dat_value(ui, val, id_source, editable, cur_property_path)
                        {
                            picking = Some(pick);
                        }
                        cur_property_path.pop();
                    }
                });
                if ui.button("Add Value").clicked() {
                    arr.push(Value::Null);
                }
            });
        }
        Value::TypedArray(ref mut t, ref mut arr) => {
            ui.vertical(|ui| {
                ui_entity_dat_type_picker(ui, t, id_source, editable);
                ui.indent(id_source.clone(), |ui| {
                    *id_source += 1;
                    for (i, val) in arr.iter_mut().enumerate() {
                        cur_property_path.push(PropertyPathNode::Index(i));
                        if discriminant(val) != discriminant(t) {
                            *val = *t.clone();
                        }
                        if let Some(pick) =
                            ui_entity_dat_value(ui, val, id_source, editable, cur_property_path)
                        {
                            picking = Some(pick);
                        }
                        cur_property_path.pop();
                    }
                });
                if editable != EntityTypedDataEditableness::Readonly {
                    if ui.button("Add Value").clicked() {
                        arr.push(*t.clone());
                    }
                }
            });
        }
        Value::Object(ref mut object) => {
            ui.vertical(|ui| {
                if let Some(pick) =
                    ui_entity_obj_data(ui, object, id_source, editable, cur_property_path)
                {
                    picking = Some(pick);
                }
                *id_source += 1;
            });
        }
        // (only Value::Null should be effected here)
        _ => {
            ui.label(format!("{:?}", json_value));
        }
    }
    picking
}
