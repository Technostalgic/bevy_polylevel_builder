use bevy::prelude::*;

use crate::{
    core::{EditorObject, EditorWorldObject, LEVEL_ENT_Z_ORDER},
    user_data::{UserDataComponent, UserDataListingHandle},
};

// Types: ------------------------------------------------------------------------------------------

pub struct MapEntityPlugin;

#[derive(Default, Clone)]
pub struct MapEntityListing {
    pub label: String,
    pub icon: Sprite,
    pub icon_image: Handle<Image>,
    pub scale: Vec2,
    pub id: usize,
    pub user_data: UserDataListingHandle,
}

#[derive(Resource)]
pub struct MapEntityListings {
    pub entity_types: Vec<MapEntityListing>,
}

#[derive(Component, Default)]
pub struct MapEntity {
    pub listing_id: usize,
}

#[derive(Bundle, Default)]
pub struct MapEntityBundle {
    pub editor_obj: EditorObject,
    pub editor_world_obj: EditorWorldObject,
    pub name: Name,
    pub level_ent_dat: MapEntity,
    pub sprite_bundle: SpriteBundle,
}

// -------------------------------------------------------------------------------------------------

impl Plugin for MapEntityPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(MapEntityListings::default());
    }
}

impl Default for MapEntityListings {
    fn default() -> Self {
        Self {
            entity_types: vec![
                MapEntityListing {
                    label: "spawn_point".to_string(),
                    scale: Vec2::new(20.0, 20.0),
                    id: 0,
                    icon: Sprite {
                        color: Color::BLUE,
                        ..Default::default()
                    },
                    ..default()
                },
                MapEntityListing {
                    label: "gold".to_string(),
                    scale: Vec2::new(10.0, 10.0),
                    id: 1,
                    icon: Sprite {
                        color: Color::GOLD,
                        ..Default::default()
                    },
                    ..default()
                },
            ],
        }
    }
}

impl MapEntityListing {
    /// create an instance of the level entity at the specified world position
    pub fn create_instance_at(&self, commands: &mut Commands, world_pos: Vec2) -> Entity {
        // create map entity bundle
        let mut ent_cmd = commands.spawn(MapEntityBundle {
            name: Name::new(self.label.clone()),
            level_ent_dat: MapEntity {
                listing_id: self.id,
            },
            sprite_bundle: SpriteBundle {
                sprite: self.icon.clone(),
                texture: self.icon_image.clone(),
                transform: Transform {
                    translation: world_pos.extend(LEVEL_ENT_Z_ORDER),
                    scale: self.scale.extend(1.0),
                    ..Default::default()
                },
                ..Default::default()
            },
            ..Default::default()
        });
        // insert user data if applicable
        if self.user_data.is_valid() {
            ent_cmd.insert(UserDataComponent {
                listing: self.user_data.clone(),
                data: default(),
            });
        }
        // return entity
        ent_cmd.id()
    }
}

// -------------------------------------------------------------------------------------------------
