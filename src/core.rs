use std::path::PathBuf;

use bevy::{
    ecs::system::SystemId,
    prelude::*,
    render::camera::RenderTarget,
    utils::HashSet,
    window::{PrimaryWindow, WindowRef},
};
use bevy_egui::EguiPlugin;
use bevy_prototype_lyon::{draw::Stroke, plugin::ShapePlugin};

use crate::{
    clipboard::ClipboardPlugin,
    controls::ControlsPlugin,
    geometry::{GeometryComponent, GeometryPlugin},
    gizmo_extension::GizmoExtensionPlugin,
    hotkeys::HotkeyPlugin,
    map_entity::{MapEntity, MapEntityListings, MapEntityPlugin},
    save::{build_level_from_data, marshal_level_data, LevelData, SavePlugin},
    selection::SelectionPlugin,
    tools::{EditorTool, ToolPlugin},
    ui::{widget_level::GridRenderer, UiPlugin},
    user_data::{UserDataComponent, UserDataListings, UserDataPlugin},
    utility::UtilityPlugin,
};

// -------------------------------------------------------------------------------------------------

pub const LEVEL_GEOM_Z_ORDER: f32 = 0.0;
pub const LEVEL_ENT_Z_ORDER: f32 = 1.0;
pub const UI_PROPERTY_GIZMO_Z_ORDER: f32 = 2.0;
pub const UI_TOOL_GIZMO_Z_ORDER: f32 = 3.0;
pub const UI_TOOL_FOCUS_Z_ORDER: f32 = 4.0;
pub const UI_SEL_RECT_GIZMO_Z_ORDER: f32 = 5.0;

// -------------------------------------------------------------------------------------------------

#[derive(Default)]
pub struct BevyLevelEditPlugin {
    hide_non_editor_cams: bool,
    clear_on_edit: bool,
    edit_hotkey: Option<KeyCode>,
}

#[derive(Component, Default)]
pub struct EditorObject;

#[derive(Component, Default)]
pub struct EditorUtilityObject;

#[derive(Component, Default)]
pub struct EditorWorldObject;

#[derive(Resource, Default)]
pub struct LevelEditorData {
    pub hide_non_editor_cams: bool,
    pub clear_scene_on_edit: bool,
    pub level_data: Option<LevelData>,
    pub editor_hotkey: Option<KeyCode>,
    pub(super) hidden_cams: HashSet<Entity>,
}

#[derive(Resource)]
pub struct LevelEditProperties {
    pub grid_size: f32,
    pub background_color: Color,
    pub render_grid: bool,
    pub snap_to_grid: bool,
    pub cur_file_path: Option<PathBuf>,
}

#[derive(Resource)]
pub struct PixelWorldSize(pub f32);

#[derive(Resource)]
pub struct CoreSystemIds {
    pub unhide_non_editor_cams: SystemId,
    pub hide_non_editor_cams: SystemId,
    pub clear_scene: SystemId,
    pub clear_editor_entites: SystemId,
}

#[derive(States, Default, Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum LevelEditorState {
    #[default]
    Off,
    Editing,
}

// -------------------------------------------------------------------------------------------------

impl Plugin for BevyLevelEditPlugin {
    fn build(&self, app: &mut App) {
        let core_sys_ids = CoreSystemIds {
            hide_non_editor_cams: app.world.register_system(hide_non_editor_cams),
            unhide_non_editor_cams: app.world.register_system(unhide_non_editor_cams),
            clear_scene: app.world.register_system(clear_scene),
            clear_editor_entites: app.world.register_system(clear_editor_entities),
        };
        app.add_plugins((
            EguiPlugin,
            HotkeyPlugin,
            ClipboardPlugin,
            ShapePlugin,
            MapEntityPlugin,
            UserDataPlugin,
            SavePlugin,
            ToolPlugin,
            ControlsPlugin,
            GeometryPlugin,
            UiPlugin,
            SelectionPlugin,
            UtilityPlugin,
            GizmoExtensionPlugin,
        ))
        .init_state::<LevelEditorState>()
        .insert_resource(core_sys_ids)
        .insert_resource(LevelEditorData {
            clear_scene_on_edit: self.clear_on_edit,
            editor_hotkey: self.edit_hotkey,
            hide_non_editor_cams: self.hide_non_editor_cams,
            ..Default::default()
        })
        .insert_resource(LevelEditProperties {
            grid_size: 10.0,
            render_grid: true,
            snap_to_grid: true,
            ..Default::default()
        })
        .insert_resource(PixelWorldSize(1.0))
        .insert_resource(EditorTool::default())
        .add_systems(
            OnEnter::<LevelEditorState>(LevelEditorState::Editing),
            construct_editor_world,
        )
        .add_systems(
            OnExit::<LevelEditorState>(LevelEditorState::Editing),
            destruct_editor_world,
        )
        .add_systems(
            PreUpdate,
            (
                listen_for_hotkey,
                (handle_pixel_world_size, handle_bg_color)
                    .run_if(in_state(LevelEditorState::Editing)),
            ),
        );
    }
}

impl Default for LevelEditProperties {
    fn default() -> Self {
        Self {
            background_color: Color::rgb(0.2, 0.2, 0.2),
            cur_file_path: default(),
            grid_size: 10.0,
            render_grid: true,
            snap_to_grid: true,
        }
    }
}

impl BevyLevelEditPlugin {
    /// Creates the plugin with the specified parameters
    ///
    /// # Argumets
    /// * `clear_on_edit` indicates whether or not all entities with a transform component are
    ///     removed from the scene when the level editor is initiated
    /// * `edit_hotkey` a hotkey that can be used to initiate the level editor, which will spawn
    ///     all necessary resources and entities to use the level editor interface
    pub fn new(hide_cams_on_edit: bool, clear_on_edit: bool, edit_hotkey: Option<KeyCode>) -> Self {
        Self {
            hide_non_editor_cams: hide_cams_on_edit,
            clear_on_edit,
            edit_hotkey,
        }
    }
}

// Systems: ----------------------------------------------------------------------------------------

pub fn listen_for_hotkey(
    editor_data: Res<LevelEditorData>,
    keys: Res<ButtonInput<KeyCode>>,
    mut state: ResMut<NextState<LevelEditorState>>,
) {
    if let Some(hotkey) = editor_data.editor_hotkey {
        if keys.just_pressed(hotkey) {
            state.0 = Some(LevelEditorState::Editing);
        }
    }
}

/// Handles updating the [`PixelWorldSize`] to the size of a pixel in the world at the current
/// camera zoom level and resolution
pub fn handle_pixel_world_size(
    mut px_size: ResMut<PixelWorldSize>,
    cam_query: Query<(&Camera, &GlobalTransform), With<EditorObject>>,
    window_query: Query<&Window, With<PrimaryWindow>>,
) {
    let window = window_query.single();
    let (cam, cam_trans) = cam_query.single();
    let world_vp_width = (cam
        .ndc_to_world(cam_trans, Vec3::new(1.0, 0.0, 0.0))
        .unwrap_or(Vec3::ZERO)
        .x
        - (cam_trans.translation().x))
        * 2.0;

    px_size.0 = (1.0 / window.width()) * world_vp_width;
}

/// Handles setting the background color of the camera to what it's set to in the settings
pub fn handle_bg_color(
    editor_props: Res<LevelEditProperties>,
    mut cam_query: Query<&mut Camera, With<EditorObject>>,
    mut grid_query: Query<&mut Stroke, With<GridRenderer>>,
) {
    let cam = cam_query.get_single_mut();
    if cam.is_err() {
        return;
    }
    let mut cam = cam.unwrap();
    if let ClearColorConfig::Custom(bg_col) = cam.clear_color {
        if bg_col != editor_props.background_color {
            cam.clear_color = ClearColorConfig::Custom(editor_props.background_color.clone());
            let is_light = bg_col.r() + bg_col.g() + bg_col.b() > 1.0;
            for mut stroke in &mut grid_query {
                // if light background color, set grid to black
                if is_light {
                    stroke.color = Color::BLACK.with_a(stroke.color.a());
                // dark background color, set grid to white
                } else {
                    stroke.color = Color::WHITE.with_a(stroke.color.a());
                }
            }
        }
    }
}

/// One shot system that removes every entity from the scene with [`Transform`] component
pub fn clear_scene(
    mut commands: Commands,
    entity_query: Query<Entity, (With<Transform>, Without<EditorObject>)>,
    editor_data: Res<LevelEditorData>,
) {
    if !editor_data.clear_scene_on_edit {
        return;
    }
    // remove everything from the game
    for ent in &entity_query {
        let mut ent_cmd = commands.entity(ent);
        ent_cmd.despawn();
    }
}

/// Oneshot system that clears all the entities with [`EditorObject`] component
pub fn clear_editor_entities(
    mut commands: Commands,
    entity_query: Query<Entity, With<EditorObject>>,
) {
    // remove everything from the game
    for ent in &entity_query {
        let mut ent_cmd = commands.entity(ent);
        ent_cmd.despawn();
    }
}

pub fn hide_non_editor_cams(
    mut data: ResMut<LevelEditorData>,
    mut cam_query: Query<(Entity, &mut Camera), Without<EditorObject>>,
) {
    for (ent, mut cam) in &mut cam_query {
        if let RenderTarget::Window(win_ref) = cam.target {
            if let WindowRef::Primary = win_ref {
                cam.is_active = false;
                data.hidden_cams.insert(ent);
            }
        }
    }
}

pub fn unhide_non_editor_cams(
    mut cam_query: Query<&mut Camera, Without<EditorObject>>,
    mut data: ResMut<LevelEditorData>,
) {
    for cam_ent in &data.hidden_cams {
        if let Ok(mut cam) = cam_query.get_mut(*cam_ent) {
            cam.is_active = true;
        }
    }
    data.hidden_cams.clear();
}

pub fn construct_editor_world(
    mut commands: Commands,
    sys_ids: Res<CoreSystemIds>,
    data: Res<LevelEditorData>,
) {
    // remove everything from the game
    commands.run_system(sys_ids.clear_scene);
    commands.run_system(sys_ids.hide_non_editor_cams);

    // spawn camera
    commands.spawn((
        EditorObject,
        EditorUtilityObject,
        Camera2dBundle {
            camera: Camera {
                clear_color: ClearColorConfig::Custom(Color::rgb(0.2, 0.2, 0.2)),
                ..Default::default()
            },
            projection: OrthographicProjection {
                scale: 0.5,
                far: 1000.0,
                near: -1000.0,
                ..Default::default()
            },
            ..Default::default()
        },
    ));

    // hide cams if applicable
    if data.hide_non_editor_cams {}

    // load game from data resource
    if let Some(level) = data.level_data.as_ref() {
        build_level_from_data(&mut commands, level);
    }
}

pub fn destruct_editor_world(
    mut commands: Commands,
    sys_ids: Res<CoreSystemIds>,
    ent_listings: Res<MapEntityListings>,
    usr_dat_listings: Res<UserDataListings>,
    geom_query: Query<(
        Entity,
        &GlobalTransform,
        &GeometryComponent,
        Option<&UserDataComponent>,
    )>,
    entity_query: Query<(
        Entity,
        &GlobalTransform,
        &MapEntity,
        Option<&UserDataComponent>,
    )>,
    mut data: ResMut<LevelEditorData>,
) {
    // save level data into resource
    data.level_data = Some(marshal_level_data(
        &ent_listings,
        &usr_dat_listings,
        &geom_query,
        &entity_query,
    ));

    // remove all editor things from the game
    commands.run_system(sys_ids.clear_editor_entites);
    commands.run_system(sys_ids.unhide_non_editor_cams);
}
