use bevy::{
    input::mouse,
    input::mouse::MouseWheel,
    prelude::*,
    utils::{hashbrown::hash_set::Iter, HashSet},
    window::PrimaryWindow,
};

use crate::{
    clipboard::ClipboardSystemIds,
    core::{EditorObject, LevelEditProperties, LevelEditorState},
    hotkeys::{HotkeyBinding, HotkeyPollState, KeyPressCondition},
    save::SaveSystemIds,
    tools::{EditorTool, OnToolSwitched, SelectionModifier, SelectionModifiers, ToolState},
    ui::widget_level::WidgetLevelSystemIds,
    utility::UtilSystemIds,
};

//--------------------------------------------------------------------------------------------------

const BINDING_SEL_UNION: HotkeyBinding = HotkeyBinding {
    action_key: None,
    require_ctrl: KeyPressCondition::Pressed,
    require_shift: KeyPressCondition::Any,
    require_alt: KeyPressCondition::Any,
};

const BINDING_SEL_DIFFERENCE: HotkeyBinding = HotkeyBinding {
    action_key: None,
    require_ctrl: KeyPressCondition::Any,
    require_shift: KeyPressCondition::Any,
    require_alt: KeyPressCondition::Pressed,
};

const BINDING_SEL_ALL: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::KeyA),
    require_ctrl: KeyPressCondition::Pressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_DELETE: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::Delete),
    require_ctrl: KeyPressCondition::Unpressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_SAVE: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::KeyS),
    require_ctrl: KeyPressCondition::Pressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_SAVE_AS: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::KeyS),
    require_ctrl: KeyPressCondition::Pressed,
    require_shift: KeyPressCondition::Pressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_OPEN: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::KeyO),
    require_ctrl: KeyPressCondition::Pressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_EXIT_EDITOR: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::F5),
    require_ctrl: KeyPressCondition::Unpressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_COPY: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::KeyC),
    require_ctrl: KeyPressCondition::Pressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_PASTE: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::KeyV),
    require_ctrl: KeyPressCondition::Pressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_NUDGE_RIGHT: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::ArrowRight),
    require_ctrl: KeyPressCondition::Unpressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_NUDGE_LEFT: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::ArrowLeft),
    require_ctrl: KeyPressCondition::Unpressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_NUDGE_UP: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::ArrowUp),
    require_ctrl: KeyPressCondition::Unpressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_NUDGE_DOWN: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::ArrowDown),
    require_ctrl: KeyPressCondition::Unpressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_PAN_LEFT: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::KeyA),
    require_ctrl: KeyPressCondition::Unpressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_PAN_RIGHT: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::KeyD),
    require_ctrl: KeyPressCondition::Unpressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_PAN_UP: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::KeyW),
    require_ctrl: KeyPressCondition::Unpressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_PAN_DOWN: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::KeyS),
    require_ctrl: KeyPressCondition::Unpressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_NORMALIZE_VIEW: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::KeyB),
    require_ctrl: KeyPressCondition::Pressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_TOOL_SELECT_HOLD: HotkeyBinding = HotkeyBinding {
    action_key: None,
    require_ctrl: KeyPressCondition::Any,
    require_shift: KeyPressCondition::Pressed,
    require_alt: KeyPressCondition::Any,
};

const BINDING_TOOL_SELECT: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::KeyS),
    require_ctrl: KeyPressCondition::Unpressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_TOOL_POLY: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::KeyP),
    require_ctrl: KeyPressCondition::Unpressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_TOOL_VERT: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::KeyV),
    require_ctrl: KeyPressCondition::Unpressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_TOOL_ENT: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::KeyE),
    require_ctrl: KeyPressCondition::Unpressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

const BINDING_TOOL_TRANSFORM: HotkeyBinding = HotkeyBinding {
    action_key: Some(KeyCode::KeyT),
    require_ctrl: KeyPressCondition::Unpressed,
    require_shift: KeyPressCondition::Unpressed,
    require_alt: KeyPressCondition::Unpressed,
};

//--------------------------------------------------------------------------------------------------

pub struct ControlsPlugin;

#[derive(Default, Clone, Copy, PartialEq, Eq, Hash)]
pub enum AbstractClick {
    #[default]
    Primary,
    Secondary,
    Tertiery,
    Back,
    Forward,
    Alternative(u16),
}

#[derive(Resource, Default, Clone, Copy)]
pub struct AbstractDragState {
    is_pressed: bool,
    is_pressed_world: bool,
    is_dragging: bool,
    is_dragging_world: bool,
    click_type: AbstractClick,
    start_pos: Vec2,
    start_pos_world: Vec2,
    start_pos_world_snapped: Vec2,
    end_pos: Vec2,
    end_pos_world: Vec2,
    end_pos_world_snapped: Vec2,
    last_end_pos: Vec2,
    last_end_pos_world: Vec2,
    last_end_pos_world_snapped: Vec2,
}

#[derive(Resource, Default)]
pub struct AbstractMouseState {
    inside_ui: bool,
    inside_ui_last: bool,
    pressed_clicks: HashSet<AbstractClick>,
    just_pressed_clicks: HashSet<AbstractClick>,
    just_released_clicks: HashSet<AbstractClick>,
    position: Vec2,
    position_world: Vec2,
    position_world_snapped: Vec2,
}

// Implementations: --------------------------------------------------------------------------------

impl Plugin for ControlsPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(AbstractMouseState::default())
            .insert_resource(AbstractDragState::default())
            .add_systems(
                PreUpdate,
                (handle_abstract_mouse, handle_drag)
                    .run_if(in_state(LevelEditorState::Editing))
                    .after(mouse::mouse_button_input_system)
                    .chain(),
            )
            .add_systems(
                Update,
                (handle_mouse_controls, handle_hotkeys).run_if(in_state(LevelEditorState::Editing)),
            );
    }
}

impl From<MouseButton> for AbstractClick {
    fn from(value: MouseButton) -> Self {
        match value {
            MouseButton::Left => AbstractClick::Primary,
            MouseButton::Right => AbstractClick::Secondary,
            MouseButton::Middle => AbstractClick::Tertiery,
            MouseButton::Back => AbstractClick::Back,
            MouseButton::Forward => AbstractClick::Forward,
            MouseButton::Other(id) => AbstractClick::Alternative(id),
        }
    }
}

impl From<AbstractClick> for MouseButton {
    fn from(value: AbstractClick) -> Self {
        match value {
            AbstractClick::Primary => MouseButton::Left,
            AbstractClick::Secondary => MouseButton::Right,
            AbstractClick::Tertiery => MouseButton::Middle,
            AbstractClick::Back => MouseButton::Back,
            AbstractClick::Forward => MouseButton::Forward,
            AbstractClick::Alternative(id) => MouseButton::Other(id),
        }
    }
}

impl AbstractDragState {
    /// whether or not the mouse drag has started
    pub fn is_dragging(&self) -> bool {
        self.is_dragging
    }

    /// whether or not the mouse drag has started (in the world, not in ui)
    pub fn is_dragging_world(&self) -> bool {
        self.is_dragging_world
    }

    /// which mouse button is being dragged with
    pub fn click_type(&self) -> AbstractClick {
        self.click_type
    }

    /// the start position of the mouse drag
    pub fn _start_pos(&self) -> Vec2 {
        self.start_pos
    }

    /// the start position of the drag in world space
    pub fn start_pos_world(&self) -> Vec2 {
        self.start_pos_world
    }

    /// the start position of the drag in world space
    pub fn start_pos_world_snapped(&self) -> Vec2 {
        self.start_pos_world_snapped
    }

    /// the end position of the mouse drag
    pub fn _end_pos(&self) -> Vec2 {
        self.end_pos
    }

    /// the end position of the drag in world space
    pub fn end_pos_world(&self) -> Vec2 {
        self.end_pos_world
    }

    /// the end position of the drag in world space
    pub fn end_pos_world_snapped(&self) -> Vec2 {
        self.end_pos_world_snapped
    }

    /// the total difference between the drag start and end points
    pub fn delta(&self) -> Vec2 {
        self.end_pos - self.start_pos
    }

    /// the total difference between the drag start and end points in world space
    pub fn _delta_world(&self) -> Vec2 {
        self.end_pos_world - self.start_pos_world
    }

    /// the total difference between the drag start and end points in world space
    pub fn _delta_world_snapped(&self) -> Vec2 {
        self.end_pos_world_snapped - self.start_pos_world_snapped
    }

    /// the difference between the drag start and end points this frame
    pub fn frame_delta(&self) -> Vec2 {
        self.end_pos - self.last_end_pos
    }

    /// the difference between the drag start and end points in world space this frame
    pub fn _frame_delta_world(&self) -> Vec2 {
        self.end_pos_world - self.last_end_pos_world
    }

    /// the difference between the drag start and end points in world space this frame
    pub fn frame_delta_world_snapped(&self) -> Vec2 {
        self.end_pos_world_snapped - self.last_end_pos_world_snapped
    }
}

impl AbstractMouseState {
    /// whether or not the mouse cursor is inside a ui window this tick
    pub fn inside_ui(&self) -> bool {
        self.inside_ui
    }

    /// if the mouse cursor was inside a ui window last tick (may be useful if querying for if mouse
    /// is inside ui at some time when the normal inside_ui() value is invalid, at certain points
    /// of the PreUpdate schedule)
    pub fn was_inside_ui(&self) -> bool {
        self.inside_ui_last
    }

    /// set the is inside ui flag, should be called from ui methods to let the mouse state know it's
    /// currently inside one if it is
    pub fn set_inside_ui(&mut self, is_inside_ui: bool) {
        self.inside_ui = is_inside_ui;
        if is_inside_ui {
            self.inside_ui_last = true;
        }
    }

    /// used to reset the flag for tracking if mouse was in ui last frame
    pub fn unset_was_inside_ui(&mut self) {
        self.inside_ui_last = false;
    }

    /// if the specified mouse click is currently pressed
    pub fn pressed(&self, click_type: &AbstractClick) -> bool {
        self.pressed_clicks.contains(click_type)
    }

    /// get an iterator for all the mouse clicks that are currently pressed
    pub fn get_pressed(&self) -> Iter<'_, AbstractClick> {
        self.pressed_clicks.iter()
    }

    /// if the specified mouse click was just pressed this tick
    pub fn just_pressed(&self, click_type: &AbstractClick) -> bool {
        self.just_pressed_clicks.contains(click_type)
    }

    pub fn _just_pressed_any(&self) -> bool {
        self.just_pressed_clicks.len() > 0
    }

    /// if the specified mouse click was just pressed last tick
    pub fn just_released(&self, click_type: &AbstractClick) -> bool {
        self.just_released_clicks.contains(click_type)
    }

    pub fn just_released_any(&self) -> bool {
        self.just_released_clicks.len() > 0
    }

    /// the current screen position of the mouse
    pub fn position(&self) -> Vec2 {
        self.position
    }

    /// current world position of the mouse
    pub fn position_world(&self) -> Vec2 {
        self.position_world
    }

    /// returns the mouse world position snapped to the nearest grid cell
    pub fn position_world_snapped(&self) -> Vec2 {
        self.position_world_snapped
    }
}

// Systems: ----------------------------------------------------------------------------------------

pub fn handle_abstract_mouse(
    editor_props: Res<LevelEditProperties>,
    mouse_button_input: Res<ButtonInput<MouseButton>>,
    mut mouse: ResMut<AbstractMouseState>,
    window_query: Query<&mut Window, With<PrimaryWindow>>,
    cam_query: Query<(&GlobalTransform, &Camera), With<EditorObject>>,
) {
    let window = window_query.single();
    let cursor_pos = window.cursor_position().unwrap_or(Vec2::ZERO);
    mouse.position = cursor_pos;

    mouse.just_pressed_clicks.clear();
    mouse.just_released_clicks.clear();

    for button in mouse_button_input.get_just_pressed() {
        mouse
            .just_pressed_clicks
            .insert(AbstractClick::from(*button));
        mouse.pressed_clicks.insert(AbstractClick::from(*button));
    }
    for button in mouse_button_input.get_just_released() {
        mouse
            .just_released_clicks
            .insert(AbstractClick::from(*button));
        mouse.pressed_clicks.remove(&AbstractClick::from(*button));
    }

    if let Ok((glob_trans, cam)) = cam_query.get_single() {
        let world_pos = cam
            .viewport_to_world_2d(glob_trans, cursor_pos)
            .unwrap_or(Vec2::ZERO);
        mouse.position_world = world_pos;
        let snapped_world_pos = if editor_props.snap_to_grid {
            (world_pos / editor_props.grid_size).round() * editor_props.grid_size
        } else {
            world_pos
        };
        mouse.position_world_snapped = snapped_world_pos;
    }
}

pub fn handle_drag(mouse: Res<AbstractMouseState>, mut mouse_drag: ResMut<AbstractDragState>) {
    // get the current mouse positions
    let mouse_pos = mouse.position();
    let mouse_pos_world = mouse.position_world();
    let mouse_pos_world_snapped = mouse.position_world_snapped();

    // get the currently held button
    let mut cur_drag_button: Option<AbstractClick> = None;
    for btn in mouse.get_pressed() {
        cur_drag_button = Some(*btn);
        break;
    }

    // mouse button is held
    if let Some(cur_held_button) = cur_drag_button {
        // store start pose if first frame button is pressed
        if !mouse_drag.is_pressed {
            mouse_drag.is_pressed = true;
            mouse_drag.start_pos = mouse_pos;
            mouse_drag.start_pos_world = mouse_pos_world;
            mouse_drag.start_pos_world_snapped = mouse_pos_world_snapped;
            mouse_drag.end_pos = mouse_pos;
            if !mouse.was_inside_ui() {
                mouse_drag.is_pressed_world = true;
                mouse_drag.end_pos_world = mouse_pos_world;
                mouse_drag.end_pos_world_snapped = mouse_pos_world_snapped;
            }
        }

        // store end position every frame mouse button is held
        mouse_drag.last_end_pos = mouse_drag.end_pos;
        mouse_drag.end_pos = mouse_pos;
        if !mouse.was_inside_ui() {
            mouse_drag.last_end_pos_world = mouse_drag.end_pos_world;
            mouse_drag.last_end_pos_world_snapped = mouse_drag.end_pos_world_snapped;
            mouse_drag.end_pos_world = mouse_pos_world;
            mouse_drag.end_pos_world_snapped = mouse_pos_world_snapped;
        }

        // mouse was moved while held (dragging begins)
        let delta = mouse_drag.delta();
        if delta.x != 0.0 || delta.y != 0.0 {
            mouse_drag.is_dragging = true;
            if mouse_drag.is_pressed_world {
                mouse_drag.is_dragging_world = true;
            }
        }

        // restart drag if button is different
        if mouse_drag.click_type != cur_held_button {
            mouse_drag.is_pressed = false;
            mouse_drag.is_pressed_world = false;
            mouse_drag.is_dragging = false;
            mouse_drag.is_dragging_world = false;
        }

        mouse_drag.click_type = cur_held_button;

    // just stopped pressing mouse button (not necessarily stopped dragging if drag never started)
    } else if mouse_drag.is_pressed {
        mouse_drag.is_pressed = false;
        mouse_drag.is_pressed_world = false;

        // just stopped dragging
        if mouse_drag.is_dragging {
            mouse_drag.last_end_pos = mouse_drag.end_pos;
            if mouse_drag.is_dragging_world {
                mouse_drag.last_end_pos_world = mouse_drag.end_pos_world;
                mouse_drag.last_end_pos_world_snapped = mouse_drag.end_pos_world_snapped;
            }
            mouse_drag.is_dragging = false;
            mouse_drag.is_dragging_world = false;
        }
    }
}

/// perform functionality in the program based on user keyboard input
pub fn handle_hotkeys(
    hotkey_poll: Res<HotkeyPollState>,
    mut commands: Commands,
    util_systems: Res<UtilSystemIds>,
    clipboard_systems: Res<ClipboardSystemIds>,
    save_systems: Res<SaveSystemIds>,
    ui_lvl_systems: Res<WidgetLevelSystemIds>,
    mut current_tool: ResMut<EditorTool>,
    mut tool_state: ResMut<ToolState>,
    mut sel_modifiers: ResMut<SelectionModifiers>,
    mut tool_evts: EventWriter<OnToolSwitched>,
    mut edit_state: ResMut<NextState<LevelEditorState>>,
    mut cam_query: Query<
        (&mut Transform, &mut OrthographicProjection),
        (With<Camera2d>, With<EditorObject>),
    >,
    time: Res<Time>,
) {
    // add/remove union selection modifier
    if hotkey_poll.is_binding_triggered(&BINDING_SEL_UNION) {
        sel_modifiers.insert(SelectionModifier::Union);
    } else if hotkey_poll.is_binding_untriggered(&BINDING_SEL_UNION) {
        sel_modifiers.remove(&SelectionModifier::Union);
    }

    // add/remove difference selection modifier
    if hotkey_poll.is_binding_triggered(&BINDING_SEL_DIFFERENCE) {
        sel_modifiers.insert(SelectionModifier::Difference);
    } else if hotkey_poll.is_binding_untriggered(&BINDING_SEL_DIFFERENCE) {
        sel_modifiers.remove(&SelectionModifier::Difference);
    }

    // temporarily switch to select tool while binding is held
    if hotkey_poll.is_binding_held(&BINDING_TOOL_SELECT_HOLD) {
        let prev_tool = *current_tool;
        if current_tool.set(EditorTool::Select, &mut tool_evts) {
            tool_state.cached_tool = Some(prev_tool);
        }
    } else if let Some(cached_tool) = tool_state.cached_tool {
        current_tool.set(cached_tool, &mut tool_evts);
        tool_state.cached_tool = None;
    }

    // camera hotkeys
    if let Ok((mut cam_trans, cam_proj)) = cam_query.get_single_mut() {
        const PAN_SPEED: f32 = 500.0;
        let dt = time.delta_seconds();
        let mut mov = Vec2::ZERO;
        if hotkey_poll.is_binding_held(&BINDING_PAN_LEFT) {
            mov.x -= 1.0;
        }
        if hotkey_poll.is_binding_held(&BINDING_PAN_RIGHT) {
            mov.x += 1.0;
        }
        if hotkey_poll.is_binding_held(&BINDING_PAN_UP) {
            mov.y += 1.0;
        }
        if hotkey_poll.is_binding_held(&BINDING_PAN_DOWN) {
            mov.y -= 1.0;
        }
        cam_trans.translation += (mov * cam_proj.scale * dt * PAN_SPEED).extend(0.0);
    }

    // check if any potential bindings were triggered this frame before checking each
    // individual binding
    if hotkey_poll.any_bindings_triggered() && !hotkey_poll.ui_focus() {
        // delete selected entities
        if hotkey_poll.is_binding_triggered(&BINDING_DELETE) {
            commands.run_system(util_systems.remove_selected_objs);
        }

        if hotkey_poll.is_binding_triggered(&BINDING_SEL_ALL) {
            commands.run_system(util_systems.select_all);
        }
        if hotkey_poll.is_binding_triggered(&BINDING_NORMALIZE_VIEW) {
            commands.run_system(util_systems.normalize_view);
        }

        // save/load
        if hotkey_poll.is_binding_triggered(&BINDING_SAVE_AS) {
            commands.run_system(ui_lvl_systems.save_as_id);
        }
        if hotkey_poll.is_binding_triggered(&BINDING_SAVE) {
            commands.run_system(save_systems.save_file_id);
        }
        if hotkey_poll.is_binding_triggered(&BINDING_OPEN) {
            commands.run_system(ui_lvl_systems.open_file_id);
        }

        // exit editor
        if hotkey_poll.is_binding_triggered(&BINDING_EXIT_EDITOR) {
            edit_state.0 = Some(LevelEditorState::Off);
        }

        // copy selection
        if hotkey_poll.is_binding_triggered(&BINDING_COPY) {
            commands.run_system(clipboard_systems.copy_selection_id);
        }

        // paste clipboard top clipboard entry
        if hotkey_poll.is_binding_triggered(&BINDING_PASTE) {
            commands.run_system(clipboard_systems.paste_clipboard_top_entry_id);
        }

        // nudge translate slection
        if hotkey_poll.is_binding_triggered(&BINDING_NUDGE_RIGHT) {
            commands.run_system(util_systems.nudge_selection_right);
        }
        if hotkey_poll.is_binding_triggered(&BINDING_NUDGE_LEFT) {
            commands.run_system(util_systems.nudge_selection_left);
        }
        if hotkey_poll.is_binding_triggered(&BINDING_NUDGE_UP) {
            commands.run_system(util_systems.nudge_selection_up);
        }
        if hotkey_poll.is_binding_triggered(&BINDING_NUDGE_DOWN) {
            commands.run_system(util_systems.nudge_selection_down);
        }

        // tool hotkeys
        if hotkey_poll.is_binding_triggered(&BINDING_TOOL_SELECT) {
            switch_tool_from_hotkey(
                EditorTool::Select,
                &mut current_tool,
                &mut tool_state,
                &mut tool_evts,
            );
        }
        if hotkey_poll.is_binding_triggered(&BINDING_TOOL_POLY) {
            switch_tool_from_hotkey(
                EditorTool::Polygon,
                &mut current_tool,
                &mut tool_state,
                &mut tool_evts,
            );
        }
        if hotkey_poll.is_binding_triggered(&BINDING_TOOL_VERT) {
            switch_tool_from_hotkey(
                EditorTool::Vertex,
                &mut current_tool,
                &mut tool_state,
                &mut tool_evts,
            );
        }
        if hotkey_poll.is_binding_triggered(&BINDING_TOOL_ENT) {
            switch_tool_from_hotkey(
                EditorTool::Entity,
                &mut current_tool,
                &mut tool_state,
                &mut tool_evts,
            );
        }
        if hotkey_poll.is_binding_triggered(&BINDING_TOOL_TRANSFORM) {
            switch_tool_from_hotkey(
                EditorTool::Transform,
                &mut current_tool,
                &mut tool_state,
                &mut tool_evts,
            )
        }
    }
}

/// perform functionality in the program based on user mouse input
pub fn handle_mouse_controls(
    mouse: Res<AbstractMouseState>,
    mouse_drag: Res<AbstractDragState>,
    mut mouse_wheel: EventReader<MouseWheel>,
    mut cam_query: Query<
        (&mut Transform, &mut OrthographicProjection),
        (With<Camera2d>, With<EditorObject>),
    >,
) {
    // we don't want to control the camera if the mouse is being used for ui
    if mouse.inside_ui {
        return;
    }

    let mut cam_components = cam_query.single_mut();
    let cam_inverse_zoom = cam_components.1.scale;

    // mouse drag panning
    if mouse_drag.is_dragging() {
        let drag_delta = mouse_drag.frame_delta();

        // camera pan with middle mouse button
        if mouse_drag.click_type() == AbstractClick::Tertiery {
            let mut cam_transform = cam_components.0;
            let target_delta = Vec3::new(-drag_delta.x, drag_delta.y, 0.0);
            cam_transform.translation += target_delta * cam_inverse_zoom;
        }
    }

    // mouse wheel zooming
    const ZOOM_MULT: f32 = 0.84089641525371454303112547623321; // sqrt(sqrt(0.5))
    let mut new_inv_zoom = cam_inverse_zoom;
    for ev in mouse_wheel.read() {
        let zoom = if ev.y > 0.0 {
            ZOOM_MULT
        } else if ev.y < 0.0 {
            1.0 / ZOOM_MULT
        } else {
            1.0
        };
        new_inv_zoom *= zoom;
    }

    // ensure coumpounding float multiplications don't degrade baseline zoom level
    if new_inv_zoom > 1.0 * ZOOM_MULT + 0.01 && new_inv_zoom < 1.0 / ZOOM_MULT - 0.01 {
        new_inv_zoom = 1.0;
    }
    cam_components.1.scale = new_inv_zoom;
}

// Utility: ----------------------------------------------------------------------------------------

fn switch_tool_from_hotkey(
    next_tool: EditorTool,
    cur_tool: &mut ResMut<EditorTool>,
    tool_state: &mut ResMut<ToolState>,
    evts: &mut EventWriter<OnToolSwitched>,
) {
    // let prev_tool = **cur_tool;

    // if tool is different, swap to it
    if cur_tool.set(next_tool, evts) {
        // tool_state.cached_tool = Some(prev_tool);

        // if the tool is the same, swap back to the cached tool
    } else if let Some(cached_tool) = tool_state.cached_tool {
        println!("Tool '{:?}' already selected", cached_tool)
        // let prev_tool = **cur_tool;
        // cur_tool.set(cached_tool, evts);
        // tool_state.cached_tool = Some(prev_tool);
    }
}
