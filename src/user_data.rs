use std::{
    collections::hash_map::DefaultHasher,
    hash::{Hash, Hasher},
    mem::{discriminant, Discriminant},
};

use bevy::{
    ecs::system::SystemId,
    prelude::*,
    utils::hashbrown::{HashMap, HashSet},
};
use serde::{Deserialize, Serialize};

use crate::core::{EditorObject, EditorUtilityObject, LevelEditorState};

// Types: ------------------------------------------------------------------------------------------

pub struct UserDataPlugin;

#[derive(Resource)]
pub struct UserDataListings {
    pub data_listings: Vec<UserDataListing>,
}

#[derive(Debug)]
pub struct UserDataListing {
    pub label: String,
    pub data: TypedUserData,
}

#[derive(Default, Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct UserDataListingHandle {
    handle: ListingHandleType,
}

#[derive(Default, Serialize, Deserialize, Debug, Clone, PartialEq)]
enum ListingHandleType {
    #[default]
    None,
    Index(usize),
}

#[derive(Default, Debug, Serialize, Deserialize, Clone, Copy, PartialEq, Eq)]
pub enum EntityReference {
    #[default]
    None,
    Hashed(u64),
    Ready(Entity),
}

#[derive(Default, Debug, Serialize, Deserialize, Clone, PartialEq)]
pub enum Value {
    #[default]
    Null,
    Bool(bool),
    Int(i32),
    Float(f32),
    Vector(Vec2),
    String(String),
    EntityReference(EntityReference),
    Array(Vec<Value>),
    TypedArray(Box<Value>, Vec<Value>),
    Object(HashMap<String, Value>),
}

#[derive(Component, Debug, Default, Clone)]
pub struct UserDataComponent {
    pub listing: UserDataListingHandle,
    pub data: TypedUserData,
}

#[derive(Component, Debug, Clone)]
pub struct TypedUserDataFiller {
    pub data_untyped: UserData,
}

#[derive(Component, Debug, Clone, Copy)]
pub struct HashIdLink {
    pub hash_id: u64,
}

pub type UserData = serde_json::Map<String, serde_json::Value>;

pub type TypedUserData = HashMap<String, Value>;

pub trait MapEntityUserDataConverter {
    fn to_map_ent_usr_dat(&self) -> UserData;
    fn copy_from_untyped_data(&mut self, untyped_data: &UserData);
}

#[derive(Resource)]
pub struct UserDataSysIds {
    /// Creates a [`HashIdLink`] for each level editor entity in the world and attaches it, should
    /// be run before spawning any entities with a deserialized [`UserDataComponent`] with any
    /// entity reference field, since the entity references are stored as hashed when they are
    /// serialized, and need to link to the entities who exist in the world currently by comparing
    /// their hashes. This happens in the [`handle_hashed_entity_refs`] system, which runs
    /// automatically whenever there are any [`HashIdLink`] components in the world
    pub link_hash_ids: SystemId,
}

// Implementations: --------------------------------------------------------------------------------

impl Plugin for UserDataPlugin {
    fn build(&self, app: &mut App) {
        let sys_ids = UserDataSysIds {
            link_hash_ids: app.world.register_system(link_hash_ids),
        };
        app.insert_resource(sys_ids)
            .insert_resource(UserDataListings::default())
            .add_systems(
                Update,
                (
                    handle_hashed_entity_refs,
                    initialize_user_data_components,
                    fill_typed_user_data,
                )
                    .run_if(in_state(LevelEditorState::Editing)),
            );
    }
}

impl MapEntityUserDataConverter for TypedUserData {
    fn to_map_ent_usr_dat(&self) -> UserData {
        let mut map = UserData::new();
        for (k, v) in self {
            map.insert(k.clone(), v.to_json_value());
        }
        map
    }
    fn copy_from_untyped_data(&mut self, untyped_data: &UserData) {
        copy_recursive_usr_data_json_to_typed(self, untyped_data);
    }
}

impl UserDataComponent {
    pub(crate) fn enforce_data_type(&mut self, type_spec: &TypedUserData) -> bool {
        enforce_data_type_recursive(&mut self.data, type_spec)
    }
}

impl Value {
    pub fn to_json_value(&self) -> serde_json::Value {
        match self {
            Value::Null => serde_json::Value::Null,
            Value::Bool(val) => serde_json::Value::Bool(*val),
            Value::Int(val) => serde_json::Value::Number(serde_json::Number::from(*val)),
            Value::Float(val) => serde_json::Value::Number(
                serde_json::Number::from_f64(*val as f64)
                    .unwrap_or(serde_json::Number::from_f64(0.0).unwrap()),
            ),
            Value::Vector(val) => {
                let mut map = serde_json::Map::<String, serde_json::Value>::default();
                map.insert(
                    "x".to_string(),
                    serde_json::Value::Number(
                        serde_json::Number::from_f64(val.x as f64)
                            .unwrap_or(serde_json::Number::from_f64(0.0).unwrap()),
                    ),
                );
                map.insert(
                    "y".to_string(),
                    serde_json::Value::Number(
                        serde_json::Number::from_f64(val.y as f64)
                            .unwrap_or(serde_json::Number::from_f64(0.0).unwrap()),
                    ),
                );
                serde_json::Value::Object(map)
            }
            Value::String(val) => serde_json::Value::String(val.clone()),
            Value::EntityReference(val) => match val {
                EntityReference::Ready(val) => {
                    let mut hasher = DefaultHasher::new();
                    val.hash(&mut hasher);
                    serde_json::Value::Number(serde_json::Number::from(hasher.finish()))
                }
                EntityReference::Hashed(val) => {
                    serde_json::Value::Number(serde_json::Number::from(*val))
                }
                EntityReference::None => serde_json::Value::Null,
            },
            Value::Array(val) => {
                let mut vec = Vec::<serde_json::Value>::new();
                for val in val {
                    vec.push(val.to_json_value());
                }
                serde_json::Value::Array(vec)
            }
            Value::TypedArray(_, val) => Value::Array(val.clone()).to_json_value(),
            Value::Object(val) => {
                let mut map = serde_json::Map::<String, serde_json::Value>::default();
                for (k, v) in val {
                    map.insert(k.clone(), v.to_json_value());
                }
                serde_json::Value::Object(map)
            }
        }
    }
    pub fn from_json_value(
        untyped: &serde_json::Value,
        expected_type: &Discriminant<Value>,
    ) -> Option<Self> {
        let parsed = match untyped {
            serde_json::Value::Null => Value::Null,
            serde_json::Value::Bool(val) => Value::Bool(*val),
            serde_json::Value::Number(val) => {
                if *expected_type == discriminant(&Value::Int(0)) {
                    if let Some(int) = val.as_i64() {
                        Value::Int(int as i32)
                    } else {
                        Value::Null
                    }
                } else if *expected_type == discriminant(&Value::Float(0.0)) {
                    if let Some(float) = val.as_f64() {
                        Value::Float(float as f32)
                    } else {
                        Value::Null
                    }
                } else if *expected_type == discriminant(&Value::EntityReference(default())) {
                    if let Some(hash) = val.as_u64() {
                        Value::EntityReference(EntityReference::Hashed(hash))
                    } else {
                        Value::EntityReference(EntityReference::None)
                    }
                } else {
                    Value::Null
                }
            }
            serde_json::Value::String(val) => Value::String(val.clone()),
            serde_json::Value::Array(_val) => todo!(),
            serde_json::Value::Object(val) => {
                if *expected_type == discriminant(&Value::Vector(Vec2::ZERO)) {
                    let mut i = 0;
                    let mut vect = Vec2::ZERO;
                    for (u_key, u_val) in val {
                        if i >= 2 {
                            break;
                        }
                        i += 1;
                        if u_key == "x" {
                            if let Some(x) = u_val.as_f64() {
                                vect.x = x as f32;
                            } else {
                                i += 5;
                            }
                        }
                        if u_key == "y" {
                            if let Some(y) = u_val.as_f64() {
                                vect.y = y as f32;
                            } else {
                                i += 5;
                            }
                        }
                    }
                    if i < 2 {
                        Value::Vector(vect)
                    } else {
                        Value::Null
                    }
                } else if *expected_type == discriminant(&Value::Object(default())) {
                    todo!()
                } else {
                    Value::Null
                }
            }
        };

        // only return if it matches the expected enum variant
        if discriminant(&parsed) == *expected_type {
            Some(parsed)
        } else {
            None
        }
    }
    pub fn copy_from_json_value(&mut self, untyped: &serde_json::Value) -> bool {
        match self {
            Value::Null => {
                return untyped.is_null();
            }
            Value::Bool(ref mut val) => {
                if let Some(u_val) = untyped.as_bool() {
                    *val = u_val;
                    true
                } else {
                    false
                }
            }
            Value::Int(ref mut val) => {
                if let Some(u_val) = untyped.as_i64() {
                    *val = u_val as i32;
                    true
                } else {
                    false
                }
            }
            Value::Float(ref mut val) => {
                if let Some(u_val) = untyped.as_f64() {
                    *val = u_val as f32;
                    true
                } else {
                    false
                }
            }
            Value::Vector(ref mut val) => {
                if let Some(u_val) = untyped.as_object() {
                    if let Some(u_val_x) = u_val.get("x") {
                        if let Some(u_val_x) = u_val_x.as_f64() {
                            val.x = u_val_x as f32;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                    if let Some(u_val_y) = u_val.get("y") {
                        if let Some(u_val_y) = u_val_y.as_f64() {
                            val.y = u_val_y as f32;
                            true
                        } else {
                            false
                        }
                    } else {
                        false
                    }
                } else {
                    false
                }
            }
            Value::String(ref mut val) => {
                if let Some(u_val) = untyped.as_str() {
                    *val = u_val.to_string();
                    true
                } else {
                    false
                }
            }
            Value::EntityReference(ref mut val) => {
                if let Some(u_val) = untyped.as_u64() {
                    *val = EntityReference::Hashed(u_val);
                    true
                } else {
                    false
                }
            }
            Value::Array(ref mut val) => {
                if let Some(u_val) = untyped.as_array() {
                    let type_arr: Vec<Discriminant<Value>> =
                        val.iter().map(|x| discriminant(x)).collect();
                    val.clear();
                    for (i, arr_val) in u_val.iter().enumerate() {
                        if i >= type_arr.len() {
                            break;
                        }
                        let t = &type_arr[i];
                        if let Some(arr_val) = Self::from_json_value(arr_val, t) {
                            val.push(arr_val)
                        } else {
                            return false;
                        }
                    }
                    true
                } else {
                    false
                }
            }
            Value::TypedArray(ref t, ref mut val) => {
                if let Some(u_val) = untyped.as_array() {
                    val.clear();
                    for u_arr_val in u_val {
                        let mut arr_val = *t.clone();
                        if !arr_val.copy_from_json_value(u_arr_val) {
                            return false;
                        }
                        val.push(arr_val);
                    }
                    true
                } else {
                    false
                }
            }
            Value::Object(ref mut val) => {
                if let Some(u_val) = untyped.as_object() {
                    copy_recursive_usr_data_json_to_typed(val, u_val);
                    true
                } else {
                    false
                }
            }
        }
    }
}

impl Default for UserDataListings {
    fn default() -> Self {
        Self {
            data_listings: vec![
                UserDataListing {
                    label: "gold_properties".to_string(),
                    data: HashMap::from([("gold".to_string(), Value::Int(1))]),
                },
                UserDataListing {
                    label: "trigger".to_string(),
                    data: HashMap::from([(
                        "linked_entities".to_string(),
                        Value::TypedArray(
                            Box::new(Value::EntityReference(EntityReference::None)),
                            default(),
                        ),
                    )]),
                },
            ],
        }
    }
}

impl UserDataListings {
    fn get_index(&self, handle: &ListingHandleType) -> Option<usize> {
        match handle {
            ListingHandleType::None => None,
            ListingHandleType::Index(i) => {
                if *i < self.data_listings.len() {
                    Some(*i)
                } else {
                    None
                }
            }
        }
    }
    pub fn get(&self, handle: &UserDataListingHandle) -> Option<&UserDataListing> {
        if let Some(index) = self.get_index(&handle.handle) {
            Some(&self.data_listings[index])
        } else {
            None
        }
    }
    pub fn _get_mut(&mut self, handle: &UserDataListingHandle) -> Option<&mut UserDataListing> {
        if let Some(index) = self.get_index(&handle.handle) {
            Some(&mut self.data_listings[index])
        } else {
            None
        }
    }
}

impl UserDataListingHandle {
    pub const INVALID: Self = Self {
        handle: ListingHandleType::None,
    };
    pub fn is_valid(&self) -> bool {
        !matches!(self.handle, ListingHandleType::None)
    }
    pub fn from_index(index: usize) -> Self {
        Self {
            handle: ListingHandleType::Index(index),
        }
    }
    pub fn get_index(&self) -> Option<usize> {
        if let ListingHandleType::Index(ind) = self.handle {
            Some(ind)
        } else {
            None
        }
    }
}

// Utility: ----------------------------------------------------------------------------------------

fn copy_recursive_usr_data_json_to_typed(into: &mut TypedUserData, from: &UserData) {
    for (source_key, source_val) in into.iter_mut() {
        if let Some(targ_val) = from.get(source_key) {
            if !source_val.copy_from_json_value(targ_val) {
                warn!("Mismatching value types at key '{}'", source_key);
            }
        } else {
            warn!("Key '{}' not found in target data", source_key);
        }
    }
}

/// enforce the data to have the same type data structure as the specified type data.
/// fields in the entity's data that don't exist in the type spec will be removed and fields in
/// the type spec that don't exist in the entity data will be added. Fields with mismatching
/// types will be modified
fn enforce_data_type_recursive(target: &mut TypedUserData, type_spec: &TypedUserData) -> bool {
    let mut changed = false;
    let mut to_remove = HashSet::<String>::new();
    let mut to_add = HashMap::<String, Value>::new();
    let mut to_recurse = HashSet::<String>::new();
    for (targ_key, targ_val) in target.iter() {
        let spec_val = type_spec.get(targ_key);
        if let Some(spec_val) = spec_val {
            if discriminant(targ_val) != discriminant(spec_val) {
                // type mismatch: remove field and insert default value
                to_remove.insert(targ_key.clone());
                to_add.insert(targ_key.clone(), spec_val.clone());
            } else {
                // type matches, but could have internal fields that are not matching
                match targ_val {
                    Value::Object(_) => {
                        to_recurse.insert(targ_key.clone());
                    }
                    Value::TypedArray(t, _) => {
                        if let Value::TypedArray(t_spec, _) = spec_val {
                            // array type mismatch
                            if discriminant(&*t.clone()) != discriminant(&*t_spec.clone()) {
                                to_remove.insert(targ_key.clone());
                                to_add.insert(targ_key.clone(), spec_val.clone());
                            }
                        }
                    }
                    _ => {}
                }
            }
        } else {
            // field does not exist in spec
            to_remove.insert(targ_key.clone());
        }
    }
    for (spec_key, spec_val) in type_spec.iter() {
        if target.get(spec_key).is_none() {
            to_add.insert(spec_key.clone(), spec_val.clone());
        }
    }
    for key in &to_recurse {
        if let Value::Object(targ_obj) = target.get_mut(key).unwrap() {
            if let Value::Object(spec_obj) = type_spec.get(key).unwrap() {
                if enforce_data_type_recursive(targ_obj, spec_obj) {
                    changed = true;
                }
            }
        }
    }
    for key in to_remove {
        target.remove(&key);
        changed = true;
    }
    for (key, val) in to_add {
        target.insert(key, val);
        changed = true;
    }
    changed
}

fn set_entity_refs_from_hash_id_recursive(value: &mut Value, hash_id_map: &HashMap<u64, Entity>) {
    match value {
        Value::EntityReference(ent_ref) => {
            if let EntityReference::Hashed(hash) = ent_ref {
                if let Some(ent) = hash_id_map.get(hash) {
                    *ent_ref = EntityReference::Ready(*ent);
                }
            }
        }
        Value::Array(arr) | Value::TypedArray(_, arr) => {
            for val in arr {
                set_entity_refs_from_hash_id_recursive(val, hash_id_map);
            }
        }
        Value::Object(obj) => {
            for (_, val) in obj {
                set_entity_refs_from_hash_id_recursive(val, hash_id_map);
            }
        }
        _ => {}
    }
}

// Systems: ----------------------------------------------------------------------------------------

fn handle_hashed_entity_refs(
    mut commands: Commands,
    mut usr_dat_query: Query<(Entity, &mut UserDataComponent)>,
    hashed_id_query: Query<(Entity, &HashIdLink)>,
) {
    if hashed_id_query.is_empty() {
        return;
    }
    // create a hashmap that links all the hash_ids to the corresponding entity
    let mut hash_id_map: HashMap<u64, Entity> = default();
    for (ent, hash_link) in &hashed_id_query {
        hash_id_map.insert(hash_link.hash_id, ent);
    }
    // iterate through each entity reference in each user data component
    for (_, mut usr_dat) in &mut usr_dat_query {
        for (_, val) in &mut usr_dat.data {
            set_entity_refs_from_hash_id_recursive(val, &hash_id_map);
        }
    }
    // remove all the hash id links
    for (ent, _) in &hashed_id_query {
        commands.entity(ent).remove::<HashIdLink>();
    }
}

/// Creates a [`HashIdLink`] for each level editor entity in the world and attaches it, should be
/// run before spawning any entities with a deserialized [`UserDataComponent`] with any entity
/// reference field, since the entity references are stored as hashed when they are serialized, and
/// need to link to the entities who exist in the world currently by comparing their hashes. This
/// happens in the [`handle_hashed_entity_refs`] system, which runs automatically whenever there
/// are any [`HashIdLink`] components in the world
fn link_hash_ids(
    mut commands: Commands,
    query: Query<Entity, (With<EditorObject>, Without<EditorUtilityObject>)>,
) {
    for ent in &query {
        let mut hasher = DefaultHasher::new();
        ent.hash(&mut hasher);
        commands.entity(ent).insert(HashIdLink {
            hash_id: hasher.finish(),
        });
    }
}

/// Initialize the user data components as they are added to the world
fn initialize_user_data_components(
    data_listings: Res<UserDataListings>,
    mut query: Query<
        &mut UserDataComponent,
        (Added<UserDataComponent>, Without<TypedUserDataFiller>),
    >,
) {
    for mut usr_dat in &mut query {
        if let Some(spec_type) = data_listings.get(&usr_dat.listing) {
            enforce_data_type_recursive(&mut usr_dat.data, &spec_type.data);
        }
    }
}

/// Fill user typed data from untyped data on entities that need it
fn fill_typed_user_data(
    mut commands: Commands,
    data_listings: Res<UserDataListings>,
    mut query: Query<(Entity, &mut UserDataComponent, &TypedUserDataFiller)>,
) {
    for (ent, mut typed_usr_dat, untyped_usr_dat) in &mut query {
        if let Some(listing) = data_listings.get(&typed_usr_dat.listing) {
            typed_usr_dat.data = listing.data.clone();
            typed_usr_dat
                .data
                .copy_from_untyped_data(&untyped_usr_dat.data_untyped);
            commands.entity(ent).remove::<TypedUserDataFiller>();
        }
    }
}
