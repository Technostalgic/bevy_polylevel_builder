use bevy::prelude::*;

use crate::{
    controls::{AbstractClick, AbstractMouseState},
    map_entity::MapEntityListings,
};

use super::EditorTool;

// -------------------------------------------------------------------------------------------------

pub(super) struct EntityToolPlugin;

#[derive(Resource, Default)]
pub struct EntityToolProperties {
    pub cur_selected_type: usize,
}

// -------------------------------------------------------------------------------------------------

impl Plugin for EntityToolPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(EntityToolProperties::default())
            .add_systems(
                Update,
                handle_entity_tool
                    .run_if(resource_exists_and_equals::<EditorTool>(EditorTool::Entity)),
            );
    }
}

// -------------------------------------------------------------------------------------------------

fn handle_entity_tool(
    mut commands: Commands,
    mouse: Res<AbstractMouseState>,
    ent_listings: Res<MapEntityListings>,
    mut tool_props: ResMut<EntityToolProperties>,
) {
    if mouse.inside_ui() {
        return;
    }
    // spawn entity on click
    if mouse.just_pressed(&AbstractClick::Primary) {
        if tool_props.cur_selected_type >= ent_listings.entity_types.len() {
            warn!("Invalid level entity selected");
            tool_props.cur_selected_type = 0;
        } else {
            let listing = &ent_listings.entity_types[tool_props.cur_selected_type];
            listing.create_instance_at(&mut commands, mouse.position_world_snapped());
        }
    }
}
