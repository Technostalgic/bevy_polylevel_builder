use bevy::{
    ecs::system::SystemId,
    prelude::*,
    utils::{hashbrown::hash_set::Iter, HashSet},
};

use crate::{
    controls::{AbstractClick, AbstractDragState, AbstractMouseState},
    core::{
        EditorObject, LevelEditProperties, LevelEditorState, UI_TOOL_FOCUS_Z_ORDER,
        UI_TOOL_GIZMO_Z_ORDER,
    },
    geometry::{GeometryComponent, TransformPoint2D},
    gizmo_extension::GizmosExt,
    selection::SelectedEntities,
};

use super::{EditorTool, SelectionModifier, SelectionModifiers, SelectionRectGraphic};

// -------------------------------------------------------------------------------------------------

const VERT_FOCUS_COLOR: Color = Color::rgba(0.5, 0.5, 1.0, 1.0);

const VERT_SELECT_COLOR: Color = Color::BLUE;

pub(super) struct VertexToolPlugin;

#[derive(Default, Clone, Copy, PartialEq, Eq)]
enum VertexToolClickMode {
    #[default]
    None,
    Pick,
    RectSelect,
    Translate,
}

#[derive(Default, Debug)]
struct SelectedVerts {
    set: HashSet<(Entity, usize)>,
}

#[derive(Resource, Default)]
pub struct VertexToolProperties {
    pub snap_selected_verts: Option<SystemId>,
    focused_vert: Option<(Entity, usize)>,
    selected_verts: SelectedVerts,
    cur_click_mode: VertexToolClickMode,
}

// -------------------------------------------------------------------------------------------------

impl Plugin for VertexToolPlugin {
    fn build(&self, app: &mut App) {
        let tool_props = VertexToolProperties {
            snap_selected_verts: Some(app.world.register_system(snap_selected_verts_to_grid)),
            ..Default::default()
        };
        app.insert_resource(tool_props).add_systems(
            Update,
            (handle_vertex_tool, render_vertex_tool)
                .run_if(in_state(LevelEditorState::Editing))
                .run_if(resource_exists_and_equals::<EditorTool>(EditorTool::Vertex)),
        );
    }
}

impl SelectedVerts {
    /// modify the selection, adding or removing the specified objects from the set based on the
    /// given selection modifiers
    pub fn modify_selection<T: IntoIterator<Item = (Entity, usize)>>(
        &mut self,
        targets: T,
        modifiers: &Res<SelectionModifiers>,
    ) {
        if modifiers.contains(&SelectionModifier::Union) {
            for (ent, ind) in targets {
                self.insert(ent, ind);
            }
        } else if modifiers.contains(&SelectionModifier::Difference) {
            for (ent, ind) in targets {
                self.remove(ent, ind);
            }
        } else {
            self.clear();
            for (ent, ind) in targets {
                self.insert(ent, ind);
            }
        }
    }

    /// insert a specific vertex into the selection so it's considered to be selected for this set,
    /// does nothing if vert is already inside set
    pub fn insert(&mut self, geom_entity: Entity, vert_index: usize) -> bool {
        self.set.insert((geom_entity, vert_index))
    }

    /// remove a specific vertex form the selection so it's no longer considered to be selected,
    /// does nothing if vert does not exist in set
    pub fn remove(&mut self, geom_entity: Entity, vert_index: usize) -> bool {
        self.set.remove(&(geom_entity, vert_index))
    }

    /// clear all vertices from the selection
    pub fn clear(&mut self) {
        self.set.clear();
    }

    pub fn iter<'a>(&'a self) -> Iter<'a, (Entity, usize)> {
        self.set.iter()
    }
}

// -------------------------------------------------------------------------------------------------

fn handle_vertex_tool(
    selection: Res<SelectedEntities>,
    selection_modifiers: Res<SelectionModifiers>,
    mouse: Res<AbstractMouseState>,
    mouse_drag: Res<AbstractDragState>,
    mut geom_query: Query<(&GlobalTransform, &mut GeometryComponent)>,
    mut tool_props: ResMut<VertexToolProperties>,
) {
    // iterate through each selected poly and get closest vert
    let mut closest_result: Option<(Entity, usize, f32)> = None;
    for ent in selection.iter() {
        if let Ok((glob_trans, geom)) = geom_query.get(*ent) {
            let local_pt = glob_trans.inverse_transform_point2(mouse.position_world());
            let (index, dist_sq) = geom.poly().closest_vertex(local_pt);
            if let Some((_, _, old_dist_sq)) = closest_result {
                if dist_sq < old_dist_sq {
                    closest_result = Some((*ent, index, dist_sq));
                }
            } else {
                closest_result = Some((*ent, index, dist_sq));
            }
        }
    }

    // set the tool properties to reflect the focused closest vertex
    if let Some((closest_ent, closest_vert_ind, _)) = closest_result {
        tool_props.focused_vert = Some((closest_ent, closest_vert_ind));
    } else {
        tool_props.focused_vert = None;
    }

    // reset click mode on mouse click
    if mouse.just_pressed(&AbstractClick::Primary) {
        tool_props.cur_click_mode = VertexToolClickMode::Pick;
    }

    // set to translate mode on right click
    if mouse.just_pressed(&AbstractClick::Secondary) {
        tool_props.cur_click_mode = VertexToolClickMode::Translate;
    }

    // switch to rect select mode if started dragging while mouse held
    if mouse_drag.is_dragging_world() && tool_props.cur_click_mode == VertexToolClickMode::Pick {
        tool_props.cur_click_mode = VertexToolClickMode::RectSelect;
    }

    // handle selecting vertices
    if mouse.just_released_any() {
        if !mouse.inside_ui() {
            match tool_props.cur_click_mode {
                // select individual verts
                VertexToolClickMode::Pick => {
                    if let Some(focused_vert) = tool_props.focused_vert {
                        tool_props
                            .selected_verts
                            .modify_selection([focused_vert], &selection_modifiers);
                    }
                }
                // rect select
                VertexToolClickMode::RectSelect => {
                    let mut select_verts: Vec<(Entity, usize)> = Vec::new();
                    let sel_rect = Rect::from_corners(
                        mouse_drag.start_pos_world(),
                        mouse_drag.end_pos_world(),
                    );
                    for ent in selection.iter() {
                        if let Ok((glob_trans, geom)) = geom_query.get(*ent) {
                            for (index, vert) in geom.poly().verts().iter().enumerate() {
                                let glob_vert = glob_trans.transform_point2(*vert);
                                if sel_rect.contains(glob_vert) {
                                    select_verts.push((*ent, index));
                                }
                            }
                        }
                    }
                    tool_props
                        .selected_verts
                        .modify_selection(select_verts, &selection_modifiers);
                }
                _ => {}
            }
        }
        // reset click mode on release
        tool_props.cur_click_mode = VertexToolClickMode::None;
    }

    // move vertices
    if mouse_drag.is_dragging_world() && tool_props.cur_click_mode == VertexToolClickMode::Translate
    {
        let delta = mouse_drag.frame_delta_world_snapped();
        for (vert_ent, vert_index) in tool_props.selected_verts.iter() {
            if let Ok((_glob_trans, mut geom)) = geom_query.get_mut(*vert_ent) {
                if *vert_index < geom.poly().verts().len() {
                    geom.poly_mut().verts_mut()[*vert_index] += delta;
                }
            }
        }
    }
}

fn render_vertex_tool(
    mut sel_rect_gfx: ResMut<SelectionRectGraphic>,
    tool_properties: Res<VertexToolProperties>,
    mouse_drag: Res<AbstractDragState>,
    mouse: Res<AbstractMouseState>,
    mut gizmo_ext: ResMut<GizmosExt>,
    geom_query: Query<(&GlobalTransform, &GeometryComponent)>,
) {
    if !mouse.inside_ui() {
        // render selection rect
        if tool_properties.cur_click_mode == VertexToolClickMode::RectSelect {
            sel_rect_gfx.visible = true;
            sel_rect_gfx.start_pos_world = mouse_drag.start_pos_world();
            sel_rect_gfx.end_pos_world = mouse_drag.end_pos_world();
        } else {
            sel_rect_gfx.visible = false;
        }

        // render focused vert
        if let Some((ent, index)) = tool_properties.focused_vert {
            if let Ok((geom_glob_trans, geom)) = geom_query.get(ent) {
                let pos = geom_glob_trans.transform_point2(geom.poly().verts()[index]);
                gizmo_ext.draw_dot(pos, VERT_FOCUS_COLOR, 4.0, UI_TOOL_FOCUS_Z_ORDER);
            }
        }
    }

    // render selected verts
    for (ent, ind) in tool_properties.selected_verts.iter() {
        if let Ok((geom_glob_trans, geom)) = geom_query.get(*ent) {
            // if index does not exist, skip this selected vertex
            if *ind >= geom.poly().verts().len() {
                continue;
            }
            let pos = geom_glob_trans.transform_point2(geom.poly().verts()[*ind]);
            let mut size = 4.0;
            if let Some((_, index)) = tool_properties.focused_vert {
                if *ind == index {
                    size += 4.0;
                }
            }
            gizmo_ext.draw_dot(pos, VERT_SELECT_COLOR, size, UI_TOOL_GIZMO_Z_ORDER);
        }
    }
}

fn snap_selected_verts_to_grid(
    editor_props: Res<LevelEditProperties>,
    tool_props: Res<VertexToolProperties>,
    mut geom_query: Query<(&GlobalTransform, &mut GeometryComponent), With<EditorObject>>,
) {
    for (ent, vert_sel) in tool_props.selected_verts.iter() {
        let Ok((glob_trans, mut geom)) = geom_query.get_mut(*ent) else {
            continue;
        };
        let verts = geom.poly_mut().verts_mut();
        let vert = (glob_trans.transform_point2(verts[*vert_sel]) / editor_props.grid_size).round()
            * editor_props.grid_size;
        verts[*vert_sel] = glob_trans.inverse_transform_point2(vert);
    }
}
