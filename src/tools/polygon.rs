use bevy::prelude::*;

use crate::{
    controls::{AbstractClick, AbstractMouseState},
    core::EditorObject,
    core::LevelEditorState,
    core::{EditorWorldObject, PixelWorldSize, UI_TOOL_FOCUS_Z_ORDER},
    geometry::{GeometryBundle, GeometryComponent, TransformPoint2D},
    gizmo_extension::GizmosExt,
    selection::SelectedEntities,
};

use super::{EditorTool, POLY_ADD_COLOR};

// -------------------------------------------------------------------------------------------------

pub(super) struct PolygonToolPlugin;

#[derive(Clone, Copy, PartialEq, Eq, Default)]
pub enum PolyIndexType {
    #[default]
    Vertex,
    Edge,
}

#[derive(Resource, Default)]
pub struct PolygonToolProperties {
    edge_point: Option<Vec2>,
    closest_poly: Option<Entity>,
    poly_index: usize,
    index_type: PolyIndexType,
    dragging_vert: bool,
    deleting_vert: bool,
}

// -------------------------------------------------------------------------------------------------

impl Plugin for PolygonToolPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(PolygonToolProperties::default())
            .add_systems(
                Update,
                (handle_polygon_tool, render_polygon_tool)
                    .run_if(resource_exists_and_equals::<EditorTool>(
                        EditorTool::Polygon,
                    ))
                    .run_if(in_state(LevelEditorState::Editing)),
            );
    }
}

// -------------------------------------------------------------------------------------------------

/// A system that handles the polygon tool functionality
fn handle_polygon_tool(
    mut commands: Commands,
    mut tool_properties: ResMut<PolygonToolProperties>,
    selection: ResMut<SelectedEntities>,
    mouse: Res<AbstractMouseState>,
    pix_world: Res<PixelWorldSize>,
    mut geometries: Query<(&GlobalTransform, &mut GeometryComponent)>,
) {
    tool_properties.edge_point = None;
    if !mouse.inside_ui() {
        // if no polygon selected, stamp new polygon
        if selection.len() <= 0 {
            tool_properties.closest_poly = None;
            if mouse.just_pressed(&AbstractClick::Primary) {
                let click_pos = mouse.position_world_snapped();
                commands.spawn((
                    EditorObject,
                    EditorWorldObject,
                    GeometryBundle::square(click_pos, 20.0),
                ));
            }
        }
        // if polygon(s) are selected
        else {
            // choose closest polygon and point to cursor
            let mut result: Option<(Entity, (Vec2, usize, f32))> = None;
            for ent in selection.iter() {
                if let Ok((glob_trans, geom)) = geometries.get(*ent) {
                    let local_point = glob_trans.inverse_transform_point2(mouse.position_world());
                    let closest_edge = geom.poly().closest_local_edge_point(local_point);
                    if let Some(res) = result {
                        if res.1 .2 > closest_edge.2 {
                            result = Some((*ent, closest_edge));
                        }
                    } else {
                        result = Some((*ent, closest_edge));
                    }
                }
            }

            // if there is a closest polygon
            if let Some(result) = result {
                // use specified poly from drag if currently dragging a vert
                let closest_poly_ent = if tool_properties.dragging_vert {
                    tool_properties.closest_poly.unwrap()
                } else {
                    result.0
                };

                if let Ok((glob_trans, mut geom)) = geometries.get_mut(closest_poly_ent) {
                    // handle setting polygon tool properties
                    if !tool_properties.dragging_vert {
                        let ent = result.0;
                        tool_properties.closest_poly = Some(ent);

                        let (edge_point, index, _dist_sq) = result.1;

                        let glob_edge_point = glob_trans.transform_point2(edge_point);
                        tool_properties.edge_point = Some(glob_edge_point);

                        let vert_count = geom.poly().verts().len();
                        let cur_vert = glob_trans.transform_point2(geom.poly().verts()[index]);
                        let nex_vert = glob_trans
                            .transform_point2(geom.poly().verts()[(index + 1) % vert_count]);
                        let thresh = pix_world.0 * 4.0;
                        if cur_vert.distance(glob_edge_point) <= thresh {
                            tool_properties.poly_index = match index {
                                0 => vert_count - 1,
                                _ => index - 1,
                            };
                            tool_properties.index_type = PolyIndexType::Vertex;
                        } else if nex_vert.distance(glob_edge_point) <= thresh {
                            tool_properties.poly_index = index;
                            tool_properties.index_type = PolyIndexType::Vertex;
                        } else {
                            tool_properties.poly_index = index;
                            if !tool_properties.deleting_vert {
                                tool_properties.index_type = PolyIndexType::Edge;
                            }
                        }
                    }

                    // handle actions
                    if !mouse.inside_ui() {
                        let local_mouse_pos =
                            glob_trans.inverse_transform_point2(mouse.position_world_snapped());

                        if mouse.just_pressed(&AbstractClick::Primary) {
                            tool_properties.dragging_vert = true;

                            // add new vert if edge mode
                            if tool_properties.index_type == PolyIndexType::Edge {
                                geom.insert_local_vert(
                                    tool_properties.poly_index + 1,
                                    local_mouse_pos,
                                );
                            }

                            let vert_count = geom.poly().verts().len();
                            tool_properties.poly_index =
                                (tool_properties.poly_index + 1) % vert_count;
                        } else if mouse.just_released(&AbstractClick::Primary) {
                            tool_properties.dragging_vert = false;
                        } else if tool_properties.dragging_vert {
                            geom.poly_mut().verts_mut()[tool_properties.poly_index] =
                                local_mouse_pos;
                        }

                        if mouse.just_pressed(&AbstractClick::Secondary) {
                            tool_properties.deleting_vert = true;
                            tool_properties.index_type = PolyIndexType::Vertex;
                        } else if mouse.just_released(&AbstractClick::Secondary) {
                            if tool_properties.deleting_vert && !tool_properties.dragging_vert {
                                let vert_count = geom.poly().verts().len();
                                if vert_count > 3 {
                                    geom.remove_vert_at(
                                        (tool_properties.poly_index + 1) % vert_count,
                                    );
                                }
                            }
                            tool_properties.deleting_vert = false;
                        }
                    }
                }
            }
        }

    // mouse outside ui
    } else {
        tool_properties.dragging_vert = false;
        tool_properties.deleting_vert = false;
    }
}

/// renders the polygon tool extra lines and points
fn render_polygon_tool(
    tool_properties: Res<PolygonToolProperties>,
    mouse: Res<AbstractMouseState>,
    mut gizmo_ext: ResMut<GizmosExt>,
    query_poly: Query<(&GlobalTransform, &GeometryComponent)>,
) {
    // hide edge and point previews while dragging verts
    if tool_properties.dragging_vert || mouse.inside_ui() {
        return;
    }

    if let Some(closest_poly) = tool_properties.closest_poly {
        if let Ok((glob_trans, geom)) = query_poly.get(closest_poly) {
            // draw edge/vertex point
            if let Some(point) = tool_properties.edge_point {
                let pos = match tool_properties.index_type {
                    PolyIndexType::Edge => point,
                    PolyIndexType::Vertex => {
                        let index = (tool_properties.poly_index + 1) % geom.poly().verts().len();
                        glob_trans.transform_point2(geom.poly().verts()[index])
                    }
                };
                let color = if tool_properties.deleting_vert {
                    Color::RED
                } else {
                    POLY_ADD_COLOR
                };
                let scl = match tool_properties.index_type {
                    PolyIndexType::Edge => 4.0,
                    PolyIndexType::Vertex => 8.0,
                };
                gizmo_ext.draw_dot(pos, color, scl, UI_TOOL_FOCUS_Z_ORDER);
            }

            // draw new edge previews
            if !tool_properties.deleting_vert {
                let targ_pos = mouse.position_world_snapped();
                for i in 0..2 {
                    let vert_count = geom.poly().verts().len();
                    let vert_index = match tool_properties.index_type {
                        PolyIndexType::Vertex => (tool_properties.poly_index + i * 2) % vert_count,
                        PolyIndexType::Edge => (tool_properties.poly_index + i) % vert_count,
                    };
                    let local_vert = geom.poly().verts()[vert_index];
                    let glob_vert = glob_trans.transform_point2(local_vert);
                    gizmo_ext.draw_line(
                        glob_vert,
                        targ_pos,
                        POLY_ADD_COLOR,
                        2.0,
                        UI_TOOL_FOCUS_Z_ORDER,
                    );
                }
            }
        }
    }
}
