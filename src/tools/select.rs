use bevy::{ecs::system::SystemId, prelude::*, render::primitives::Aabb, sprite::Anchor};

use crate::{
    controls::{AbstractClick, AbstractDragState, AbstractMouseState},
    core::{EditorWorldObject, LevelEditorState, UI_TOOL_FOCUS_Z_ORDER, UI_TOOL_GIZMO_Z_ORDER},
    geometry::GeometryComponent,
    gizmo_extension::GizmosExt,
    selection::SelectedEntities,
    utility::{get_geoms_in_aabb, get_mouse_ent, get_selectable_non_geoms_in_aabb},
};

use super::{EditorTool, SelectionModifiers, SelectionRectGraphic};

// -------------------------------------------------------------------------------------------------

pub const SEL_HOVERED_COLOR: Color = Color::GREEN;

pub(super) struct SelectToolPlugin;

#[derive(Default, Debug, PartialEq, Eq, Clone, Copy)]
pub enum SelectDragMode {
    #[default]
    None,
    SelectArea,
    TranslateSelection,
}

#[derive(Resource, Default)]
pub struct SelectToolProperties {
    pub render_selection: bool,
    pub drag_mode: SelectDragMode,
}

#[derive(Resource)]
pub(super) struct SelectToolSystemIds {
    pub clean_render: SystemId,
}

// -------------------------------------------------------------------------------------------------

impl Plugin for SelectToolPlugin {
    fn build(&self, app: &mut App) {
        let sys_ids = SelectToolSystemIds {
            clean_render: app.world.register_system(clean_select_render),
        };
        app.insert_resource(SelectToolProperties::default())
            .insert_resource(sys_ids)
            .add_systems(
                Update,
                (handle_select_tool, render_select_tool, render_ruler)
                    .run_if(in_state(LevelEditorState::Editing))
                    .run_if(resource_exists_and_equals::<EditorTool>(EditorTool::Select)),
            );
    }
}

// -------------------------------------------------------------------------------------------------

/// A system that handles the selection tool functionality
fn handle_select_tool(
    mut selection: ResMut<SelectedEntities>,
    mut tool_state: ResMut<SelectToolProperties>,
    selection_modifiers: Res<SelectionModifiers>,
    mouse: Res<AbstractMouseState>,
    mouse_drag: Res<AbstractDragState>,
    geometries: Query<(Entity, &GlobalTransform, &GeometryComponent)>,
    non_geoms: Query<
        (Entity, &GlobalTransform, &Aabb),
        (With<EditorWorldObject>, Without<GeometryComponent>),
    >,
    mut sel_trans: Query<&mut Transform>,
) {
    if mouse.inside_ui() {
        return;
    }

    // get the entity currently under the mouse
    let mouse_ent: Option<Entity> = get_mouse_ent(&mouse, &geometries, &non_geoms, &selection);
    if mouse.just_pressed(&AbstractClick::Primary) {
        if mouse_ent.is_none() {
            tool_state.drag_mode = SelectDragMode::SelectArea;
        } else {
            tool_state.drag_mode = SelectDragMode::None;
        }
    }

    // if dragging a selected entity, switch to translate mode
    if mouse_drag.is_dragging_world() && tool_state.drag_mode == SelectDragMode::None {
        if let Some(mouse_ent) = mouse_ent {
            if selection.contains(&mouse_ent) {
                tool_state.drag_mode = SelectDragMode::TranslateSelection;
            }
        }
    }

    // mouse dragging actions
    if mouse_drag.is_dragging_world() && mouse_drag.click_type() == AbstractClick::Primary {
        // move selected entities
        if tool_state.drag_mode == SelectDragMode::TranslateSelection {
            for ent in selection.iter() {
                if let Ok(mut trans) = sel_trans.get_mut(*ent) {
                    let drag_delta = mouse_drag.frame_delta_world_snapped();
                    trans.translation.x += drag_delta.x;
                    trans.translation.y += drag_delta.y;
                }
            }

        // create selection rect
        } else if tool_state.drag_mode == SelectDragMode::None {
            tool_state.drag_mode = SelectDragMode::SelectArea;
        }
    }

    // evaluate selection on mouse release
    if mouse.just_released(&AbstractClick::Primary) {
        match tool_state.drag_mode {
            SelectDragMode::None => {
                selection.modify_selection(mouse_ent, &selection_modifiers);
            }
            SelectDragMode::SelectArea => {
                let sel_rect =
                    Rect::from_corners(mouse_drag.start_pos_world(), mouse_drag.end_pos_world());
                let mut sel = get_geoms_in_aabb(sel_rect, &geometries);
                sel.append(&mut get_selectable_non_geoms_in_aabb(sel_rect, &non_geoms));
                selection.modify_selection(sel, &selection_modifiers);
            }
            _ => {}
        }
        tool_state.drag_mode = SelectDragMode::None;
    }
}

/// renders the selection tool stuff
fn render_select_tool(
    mut sel_rect: ResMut<SelectionRectGraphic>,
    mut gizmo_ext: ResMut<GizmosExt>,
    select_properties: Res<SelectToolProperties>,
    mouse_drag: Res<AbstractDragState>,
    mouse: Res<AbstractMouseState>,
    selection: ResMut<SelectedEntities>,
    geometries: Query<(Entity, &GlobalTransform, &GeometryComponent)>,
    non_geoms: Query<
        (Entity, &GlobalTransform, &Aabb),
        (With<EditorWorldObject>, Without<GeometryComponent>),
    >,
) {
    if select_properties.drag_mode != SelectDragMode::SelectArea {
        sel_rect.visible = false;
    } else {
        sel_rect.visible = true;
        sel_rect.start_pos_world = mouse_drag.start_pos_world();
        sel_rect.end_pos_world = mouse_drag.end_pos_world();
    }

    // render mouse entity if not making selection rect
    if select_properties.drag_mode != SelectDragMode::SelectArea {
        if let Some(mouse_ent) = get_mouse_ent(&mouse, &geometries, &non_geoms, &selection) {
            gizmo_ext.draw_outline(mouse_ent, SEL_HOVERED_COLOR, 4.0, 0.5);
        }
    }
}

fn render_ruler(drag: Res<AbstractDragState>, mut gizmo: ResMut<GizmosExt>) {
    if !drag.is_dragging_world() || drag.click_type() != AbstractClick::Secondary {
        return;
    }
    const RULER_COL: Color = Color::rgb(0.5, 0.7, 1.0);
    let start_pos = drag.start_pos_world_snapped();
    let end_pos = drag.end_pos_world_snapped();
    let dif = end_pos - start_pos;
    gizmo.draw_text(
        format!("{},{}", start_pos.x, start_pos.y),
        start_pos,
        RULER_COL,
        Anchor::BottomRight,
        UI_TOOL_FOCUS_Z_ORDER,
    );
    gizmo.draw_text(
        format!("{},{}", dif.x, dif.y),
        end_pos,
        RULER_COL,
        Anchor::BottomLeft,
        UI_TOOL_FOCUS_Z_ORDER,
    );
    gizmo.draw_rect(start_pos, end_pos, RULER_COL, 2.0, UI_TOOL_GIZMO_Z_ORDER);
    gizmo.draw_line(start_pos, end_pos, RULER_COL, 2.0, UI_TOOL_GIZMO_Z_ORDER);
}

fn clean_select_render(mut sel_rect: ResMut<SelectionRectGraphic>) {
    sel_rect.visible = false;
}
