use bevy::prelude::*;

use crate::{
    controls::{AbstractClick, AbstractMouseState},
    core::{EditorObject, UI_TOOL_GIZMO_Z_ORDER},
    geometry::{GeometryComponent, TransformPoint2D},
    gizmo_extension::GizmosExt,
    map_entity::MapEntity,
    selection::SelectedEntities,
    utility::RotateBy,
};

use super::EditorTool;

// Defintions: -------------------------------------------------------------------------------------

pub struct TransformToolPlugin;

#[derive(Resource, Default)]
pub struct TransformToolProperties {
    pub transform_mode: TransformMode,
    action_mode: ActionMode,
    selection_min: Vec2,
    selection_max: Vec2,
    forced_anchor: Option<Vec2>,
}

#[derive(Default, Clone, Copy, PartialEq, Eq, Hash)]
pub enum TransformMode {
    #[default]
    Rotate,
    Scale,
}

#[derive(Default, Clone, Copy, PartialEq)]
enum ActionMode {
    #[default]
    Idle,
    Transforming {
        start_pos: Vec2,
        target_pos: Vec2,
    },
}

// Implementations: --------------------------------------------------------------------------------

impl Plugin for TransformToolPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(TransformToolProperties::default())
            .add_systems(
                Update,
                (handle_transform_tool, draw_transform_tool)
                    .chain_ignore_deferred()
                    .run_if(resource_exists_and_equals(EditorTool::Transform)),
            );
    }
}

impl TransformToolProperties {
    pub fn current_rotation(&self) -> f32 {
        if !matches!(self.transform_mode, TransformMode::Rotate) {
            return 0.0;
        }
        let ActionMode::Transforming {
            start_pos,
            target_pos,
        } = self.action_mode
        else {
            return 0.0;
        };
        let centroid = self.anchor_point();
        return (target_pos - centroid).to_angle() - (start_pos - centroid).to_angle();
    }
    pub fn current_scaling(&self) -> Vec2 {
        if !matches!(self.transform_mode, TransformMode::Scale) {
            return Vec2::ONE;
        }
        let ActionMode::Transforming {
            start_pos,
            target_pos,
        } = self.action_mode
        else {
            return Vec2::ONE;
        };
        let centroid = self.anchor_point();
        let start_dif = start_pos - centroid;
        let end_dif = target_pos - centroid;
        let mut scale = Vec2::ONE;
        if start_dif.x != 0.0 {
            scale.x = end_dif.x / start_dif.x;
        }
        if start_dif.y != 0.0 {
            scale.y = end_dif.y / start_dif.y;
        }
        scale
    }
    pub fn anchor_point(&self) -> Vec2 {
        self.forced_anchor
            .unwrap_or((self.selection_min + self.selection_max) * 0.5)
    }
}

// Systems: ----------------------------------------------------------------------------------------

fn handle_transform_tool(
    selection: Res<SelectedEntities>,
    mouse: Res<AbstractMouseState>,
    mut tool_props: ResMut<TransformToolProperties>,
    mut ent_query: Query<(&GlobalTransform, &mut Transform), (With<MapEntity>, With<EditorObject>)>,
    mut geom_query: Query<(&GlobalTransform, &mut GeometryComponent), With<EditorObject>>,
) {
    let mut recalc_rect = false;
    let mut apply_transform = false;
    if !mouse.inside_ui() {
        // create/remove/move forced anchor point
        if mouse.just_pressed(&AbstractClick::Secondary) {
            tool_props.forced_anchor = match tool_props.forced_anchor {
                Some(_) => None,
                None => Some(mouse.position_world_snapped()),
            }
        }
        if mouse.pressed(&AbstractClick::Secondary) {
            match &mut tool_props.forced_anchor {
                Some(pos) => {
                    *pos = mouse.position_world_snapped();
                }
                None => {}
            }
        }
        // manipulate target point from which transform calculation comes from
        if mouse.just_pressed(&AbstractClick::Primary) {
            match tool_props.action_mode {
                ActionMode::Idle => {
                    recalc_rect = true;
                    tool_props.action_mode = ActionMode::Transforming {
                        start_pos: mouse.position_world_snapped(),
                        target_pos: mouse.position_world_snapped(),
                    }
                }
                _ => {}
            }
        } else if mouse.just_released(&AbstractClick::Primary) {
            match tool_props.action_mode {
                ActionMode::Transforming {
                    start_pos: _,
                    target_pos: _,
                } => {
                    apply_transform = true;
                }
                _ => {}
            }
        }
        match &mut tool_props.action_mode {
            ActionMode::Transforming {
                start_pos: _,
                target_pos,
            } => {
                *target_pos = mouse.position_world_snapped();
            }
            _ => {}
        }
    }

    // calculate the bounding rect over all the selected entities and geometries
    if recalc_rect {
        let mut min: Option<Vec2> = None;
        let mut max: Option<Vec2> = None;
        for ent in &selection.entities {
            if let Ok((glob_trans, _)) = ent_query.get(*ent) {
                min = Some(match min {
                    None => glob_trans.translation().truncate(),
                    Some(min) => min.min(glob_trans.translation().truncate()),
                });
                max = Some(match max {
                    None => glob_trans.translation().truncate(),
                    Some(max) => max.max(glob_trans.translation().truncate()),
                });
            } else if let Ok((glob_trans, geom)) = geom_query.get(*ent) {
                for vert in geom.poly().verts() {
                    let glob_vert = glob_trans.transform_point2(*vert);
                    min = Some(match min {
                        None => glob_vert,
                        Some(min) => min.min(glob_vert),
                    });
                    max = Some(match max {
                        None => glob_vert,
                        Some(max) => max.max(glob_vert),
                    });
                }
            }
        }
        if let Some(min) = min {
            tool_props.selection_min = min;
        }
        if let Some(max) = max {
            tool_props.selection_max = max;
        }
    }
    // now we will apply the transformation
    if !apply_transform {
        return;
    }
    let centroid = tool_props.anchor_point();
    let rotation = tool_props.current_rotation();
    let scale = tool_props.current_scaling();
    for ent in &selection.entities {
        if let Ok((_, mut trans)) = ent_query.get_mut(*ent) {
            let dif = trans.translation.truncate() - centroid;
            let dif_new = dif.rotate_by(rotation) * scale;
            trans.translation.x = centroid.x + dif_new.x;
            trans.translation.y = centroid.y + dif_new.y;
        } else if let Ok((glob_trans, mut geom)) = geom_query.get_mut(*ent) {
            let loc_centroid = glob_trans.inverse_transform_point2(centroid);
            for vert in geom.poly_mut().verts_mut() {
                let dif = *vert - loc_centroid;
                let dif_new = dif.rotate_by(rotation) * scale;
                vert.x = loc_centroid.x + dif_new.x;
                vert.y = loc_centroid.y + dif_new.y;
            }
        }
    }
    tool_props.action_mode = ActionMode::Idle;
}

fn draw_transform_tool(tool_props: Res<TransformToolProperties>, mut gizmos: ResMut<GizmosExt>) {
    const COLOR_TRANS: Color = Color::ORANGE_RED;
    const COLOR_ORIG: Color = Color::ORANGE;
    if let Some(forced_anchor) = tool_props.forced_anchor {
        gizmos.draw_dot(forced_anchor, COLOR_TRANS, 6.0, UI_TOOL_GIZMO_Z_ORDER);
    }

    let ActionMode::Transforming {
        start_pos,
        target_pos,
    } = tool_props.action_mode
    else {
        return;
    };

    let centroid = tool_props.anchor_point();

    // start/target mouse and centroid positions
    gizmos.draw_dot(start_pos, COLOR_TRANS, 4.0, UI_TOOL_GIZMO_Z_ORDER);
    gizmos.draw_dot(target_pos, COLOR_TRANS, 4.0, UI_TOOL_GIZMO_Z_ORDER);
    gizmos.draw_dot(centroid, COLOR_ORIG, 4.0, UI_TOOL_GIZMO_Z_ORDER + 0.5);

    // draw lines if rotate mode
    if matches!(tool_props.transform_mode, TransformMode::Rotate) {
        gizmos.draw_line(centroid, start_pos, COLOR_ORIG, 2.0, UI_TOOL_GIZMO_Z_ORDER);
        gizmos.draw_line(
            centroid,
            target_pos,
            COLOR_TRANS,
            2.0,
            UI_TOOL_GIZMO_Z_ORDER,
        );
    }

    // original box
    gizmos.draw_rect(
        tool_props.selection_min,
        tool_props.selection_max,
        COLOR_ORIG.with_a(0.5),
        2.0,
        UI_TOOL_GIZMO_Z_ORDER,
    );

    // transformed box
    let min = tool_props.selection_min;
    let max = tool_props.selection_max;
    let rot = tool_props.current_rotation();
    let scl = tool_props.current_scaling();
    let corners: [Vec2; 4] = [
        ((Vec2::new(min.x, min.y) - centroid) * scl).rotate_by(rot) + centroid,
        ((Vec2::new(min.x, max.y) - centroid) * scl).rotate_by(rot) + centroid,
        ((Vec2::new(max.x, max.y) - centroid) * scl).rotate_by(rot) + centroid,
        ((Vec2::new(max.x, min.y) - centroid) * scl).rotate_by(rot) + centroid,
    ];
    gizmos.draw_poly(
        corners,
        true,
        COLOR_TRANS.with_a(0.5),
        2.0,
        UI_TOOL_GIZMO_Z_ORDER,
    );
}
