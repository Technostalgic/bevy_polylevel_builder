# Bevy Poly Level

### NOTE - Bevy Poly Level is in very early access, expect bugs and frequent changes

Bevy Poly Level is a level editor and importer that can be integrated directly into your game. The
idea is similar to that of the Level Designer Toolkit (LDtk), however it focuses on polygonal 
geometry based levels, rather than tile based levels.

What this crate offers is a level editor program, if compiled and ran independently, or if 
integrated into a bevy game project, it can be used as an embedded level editor. This allows Bevy
Poly Level to exchange data through a "level data" resource, allowing the editor to edit the current
game level (or whatever level data is passed to the resource) with the press of a hotkey, or a 
different action defined by the user.

Integrating level data into the game as collision geometry, entities, and all custom defined user
data within the level and it's geometry or entities, is all dependent upon implementation by the 
user, which allows the level editor to be used for practically any case. The functionality that this
crate provides is an interface for creating custom levels, switching to and from that interface 
"editor state" within the game, serializing the level data into a file, and then being able to load 
and deserialize these files as level data. What the data actually represents in-game is up to how 
you define the implementation.

## Compatibility

| bevy_level_edit | bevy |
| ---- | ---- |
| 0.2.x | 0.13 |
| 0.1.x | 0.12 |
